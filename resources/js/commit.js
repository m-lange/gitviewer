
window.ready = function() {
	
	function showFirstDiff() {
		var files = $('.commit-file');
		var first = $(files).first();
		if( files.length == 0 ) {
			app.clearDiff();
		} else {
			var path = app.getSelectedFile();
			var found = false;
			if(path != null) {
				$(files).each(function() {
					var fp = $(this).data('file-path');
					if( fp == path ) {
						$(this).trigger( "click" );
						found = true;
						return;
					}
				})
			}
			if( ! found ) $(first).trigger( "click" );
		}
	}
	
	$('.commit-file').click(
			function(evt) {
				var self = this
				$('.commit-file').each(function(index) {
					if( this == self ) {
						$(this).addClass('file-selected');
					} else {
						$(this).removeClass('file-selected');
					}
				});
				app.showDiff( $(this).data('commit'), $(this).data('parent'), $(this).data('file-path') );
			});

	$('.commit-file').hover(
			function () {
				$(this).css('font-weight', 'bold');
			},
			function() {
				$(this).css('font-weight', '');
			});

	$('.parent').click(
			function(evt) {
				var id = $(this).data('commit')
				if( evt.ctrlKey ) {
					var sel = app.getSelection();
					var selectedA = (sel.length > 0) ? sel[0] : null;
					app.updateSelection( selectedA, id );
				} else {
					app.updateSelection( id, null );
				}
			});

	showFirstDiff();
}
