

function updateSelection(scrollToSelection) {
	var sel = app.getSelection();
	var selectedA = (sel.length > 0) ? sel[0] : null;
	var selectedB = (sel.length > 1) ? sel[1] : null;
	$('.commitHighlight').attr('fill', 'transparent');
	if ( selectedB != null ) {
		$('.commitHighlight#'+selectedB).attr('fill', 'gainsboro');
	}
	if( selectedA == null ) {
		var firstRow = $('.row')
		if( firstRow.length > 0 ) {
			selectedA = $(firstRow).attr('id').replace('row-', '');
		}
	}
	if( selectedA != null ) {
		var row = $('.commitHighlight#'+selectedA)
		$(row).attr('fill', 'lightgray');
		var pos = $(row).attr('y');
		
		if( scrollToSelection ) {
			var viewportTop = $(window).scrollTop();
			var viewportBottom = viewportTop + document.body.clientHeight;
			if( pos < viewportTop || pos > viewportBottom - 50 ) $("html, body").animate({ scrollTop: pos }, 600);
		}
	}
	app.setSelection( selectedA, selectedB );
}

function getScrollPosition() {
	return $(window).scrollTop();
}
function setScrollPosition(pos) {
	$(window).scrollTop(pos);
}

window.ready = function() {

	$('.row').hover(
			function () {
				$(this).find('.commitDot').attr('r',dotRadiusHover);
				$(this).find('.message').attr('font-weight', 'bold');
				$(this).find('.commit-id').attr('font-weight', 'bold');
			},
			function() {
				$(this).find('.commitDot').attr('r',dotRadius);
				$(this).find('.message').attr('font-weight', '');
				$(this).find('.commit-id').attr('font-weight', '');
			});
	$('.row').click(
			function(evt) {
				var sel = app.getSelection()
				var selectedA = (sel.length > 0) ? sel[0] : null
				var selectedB = (sel.length > 1) ? sel[1] : null
				
				if ( evt.ctrlKey && (selectedA != null) ) {
					if(selectedB != null) {
						$('.commitHighlight#'+selectedB).attr('fill', 'transparent');
					}
					selectedB = $(this).attr('id').replace('row-', '');
					$('.commitHighlight#'+selectedB).attr('fill', 'gainsboro');
					app.setSelection( selectedA, selectedB );
				} else {
					if(selectedA != null) {
						$('.commitHighlight#'+selectedA).attr('fill', 'transparent');
					}
					if(selectedB != null) {
						$('.commitHighlight#'+selectedB).attr('fill', 'transparent');
						selectedB = null;
					}
					selectedA = $(this).attr('id').replace('row-', '');
					$('.commitHighlight#'+selectedA).attr('fill', 'lightgray');
					app.setSelection( selectedA, null );
				}
			});
	$('.row').mousedown(function(event) {
		if(event.which == 3) {
			var id = $(this).attr('id').replace('row-', '');
			app.contextMenuCommit( id, event.screenX, event.screenY );
		}
	});
	$('#load-more-link').click(
			function(evt) {
				app.loadMoreCommits();
			});
	setScrollPosition( app.getLastScrollPosition() );
	updateSelection(false);
}