
window.ready = function() {
	
	$('.branch, .tag, .remote').click(
			function(evt) {
				var id = $(this).data('commit-id');
				if( evt.ctrlKey ) {
					var sel = app.getSelection();
					var selectedA = (sel.length > 0) ? sel[0] : null;
					app.updateSelection( selectedA, id );
				} else {
					app.updateSelection( id, null );
				}
			});

	$('.branch, .tag, .remote').hover(
			function () {
				$(this).css('font-weight', 'bold');
			},
			function() {
				$(this).css('font-weight', '');
			});

	$('.branch').mousedown(function(event) {
		event.preventDefault();
		if(event.which == 3) {
			var id = $(this).data('name');
			var localOnly = $(this).hasClass("local");
			app.contextMenuBranch( id, false, localOnly, event.screenX, event.screenY );
		}
	});
	$('.remote').mousedown(function(event) {
		event.preventDefault();
		if(event.which == 3) {
			var id = $(this).data('name');
			app.contextMenuBranch( id, true, false, event.screenX, event.screenY );
		}
	});
	$('.tag').mousedown(function(event) {
		event.preventDefault();
		if(event.which == 3) {
			var id = $(this).data('name');
			var commit = $(this).data('commit-id');
			app.contextMenuTag( id, commit, event.screenX, event.screenY );
		}
	});

	$('.branch').dblclick(
			function(evt) {
				var id = $(this).data('name');
				app.checkoutBranch( id, false );
			});
	$('.remote').dblclick(
			function(evt) {
				var id = $(this).data('name');
				app.checkoutBranch( id, true );
			});
	$('.tag').dblclick(
			function(evt) {
				var commit = $(this).data('commit-id');
				app.checkoutCommit( commit );
			});

}