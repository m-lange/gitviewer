
window.ready = function() {
	
	$('.commit-file').click(
			function(evt) {
				var self = this
				$('.commit-file').each(function(index) {
					if( this == self ) {
						$(this).addClass('file-selected');
					} else {
						$(this).removeClass('file-selected');
					}
				});
				var path = $(this).data('file-path');
				app.showDiffStaging( path );
			});

	$('.commit-file').hover(
			function () {
				$(this).css('font-weight', 'bold');
			},
			function() {
				$(this).css('font-weight', '');
			});
	
	$('#message').on('input', 
			function() {
				var msg = $(this).val();
				app.saveDraftMessage(msg);
			});

	$('#stage-selected').click( function(evt) { app.stageSelected(); } );
	$('#stage-all').click( function(evt) { app.stageAll(); } );
	$('#unstage-selected').click( function(evt) { app.unstageSelected(); } );
	$('#unstage-all').click( function(evt) { app.unstageAll(); } );

	$('#commit-button').click( function(evt) { app.commit(); } );

}
