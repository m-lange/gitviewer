package de.lange.m.git

import java.io.File

/**
 * Global path variables.
 */
object Paths {
  
    /** Directory name of Git data directory (.git) */
    val gitDataDir = ".git"
    /** Path to repo config file, relative to Git data directory (gitviewer-config.xml) */
    val repoConfigPath = "gitviewer-config.xml"
    
    /** Directory of the application executable */
    val appPath = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI()).getParent
    
    /** Path to settings relative to appPath (settings.xml) */
    val settingsFile = "settings.xml"
    
    /** Path to help entry point relative to appPath (help\index.html) */
    val helpPath = "help\\index.html";
    
    /** Path to changelog relative to appPath (help\changelog.html) */
    val changelogPath = "help\\changelog.html";
    
    /** Path to UserConfig file (AppData\Roaming\gitviewer.xml) */
    val userConfigPath = System.getenv("APPDATA")+"\\gitviewer.xml";
    
    /** Path to UserConfig file (AppData\Roaming\gitviewer.log) */
    val logFilePath = System.getenv("APPDATA")+"\\gitviewer.log";
    
    /** Path to JavaScript files in resources (js) */
    val jsPath = "js";
    
    /** Path to CSS files in resources (css) */
    val cssPath = "css";
    
    /** Path to the jQuery script file in resources (js/jquery-3.3.1.min.js) */
    val jQueryFilePath = "js/jquery-3.3.1.min.js";
    
    /** Path to the application icon file in resources (images/Icon.png) */
    val appIconPath = "images/Icon.png";
    
    /** Path to the splash screen image file in resources (images/Splash.png) */
    val splashPath = "images/Splash.png";
    
    /** Path to the splash screen image file in resources (images/Splash.png) */
    val iconsPath = "icons";
    
}