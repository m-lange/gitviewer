package de.lange.m.git

import java.io.{ ByteArrayOutputStream, File, PrintStream }

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

import de.lange.m.git.data.GitTree
import de.lange.m.git.io.{ LoggerStream, XmlSettingsReader, XmlUserConfigReader }
import de.lange.m.git.settings.{ AppSettings, UserConfig }
import de.lange.m.git.ui.{ MainFrame, Splash, TreeWorker }
import de.lange.m.git.util.{ DialogUtil, GitUtil }
import it.sauronsoftware.junique.{ AlreadyLockedException, JUnique, MessageHandler }
import javafx.{ concurrent => jfxr }
import scalafx.application.Platform
import scalafx.concurrent.Task
import scalafx.embed.swing.SFXPanel

object ShowTree {
    
    var frame: MainFrame = null  
    
    def main(args: Array[String]) {
        val o = new PrintStream(new File(Paths.logFilePath))
        val console = System.out
        
        System.setOut( new PrintStream(new LoggerStream(new ByteArrayOutputStream(), o, console)) )
        System.setErr( new PrintStream(new LoggerStream(new ByteArrayOutputStream(), o, console)) )
        
        val appId = "de.lange.m.GitViewer"
    	val alreadyRunning = 
        	try {
        		JUnique.acquireLock(appId, new MessageHandler() {
        			def handle(message: String): String = {
        			    if(frame != null) {
        			        frame.toFront()
        			        println("Message to open repo: "+message)
        			        if(message.length() > 0) {
        			            frame.openRepository(message)
        			        }
        			    }
        				return "success";
        			}
        		});
        		false
        	} catch {
        	    case e: AlreadyLockedException =>
        		    true
        	}
    	if (alreadyRunning) {
    	    val basePath = if( args.length == 0 || args(0).startsWith("-") ) "" else args(0)
    	    val dir = Paths.gitDataDir
		    val path = if(basePath == null || basePath.length() == 0) "" else new File( if(basePath.endsWith(dir)) basePath else (basePath+"\\"+dir) ).getAbsolutePath
    	    JUnique.sendMessage(appId, path)
    	} else {
            try {
                runApp(args)
            } catch {
                case e: Exception =>
                    DialogUtil.error("Error", e.getClass.getSimpleName+": "+e.getMessage, e.getStackTrace.mkString("\n") )
                    Platform.implicitExit = true
            }
    	}
    }
    
    def runApp(args: Array[String]) {
	    if(args.length > 0 && (args(0) == "-v" || args(0) == "--version")) {
	        println( Version.version )
	        return
	    }
    	
		println("App directory: "+Paths.appPath)
		Platform.implicitExit = false
		
		val basePath = if( args.length == 0 || args(0).startsWith("-") ) null else args(0)
		
		println("Working directory: "+System.getProperty("user.dir"))
		
        val settings = new XmlSettingsReader().read( Paths.appPath+"\\"+Paths.settingsFile )
    	
    	val config = if(new File(Paths.userConfigPath).exists()) {
    	                    try {
    	                        new XmlUserConfigReader().read( Paths.userConfigPath ) 
    	                    } catch {
    	                        case e: Exception =>
    	                            println( "Problem loading user config from "+Paths.userConfigPath )
    	                            println( e.getClass+": "+e.getMessage )
    	                            UserConfig()
    	                    }
    	                } else {
    	                    UserConfig()
    	                }
		
		new SFXPanel()
        val dir = Paths.gitDataDir
		val path = if(basePath == null) null else if(basePath.endsWith(dir)) basePath else (basePath+"\\"+dir)
		if(path != null && GitUtil.isRepository( new File(path) )) {
            val arguments = argMap( if( args.length == 0 || args(0).startsWith("-") ) args else args.slice(1, args.length) )
            
            val file = new File(path)
            if( (! file.exists()) || (! file.isDirectory()) ) {
                println("Not a repository: "+file.getAbsolutePath)
    		    Platform.exit()
            } else {
                
                val loadTask = new TreeWorker(path, settings.tree, settings.tree.maxCommits)
                
        		Platform.runLater {
                    new Splash().show(loadTask, () => {
                                    if( loadTask.value.value == null ) {
                                        Platform.implicitExit = true
                                    } else {
                                        showFrame(loadTask.value.value, settings, config)
                                    }
                                })
                    new Thread(loadTask).start()    
                }
            }
		} else {
		    Platform.runLater {
		        val task = new DummyWorker()
                new Splash().show(task, () => showFrame(null, settings, config))
                new Thread(task).start()    
            }
		}
        
    }
    
    class DummyWorker() extends Task(new jfxr.Task[Int] {
        override protected def call: Int = {
            0
        }
    })
    
    private def showFrame( tree: GitTree, settings: AppSettings, config: UserConfig ) {
        frame = new MainFrame(settings, config)
        if(tree != null) {
            frame.showRepository(tree, true)
        }
        Platform.implicitExit = true
    }
    
    private def argMap(args: Array[String]): mutable.HashMap[String, Array[String]] = {
        val argMap = new mutable.HashMap[String, Array[String]]()

		var key: String = null
		val values = new ArrayBuffer[String]()
		for(i <- 0 until args.length) {
			if(args(i).startsWith("-") && ! args(i)(1).isDigit) {
				if( key != null ) {
					argMap(key) = values.toArray
					values.clear()
				}
				key = args(i)
			} else {
				values += args(i)
			}
		}
        if(args.length > 0) {
		    argMap(key) = values.toArray
        }
		
		for( (k,v) <- argMap ) {
			println( k + " -> " + v.mkString(" | ") )
		}
		argMap
    }
    
	private def mapValue(args: mutable.Map[String, Array[String]], short: String, long: String): Array[String] = {
		args.get("-"+short) match {
			case Some(value) => value
			case None => args.get("--"+long) match {
				case Some(value) => value
				case None => null
			}
		}
	}
    
}
