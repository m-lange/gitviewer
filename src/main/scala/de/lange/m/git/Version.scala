package de.lange.m.git

import java.util.Properties

/**
 * The running program's version.
 */
object Version extends {
    
    /** Application name / name of the .properties file */
    val app = "gitviewer"
    
    /** The [Version]. */
    val version = {
        val v = {
        	var v = "0.0.0";
    
        	val prop = new Properties()
    		prop.load( classOf[Version].getClassLoader().getResourceAsStream(app+".properties"))
    		prop.getProperty("productversion", "0.0.0").trim();
        }
        new Version(v)
    }
    
    override def toString() = version.toString()
}

/**
 * Version info according Semantic Versioning: major.minor.patch
 */
class Version (
    _version: String ) extends Ordered[Version] {
    
    /** Version string */
    val version = _version.trim()
    
    val (major, minor, patch) = {
        val parts = version.split("\\.")
        
		( parts(0).toInt,
		  parts(1).toInt,
		  parts(2).toInt )
    }
    
    /**
     * Compare this version to another version.
     */
    override def compare(other: Version): Int = {
        val maj = major.compare( other.major )
        maj match {
            case 0 => 
                val min = minor.compare( other.minor )
                min match {
                    case 0 => 
                        val pat = patch.compare( other.patch )
                        pat
                    case _ => min
                }
            case _ => maj
        }
    }
    
    override def toString() = version
}