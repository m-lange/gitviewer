package de.lange.m.git.data

/**
 * Retraced branch information.
 * 
 * @constructor Create a new branch.
 * @param name the branch's name
 * @param commits a list of all commits assigned to the branch
 * @param position position category in the graph visualization, from left to right
 */
class Branch (
            val name: String,
            val commits: Array[Commit],
            val position: Int
        ) {
  
    /** Position among branches in the same positioning category. */
    var subPosition = 0
    /** Final position in the graph visualization, from left to right. */
    var absolutePosition = position
    
    /** Index of the first commit of this branch */
    def firstIndex = commits.last.index
    /** Index of the second commit of this branch */
    def secondIndex = firstIndex + 1
    /** Index of the last commit of this branch */
    def lastIndex = commits.head.index
    /** Number of commits between first and last commit (not necessarily the number of commits in the branch!) */
    def length = lastIndex - firstIndex
    
    /**
     * Check if this branch overlaps in time with another branch.
     */
    def overlaps(other: Branch): Boolean = {
        val a1 = secondIndex
        val a2 = lastIndex
        val b1 = other.secondIndex
        val b2 = other.lastIndex
        (a1 >= b1 && a1 <= b2) || (a2 >= b1 && a2 <= b2) || (b1 >= a1 && b1 <= a2) || (b2 >= a1 && b2 <= a2)
    }
    
}