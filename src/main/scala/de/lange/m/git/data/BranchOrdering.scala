package de.lange.m.git.data

/**
 * Generic ordering of branches be name prefix.
 * 
 * @constructor Create a new ordering.
 * @param order Branch name prefixes of this ordering
 */
class BranchOrdering(
            val order: Array[String]
        ) {
  
    /**
     * Get the ordering index of a branch name by prefix.
     * 
     * @param branch Branch name to get index
     */
    def apply(branch: String): Int = {
        for(i <- 0 until order.length) {
            if( branch.startsWith(order(i)) ) return i
        }
        return order.length
    }
    
    override def clone(): BranchOrdering = {
        new BranchOrdering( order.clone() )
    }
    
}