package de.lange.m.git.data

import scala.collection.mutable.{ ArrayBuffer, LinkedHashSet }

import org.eclipse.jgit.lib.ObjectId

/**
 * Representation of a Git commit.
 * 
 * @constructor Create a new commit representation.
 * @param index the index of the commit among all commits, in ascending chronological order
 */
trait Commit {
  
    val index: Int
    
    private[this] var _parents: Array[Commit] = new Array[Commit](0)
    private[this] var _children: Array[Commit] = new Array[Commit](0)
    
    /** All branch traces the commit is on */
    val branches = new LinkedHashSet[String]()
    
    /** The horizontal position of the commit in the graphical representation */
    var position = 999
    
    /** Is this commit currently checked out? */
    var isCheckedOut = false
    
    /** The first seven characters of the commit's hash */
    val shortName: String = name.substring(0, 7)
    
    /** All tags of the commit */
    val tags = new ArrayBuffer[String]()
    
    /** All (real) branches of the commit */
    val heads = new ArrayBuffer[String]()
    
    /** Get the commit's parents */
    def parents = _parents
    /** Set the commit's parents */
    protected[data] def parents_=(par: Array[Commit]) {
        _parents = par
    }
    /** Get the commit's children */
    def children = _children
    /** Set the commit's children */
    protected[data] def children_=(ch: Array[Commit]) {
        _children = ch
    }
    /** The commit's full ID (incl. timestamp). This is not the Commit's hash! */
    def id: ObjectId
    /** The commit's full hash. */
    def name: String
    
    /** The commit's timestamp. */
    def time: Int
    
    /** The commit's author. */
    def author: String
    /** The commit's committer. */
    def committer: String
    
    /** The commit's short message. */
    def message: String
    /** The commit's full message. */
    def fullMessage: String
    
    override def toString(): String
    
    override def hashCode(): Int
}