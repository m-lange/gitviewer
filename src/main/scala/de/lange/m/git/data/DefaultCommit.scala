package de.lange.m.git.data

import scala.collection.mutable.{ ArrayBuffer, LinkedHashSet }

import org.eclipse.jgit.revwalk.RevCommit

/**
 * Representation of a Git commit.
 * 
 * @constructor Create a new commit representation.
 * @param index the index of the commit among all commits, in ascending chronological order
 * @param commit the associated JGit RevCommit object
 */
class DefaultCommit (
            override val index: Int,
            val commit: RevCommit
        ) extends Commit {
    
    /** The commit's full ID (incl. timestamp). This is not the Commit's hash! */
    def id = commit.getId
    
    /** The commit's full hash. */
    def name = commit.getName
    
    /** The commit's timestamp. */
    def time = commit.getCommitTime
    
    /** The commit's author. */
    def author = commit.getAuthorIdent.getName
    /** The commit's committer. */
    def committer = commit.getCommitterIdent.getName
    
    /** The commit's short message. */
    def message = commit.getShortMessage
    /** The commit's full message. */
    def fullMessage = commit.getFullMessage
    
    override def toString() = commit.toString()
    
    /** Check for equality with another Commit or RevCommit */
    override def equals(other: Any): Boolean = {
        other match {
            case c: DefaultCommit => commit.equals(c.commit)
            case r: RevCommit => commit.equals(r)
            case _ => false
        }
    }
    
    override def hashCode() = commit.hashCode()
}