package de.lange.m.git.data

import org.eclipse.jgit.revwalk.RevCommit

class DummyCommit(
            _index: Int,
            _commit: RevCommit
        ) extends DefaultCommit (_index, _commit) {
  
}