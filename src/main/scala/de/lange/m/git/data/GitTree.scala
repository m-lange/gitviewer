package de.lange.m.git.data

import scala.collection.mutable.{ ArrayBuffer, HashMap, LinkedHashMap }
import scala.util.Try
import scala.util.control.Breaks.{ break, breakable }

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.errors.JGitInternalException
import org.eclipse.jgit.lib.{ Constants, ObjectId, Ref }
import org.eclipse.jgit.revwalk.{ RevCommit, RevWalk }

import de.lange.m.git.settings.{ RepoConfig, TreeViewSettings }
import scalafx.scene.paint.Color
import de.lange.m.git.util.GitUtil

/**
 * Holds general information on Git branch names and formats
 */
object GitTree {
    val branchPrefix = "refs/heads/"
    val tagPrefix = "refs/tags/"
    val remotePrefix = "refs/remotes/"
    
    val mergeMessageBranchStart = "Merge branch '"
    val mergeMessageCommitStart = "Merge commit '"
    val mergeMessageSplit = "' into "
    
    val master = "master"
}

/**
 * Data structure for a repositories commits and refs.
 * 
 * @constructor Construct a new repository representation tree.
 * @param git the Git object of the repository
 * @param _commits Array of all commits to process (RevCommit instances)
 * @param refs all refs (tags, branches, heads) of the repository
 * @param settings the TreeViewSettings to be used
 */
class GitTree(
            val git: Git,
            _commits: Array[RevCommit],
            val refs: Array[Ref],
            val isComplete: Boolean,
            val settings: TreeViewSettings,
            val repoConfig: RepoConfig
        ) {
    
    /** Get the associated repository */
    def repository = git.getRepository
    
    /** The RevWalk object for the repository */
    private val walk = new RevWalk(repository)
    
    val name = repository.getDirectory.getAbsoluteFile.getParentFile.getName
    
    private var _uncommitted: Uncommitted = null
    def uncommitted = _uncommitted
    /** The commits */
    val commits: Array[Commit] = {
                            val c = new ArrayBuffer[Commit]()
                            var len = _commits.length
                            var cnt = 0
                            // TODO
                            if( GitUtil.hasUncommittedChanges(this) ) {
                                len += 1
                                _uncommitted = new Uncommitted( len-cnt-1 )
                                c += _uncommitted
                                cnt += 1
                            }
                            for( comm <- _commits ) {
                                c += new DefaultCommit( len-cnt-1, comm ) 
                                cnt += 1
                            }
                            c.toArray
                        }
                        
    /** Timestamp of the latest commit */
    val lastCommitTime = commits.head.time
    
    /** HashMap associating Commits to their name string / hash. */
    val lookup = new HashMap[String, Commit]()
                                for(c <- commits) {
                                    lookup(c.name) = c
                                }
    /** HashMap associating Commits to their name string / hash. */
    val refsLookup = new HashMap[String, Ref]()
                                for(ref <- refs if ref.getName.startsWith(GitTree.branchPrefix)) {
                                    refsLookup(ref.getName) = ref
                                }
    /** HashMap associating branch names to a commit. If the commit is not in the list, the value is null. */
    val branchRefs = new LinkedHashMap[String, Commit]()
                                for(ref <- refs if ref.getName.startsWith(GitTree.branchPrefix)) {
                                   if(lookup.contains(ref.getObjectId.name())) {
                                       branchRefs(ref.getName.substring(GitTree.branchPrefix.length())) = lookup(ref.getObjectId.name())
                                   }
                                }
    /** HashMap associating commits with branch names (traced!). */
    val branchCommits = new HashMap[Commit, ArrayBuffer[String]]()
                                for( (k,v) <- branchRefs ) {
                                    if( ! branchCommits.contains(v) ) branchCommits(v) = ArrayBuffer(k)
                                    else branchCommits(v) += k
                                }
    
    /** HashMap associating tag names to commits. If the commit is not in the list, the value is null. */
    val tagRefs = new LinkedHashMap[String, Commit]()
                                for(ref <- refs if ref.getName.startsWith(GitTree.tagPrefix)) {
                                   val commit = walk.parseCommit(ref.getObjectId())
                                   if(lookup.contains(commit.name())) {
                                       tagRefs(ref.getName.substring(GitTree.tagPrefix.length())) = lookup(commit.name())
                                   }
                                }
    /** HashMap associating commits with tag names. */
    val tagCommits = new HashMap[Commit, ArrayBuffer[String]]()
                                for( (k,v) <- tagRefs ) {
                                    if( ! tagCommits.contains(v) ) tagCommits(v) = ArrayBuffer(k)
                                    else tagCommits(v) += k
                                }
    /** HashMap associating remote branches to a commit. */
    val remoteRefs = new LinkedHashMap[String, Commit]()
                                for(ref <- refs if ref.getName.startsWith(GitTree.remotePrefix)) {
                                   val commit = walk.parseCommit(ref.getObjectId())
                                   //if(lookup.contains(commit.name())) {
                                   remoteRefs(ref.getName.substring(GitTree.remotePrefix.length())) = if(lookup.contains(commit.name())) lookup(commit.name()) else null
                                   //}
                                }
    
    /** HashMap associating branch names to a Branch info object */
    val branches = new LinkedHashMap[String, Branch]()
    
    /** HashMap associating branch names to a color */
    private val colors = new HashMap[String, Color]()
    
    /** HashMap associating branch names to a persistence index (lower = higher persistence) */
    val persistence = new HashMap[String, Int]()
    
    /**
     * Get the currently checked out commit
     */
    def current = _current
    private var _current: Commit = null
    
    /**
     * Get the currently checked out branch
     */
    def currentBranch = _currentBranch
    private var _currentBranch: String = null
    
    def remotes = repository.getRemoteNames
    
    def hasUncommittedChanges: Boolean = {
        GitUtil.hasUncommittedChanges(this)
    }
    
    /** Initialize the tree.
     *  
     *  @param progress optional progress monitor function
     */
    def init(progress: (String, Double, Double) => Unit) {
        if(progress != null) progress("Collecting branches...", 50, 100)
        headCommits()
        if(progress != null) progress("Connecting...", 50, 100)
        connectTree()
        if(progress != null) progress("Tracing branches...", 50, 100)
        createBranches()
        if(progress != null) progress("Collecting tags...", 80, 100)
        tagAllCommits()
        if(progress != null) progress("Constructing UI...", 95, 100)
    }
    
    /**
     * Get the color for a branch name. Returns DarkGrey if no color is available for the name.
     */
    def branchColor(branch: String): Color = {
        colors.get(branch) match {
            case Some(c) => c
            case None => Color.DarkGray
        }
    }
    
    /**
     * Assign tags to commits.
     */
    private def tagAllCommits() {
        for((name, com) <- tagRefs) {
            if(com != null) {
                com.tags += name
            }
        }
    }
    
    /**
     * Assign heads to commits.
     */
    private def headCommits() {
        for((name, com) <- branchRefs) {
            if(com != null) {
                com.heads += name
            }
        }
        val ref = repository.exactRef(Constants.HEAD)
        val commit = walk.parseCommit(ref.getObjectId())
        _current = if(lookup.contains(commit.name())) lookup(commit.name()) 
                    else null
        _currentBranch = repository.getBranch()
        if( _current != null ) {
            _current.isCheckedOut = true
        }
    }
    
    /**
     * Connect the tree bi-directional (i.e. assign children).
     */
    private def connectTree() {
        val children = new HashMap[ObjectId, ArrayBuffer[Commit]]()
        var cnt = 0
        for(c <- commits) {
            c.parents = c match {
                case dc: DefaultCommit => //dc.commit.getParents.filter( (p) => lookup.contains( p.getName ) ).map( (p) => lookup( p.getName ) )
                    for( p <- dc.commit.getParents ) yield {
                        if( lookup.contains( p.getName ) ) lookup( p.getName )
                        else {
                            cnt -= 1
                            new DummyCommit( cnt, p )
                        }
                        
                    }
                case uc: Uncommitted => Array(_current)
            }
            for(p <- c.parents) {
                if( ! children.contains(p.id) ) children(p.id) = new ArrayBuffer[Commit]()
                children(p.id) += c
            }
        }
        for(c <- commits) {
            if( children.contains(c.id) ) c.children = children(c.id).toArray
        }
    }
    
    private def createBranchName(branches: HashMap[String, ArrayBuffer[Commit]], baseName: String): String = {
        if( ! branches.contains(baseName) ) {
            baseName
        } else {
            val parts = baseName.split("/")
            val idx = Try(baseName.toInt).toOption
            idx match {
                case Some(index) =>
                    var i = index + 1
                    while( branches.contains( baseName+"/"+i ) ) {
                        i += 1
                    }
                    baseName+"/"+i
                case None =>
                    baseName+"/1"
            }
        }
    }
    
    /**
     * Trace branches, determine positions for all commits.s
     */
    private def createBranches() {
        val branches = new HashMap[String, ArrayBuffer[Commit]]()
        for( c <- commits ) {
            if( branchCommits.contains(c) ) {
                for( br <- branchCommits(c) ) {
                    if(! branches.contains(br)) branches(br) = new ArrayBuffer[Commit]()
                    branches(br) += c
                    c.branches += br
                }
            }
            for(child <- c.children) {
                val isMergeBranch = child.message.startsWith(GitTree.mergeMessageBranchStart)
                val isMergeCommit = child.message.startsWith(GitTree.mergeMessageCommitStart)
                val isMerge = isMergeBranch || isMergeCommit
                if( isMerge ) {
                    val splitLen = if(isMergeBranch) GitTree.mergeMessageBranchStart.length() else GitTree.mergeMessageCommitStart.length()
                    val fromTo = child.message.substring(splitLen).split(GitTree.mergeMessageSplit)
                    if(fromTo.length == 1) fromTo(0) = fromTo(0).replace("'", "")
                    var br = if(child.parents(0) == c) 
                                if(fromTo.length > 1) fromTo(1) else GitTree.master
                             else fromTo(0)
                    if(c != child.parents(0) && branches.contains(br) && (!c.branches.contains(br))) {
                        br = createBranchName(branches, br)
                    }
                    if(! branches.contains(br)) {
                        branches(br) = new ArrayBuffer[Commit]()
                    }
                    branches(br) += c
                    c.branches += br
                } else {
                    val brs = child.branches.toArray
                    if(brs.isEmpty) {
                        
                    } else if(brs.length > 1) {
                        val sort = brs.sortWith( (a,b) => repoConfig.branching.branchPersistence(a) < repoConfig.branching.branchPersistence(b) )
                        val br = sort(0)
                        if(! branches.contains(br)) branches(br) = new ArrayBuffer[Commit]()
                        branches(br) += c
                        c.branches += br
                    } else {
                        for(br <- brs) {
                            if(! branches.contains(br)) branches(br) = new ArrayBuffer[Commit]()
                            branches(br) += c
                            c.branches += br
                        }
                    }
                }
            }
            val unknown = "unknown-branch"
            if( c.branches.isEmpty || c.branches.count( _.startsWith(unknown) ) > 0 ) {
                val isMergeBranch = c.message.startsWith(GitTree.mergeMessageBranchStart)
                val isMergeCommit = c.message.startsWith(GitTree.mergeMessageCommitStart)
                val isMerge = isMergeBranch || isMergeCommit
                val br = if(isMerge) {
                                val splitLen = if(isMergeBranch) GitTree.mergeMessageBranchStart.length() else GitTree.mergeMessageCommitStart.length()
                                val fromTo = c.message.substring(splitLen).split(GitTree.mergeMessageSplit)
                                if(fromTo.length == 1) fromTo(0) = fromTo(0).replace("'", "")
                                if(fromTo.length > 1) fromTo(1) else GitTree.master
                            } else {
                                val brUnknown = c.branches.find( _.startsWith(unknown) )
                                brUnknown match {
                                    case Some(str) => str
                                    case None => createBranchName(branches, unknown)
                                }
                            }
                if(! branches.contains(br)) branches(br) = new ArrayBuffer[Commit]()
                branches(br) += c
                c.branches += br
            }
            val sort = c.branches.toArray.sortBy( repoConfig.branching.branchPersistence(_) )
            c.branches.clear()
            c.branches ++= sort
        }
        val ordered = branches.keySet.toArray.sortBy( repoConfig.branching.branchOrder(_) )
        val tempBranches = new LinkedHashMap[String, Branch]()
        for( name <- ordered ) {
            tempBranches(name) = new Branch(name, branches(name).toArray, repoConfig.branching.branchOrder(name))
        }
        val ordered2 = tempBranches.toArray.sortWith( (t1, t2) => { 
                        if(t1._2.position == t2._2.position) t1._2.length < t2._2.length
                        else t1._2.position < t2._2.position
                })
        for( i <- 0 until ordered2.length ) {
            val name = ordered2(i)._1
            val branch = tempBranches(name)
            for(j <- 0 until i) {
                val name2 = ordered2(j)._1
                val branch2 = tempBranches(name2)
                if( branch.position == branch2.position && 
                        branch.commits.length > 1 && 
                        branch2.commits.length > 1 && 
                        branch.overlaps(branch2) ) {
                     if( branch2.subPosition + 1 >= branch.subPosition ) {
                         branch.subPosition = branch2.subPosition + 1
                     }
                }
            }
        }
        val ordered3 = tempBranches.toArray.sortWith( (t1, t2) => { 
                        if(t1._2.position == t2._2.position) t1._2.subPosition < t2._2.subPosition
                        else t1._2.position < t2._2.position
                })
        this.branches ++= ordered3
        var pos = 0
        var subPos = 0
        var absPos = 0
        for( (name, branch) <- this.branches ) {
            if(branch.position > pos) {
                pos = branch.position
                subPos = 0
                absPos += 1
            } else if(branch.subPosition > subPos) {
                subPos += 1
                absPos += 1
            }
            persistence(name) = repoConfig.branching.branchPersistence(name)
            branch.absolutePosition = absPos
            
            breakable(for( (prefix, col) <- repoConfig.branching.branchColors ) {
                if(name.startsWith(prefix)) {
                    colors(name) = col
                    break
                }
            })
        }
        for( comm <- commits.reverse ) {
            comm match {
                case dc: DefaultCommit => comm.position = this.branches( comm.branches.head ).absolutePosition
                case uc: Uncommitted => comm.position = comm.parents(0).position
            }
        }
    }
    
    /**
     * Find the common branch trace of two commits. Returns the parent's first branch if no common branch is found
     */
    def commonBranch(parent: Commit, child: Commit): String = {
        for( br1 <- parent.branches ) {
            for( br2 <- child.branches ) {
                if( br1 == br2 ) return br1
            }
        }
        if( parent.branches.isEmpty ) child.branches.head
        else parent.branches.head
    }
                
}