package de.lange.m.git.data

object Uncommitted {
    val ID = "uncommitted-changes"
}

class Uncommitted (
                override val index: Int
            ) extends Commit {
    
    /** The commit's full ID (incl. timestamp). This is not the Commit's hash! */
    def id = null
    
    /** The commit's full hash. */
    def name = Uncommitted.ID
    
    /** The first seven characters of the commit's hash */
    override val shortName: String = "-------"
    
    /** The commit's timestamp. */
    def time = (System.currentTimeMillis() / 1000L).toInt
    
    /** The commit's author. */
    def author = "----"
    /** The commit's committer. */
    def committer = "----"
    
    /** The commit's short message. */
    def message = "Uncommitted changes"
    /** The commit's full message. */
    def fullMessage = "Uncommitted changes"
    
    override def toString() = "Uncommitted changes"
    
    /** Check for equality with another Commit or RevCommit */
    override def equals(other: Any): Boolean = {
        other match {
            case c: Uncommitted => this eq c
            case _ => false
        }
    }
}
