package de.lange.m.git.io

import java.io.PrintStream
import java.io.FilterOutputStream
import java.io.OutputStream
import de.lange.m.git.util.DialogUtil

class DialogStream(s: OutputStream) extends FilterOutputStream(s) {
  
    override def write(b: Array[Byte]) {
        val aString = new String(b)
        writeString(aString)
    }

    override def write(b: Array[Byte], off: Int, len: Int) {
        val aString = new String(b, off, len)
        writeString(aString)
    }
    private def writeString(str: String) {
        DialogUtil.error("Error", "An error occured!", str)
    }
    
}