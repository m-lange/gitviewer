package de.lange.m.git.io

import java.io.File

import scala.collection.JavaConverters.{ collectionAsScalaIterableConverter, iterableAsScalaIterableConverter }
import scala.collection.mutable.ArrayBuffer
import scala.util.control.Breaks.{ break, breakable }

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.{ RevCommit, RevWalk }

import de.lange.m.git.Paths
import de.lange.m.git.data.GitTree
import de.lange.m.git.settings.{ RepoConfig, TreeViewSettings }
import de.lange.m.git.util.DialogUtil

/**
 * Loads a GitTree from a repository.
 */
object GitTreeReader {
  
    private val remotePrefix = "refs/remotes/"
        
    /**
     * Load repository from a given path.
     */
    def loadTree(path: String, treeSettings: TreeViewSettings, maxCommits: Int, progress: (String, Double, Double) => Unit ): GitTree = {
        val git = Git.open( new File(path) )
        if(progress != null) progress(null, 20, 100)
        
        loadTree(git, treeSettings, maxCommits, progress)
    }
    
    /**
     * Load repository from a given Git instance.
     */
    def loadTree(git: Git, treeSettings: TreeViewSettings, maxCommits: Int, progress: (String, Double, Double) => Unit ): GitTree = {
        while( true ) {
            try {
                val repository = git.getRepository
                if(progress != null) progress("Getting refs...", 25, 100)
                
                val allRefs = repository.getAllRefs().values().asScala
                if(progress != null) progress("Initializing Walker...", 30, 100)
                
                val revWalk = new RevWalk( repository )
                for( ref <- allRefs if ! ref.getName.startsWith(remotePrefix) ) {
                    revWalk.markStart( revWalk.parseCommit( ref.getObjectId() ))
                }
                if(progress != null) progress("Collecting commits...", 35, 100)
                
                val commits = revWalk.asScala
                val result = new ArrayBuffer[RevCommit]
                var isComplete = true
                var cnt = 0
                breakable(for( commit <- commits ) {
                    result += commit
                    if( maxCommits > 0 && cnt >= maxCommits-1 ) {
                        isComplete = false
                        break
                    }
                    cnt += 1
                })
                if(progress != null) progress("Constructing tree...", 40, 100)
                val config = try {
                                new XmlRepositoryConfigReader().read(git.getRepository.getDirectory+"\\"+Paths.repoConfigPath, treeSettings)
                            } catch {
                                case e: Exception =>
                                    RepoConfig(treeSettings)
                            }
                
                val tree = new GitTree(git, result.toArray, allRefs.toArray, isComplete, treeSettings, config)
                tree.init(progress)
                return tree
            } catch {
                case e: Exception =>
                    val confirm = DialogUtil.async( () => DialogUtil.confirm("Error loading repository", "Error loading: "+e.getClass.getSimpleName, 
                                    e.getMessage+"\nTry again (Ok) or close program (Cancel)") )
                    if( ! confirm ) {
                        return null
                    }
            }
        }
        null
    }
}