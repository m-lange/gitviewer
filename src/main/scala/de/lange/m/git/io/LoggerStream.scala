package de.lange.m.git.io

import java.io.{ FilterOutputStream, OutputStream, PrintStream }

/**
 * Stream to redirect console output to a log file, additionally to the console.
 */
class LoggerStream(s: OutputStream, val printers: FilterOutputStream*) extends FilterOutputStream(s) {
  
    override def write(b: Array[Byte]) {
        //val aString = new String(b)
        //writeString(aString)
        for(p <- printers) {
            p.write(b)
            flush()
        }
    }

    override def write(b: Array[Byte], off: Int, len: Int) {
        //val aString = new String(b, off, len)
        //writeString(aString)
        for(p <- printers) {
            p.write(b, off, len)
            flush()
        }
    }
    /*private def writeString(str: String) {
        for(p <- printers) {
            p.print(str)
            flush()
        }
    }*/
}