package de.lange.m.git.io

import java.io.{ File, PrintWriter }

import scala.xml.{ Elem, NodeBuffer, Text }

import de.lange.m.git.Paths
import de.lange.m.git.data.{ Commit, GitTree }
import de.lange.m.git.util.{ ColorUtil, ResourceUtil, XmlUtil }
import scalafx.scene.paint.Color
import de.lange.m.git.data.DummyCommit

/**
 * Creates graphical SVG representation of a repository.
 * 
 * @constructor Construct a new svg exporter.
 * @param tree the GitTree object of the repository
 * @param isForInternal determines if internal JavaScript is included
 */
class SvgExporter(
            val tree: GitTree,
            val isForInternal: Boolean
        ) {
  
    private val cssFile = Paths.cssPath+"/graph.css"
    private val jsFile = Paths.jsPath+"/graph.js"
    
    
    private val baseScale = tree.settings.verticalSpacing
    private val branchDistance = tree.settings.horizontalSpacing
    private val fontSize = tree.settings.fontSize
    private val fontFamily = tree.settings.fontFamily
    private val textSpacing = tree.settings.textSpacing
    private val offset = tree.settings.graphXOffset
    private val dotRadius = tree.settings.dotSize / 2f
    private val strokeWidth = tree.settings.lineWidth
    
    private val pageWidth = 1024
    
    private val newline = sys.props("line.separator")
    
    /**
     * Generate the SVG string for the graphics.
     */
    def getSvg(): String = {
        val xml = parseTree()
        val str = xml.toString()
        str
    }
    
    /**
     * Save the SVG string for the graphics to a file.
     */
    def saveSvg(path: String) {
        val str = getSvg()
		val writer = new PrintWriter(new File(path))

		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
  		writer.write(str)
  		writer.close()
    }
    
    /**
     * Parse the tree to SVG in Scala XML representation.
     */
    private def parseTree(): Elem = {
        val height = (tree.commits.length + 3) * baseScale
        val maxPos = tree.branches.maxBy( kv => kv._2.absolutePosition )._2.absolutePosition
		val radius = 0.2 * baseScale
		
		val css = createCss(ResourceUtil.getResource( cssFile ))
		
        val xml = 
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width={ if(isForInternal) pageWidth.toString() else "100%" } height={ height.toString() }>
			<style>{new Text(css)}</style>
			{ for(c <- tree.commits) yield commitHighlight(c) }
			{ for(c <- tree.commits) yield parseCommit(c) }
			{ loadMoreLink() }
			{ if(tree.settings.interactiveSvg) XmlUtil.scriptTag( ResourceUtil.getResource( Paths.jQueryFilePath ) ) }
			{ if(tree.settings.interactiveSvg) XmlUtil.scriptTag( jsVariables() ) }
			{ if(tree.settings.interactiveSvg) XmlUtil.scriptTag( ResourceUtil.getResource( jsFile ) ) }
		</svg>
        xml
    }
    
    /**
     * Load and parse CSS.
     */
    private def createCss(css: String): String = {
        css.
            replaceAll("\\[fontSize\\]", fontSize.toString()).
            replaceAll("\\[fontFamily\\]", fontFamily.toString()).
            replaceAll("\\[commitColor\\]", ColorUtil.toWeb(tree.settings.commitColor) ).
            replaceAll("\\[mergeColor\\]", ColorUtil.toWeb(tree.settings.mergeColor) )
    }
    
    /**
     * Generate JavaScript to pass variables.
     */
    private def jsVariables(): String = {
        "var dotRadius = "+dotRadius+";\n" +
        "var dotRadiusHover = "+(1.5*dotRadius)+";\n"
    }
    
    private def loadMoreLink(): Elem = {
        if( tree.isComplete ) {
            <rect x="0" y="0" width="0" height="0" fill="transparent" />
        } else {
            val p = pos(0, -2)
            <text id="load-more-link" x={p._1.toString()} y={(p._2+0.25*fontSize).toString()}>Load more...</text>
        }
    }
    
    /**
     * Generate a commit's background rect.
     */
    private def commitHighlight(c: Commit): Elem = {
        val p = pos(c)
        val rowOffset = tree.settings.verticalSpacing / 2f
		<rect class={"commitHighlight" + (if(c.isCheckedOut) " current" else "")} id={c.name} x="0" y={ (p._2-rowOffset).toString() } width="100%" height={ tree.settings.verticalSpacing.toString() } fill="transparent" />
	}
    
    /**
     * Parse a single commit to SVG.
     */
    private def parseCommit(c: Commit): NodeBuffer = {
        val p = pos(c)
        val rowOffset = tree.settings.verticalSpacing / 2f
        val date = tree.settings.dateFormat.format(c.time * 1000L)
		val maxPos = tree.branches.maxBy( kv => kv._2.absolutePosition )._2.absolutePosition
		val merge = c.parents.length > 1
		val stroke = ColorUtil.toWeb( if( c.branches.size == 0 ) {
		                Color.DarkGray
		            } else {
		                tree.branchColor( c.branches.head )
		            } )
		var fill = if(merge) "#ffffff" else stroke
		
		val items = new NodeBuffer()
		for(par <- c.parents) {
            val col = if( par.children.length == 1 ) {
                if( c.parents.length == 1 ) {
                    if(c.branches.size > 0) tree.branchColor( c.branches.head ) else Color.DarkGray
                } else {
                    if(par.branches.size > 0) tree.branchColor( par.branches.head ) else Color.DarkGray
                }
            } else {
                tree.branchColor( tree.commonBranch( par, c ) )
            }
            items ++= connect(c, par, merge, ColorUtil.toWeb(col) )
        }
		
		val row = new NodeBuffer()
		row += <title>{ c.fullMessage+"\n"+c.author+", "+date+"\n"+c.branches.mkString(" ") }</title>
		row += <rect class="commitBg" x="0" y={ (p._2-rowOffset).toString() } width="100%" height={ tree.settings.verticalSpacing.toString() } fill="transparent" />
		row += <circle class="commitDot" cx={ p._1.toString() } cy={ p._2.toString() } r={ dotRadius.toString() } stroke={ stroke } stroke-width={ strokeWidth.toString() } fill={ fill } />
		
    	val pId = pos(0, c.index)
    	
		// ID
		row += <text class="commit-id" x={(pId._1 - textSpacing).toString()} y={(pId._2+0.25*fontSize).toString()}>{c.shortName}</text>
		
		// MESSAGE
	    fill = stroke
    	val branchBuff = new NodeBuffer()
	    for( head <- c.heads ) {
	        branchBuff += <tspan class="ref" fill={ColorUtil.toWeb(tree.branchColor(head))}>[{head}]</tspan>
	        branchBuff += new Text(" ")
	    }
	    for( tag <- c.tags ) {
	        branchBuff += <tspan class="ref" fill={fill}>({tag})</tspan>
	        branchBuff += new Text(" ")
	    }
    	val text = c.message+" ("+c.author+")"
    	
    	var pText = pos(maxPos, c.index)
    	if(branchBuff.isEmpty) pText = (pText._1 + 18, pText._2)
    	row += <text class="message" x={(pText._1 + textSpacing).toString()} y={(pText._2+0.25*fontSize).toString()}>{branchBuff}{text}</text>
    	
    	val classes = (if(merge) "row merge" else "row commit") + (if(c.isCheckedOut) " current" else "")
    	val res = <g id={ "row-"+c.name } class={classes}>
		    { row }
		</g>
    	items += res
    	items
    }
    
    /**
     * Connect two commits.
     */
    private def connect(child: Commit, parent: Commit, merge: Boolean, col: String): NodeBuffer = {
        // TODO: implement diverge early setting
        val buff = new NodeBuffer()
        val com1 = if(child.index > parent.index) parent else child
        val com2 = if(child.index > parent.index) child else parent
        val p1 = if(com1.isInstanceOf[DummyCommit]) pos(com2.position, -1) else pos(com1)
        val p2 = if(com2.isInstanceOf[DummyCommit]) pos(com1.position, -1) else pos(com2)
        if( p1._1 == p2._1 ) { //if( com1.position == com2.position ) {
            buff += <path d={ ("M "+p1._1+" "+p1._2+" L "+p2._1+" "+p2._2) } stroke={ col } stroke-width={ strokeWidth.toString() } style="fill:none" />
        } else if( com2.index == com1.index + 1 ) {
            val pm = (0.5f * (p1._1 + p2._1), 0.5f * (p1._2 + p2._2))
            buff += <path d={ ("M %s %s Q %s %s %s %s Q %s %s %s %s".format(
                                    p1._1, p1._2, 
                                    p1._1, pm._2, pm._1, pm._2, 
                                    p2._1, pm._2, p2._1, p2._2
                            )) } stroke={ col } stroke-width={ strokeWidth.toString() } style="fill:none" />
        } else {
            val p3 = if(merge) {
                var parOnPos = false
                for( ch <- com2.parents) { if(ch.position == com2.position) parOnPos = true }
                if( parOnPos ) {
                    pos(com1.position, com2.index - 1)
                } else {
                    pos(com2.position, com1.index + 1)
                }
            } else {
                pos(com2.position, com1.index + 1)
            }
            if( p3._1 == p1._1 ) {
                val pm = (0.5f * (p3._1 + p2._1), 0.5f * (p3._2 + p2._2))
                buff += <path d={ ("M %s %s L %s %s Q %s %s %s %s Q %s %s %s %s".format(
                                        p1._1, p1._2, 
                                        p3._1, p3._2,
                                        p3._1, pm._2, pm._1, pm._2, 
                                        p2._1, pm._2, p2._1, p2._2
                                )) } stroke={ col } stroke-width={ strokeWidth.toString() } style="fill:none" />
            } else {
                val pm = (0.5f * (p1._1 + p3._1), 0.5f * (p1._2 + p3._2))
                buff += <path d={ ("M %s %s Q %s %s %s %s Q %s %s %s %s L %s %s".format(
                                        p1._1, p1._2, 
                                        p1._1, pm._2, pm._1, pm._2, 
                                        p3._1, pm._2, p3._1, p3._2, 
                                        p2._1, p2._2
                                )) } stroke={ col } stroke-width={ strokeWidth.toString() } style="fill:none" />
            }
        }
        
        buff
    }
    
    /**
     * Calculate a commit's position in pixels.
     */
    private def pos(comm: Commit): (Float, Float) = {
        (offset + branchDistance * (comm.position + 1), baseScale * (tree.commits.length - comm.index))
    }
    /**
     * Calculate position in pixels.
     */
    private def pos(p: Float, idx: Float): (Float, Float) = {
        (offset + branchDistance * (p + 1), baseScale * (tree.commits.length - idx))
    }
}