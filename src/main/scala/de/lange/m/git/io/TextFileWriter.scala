package de.lange.m.git.io

import java.io.{File, PrintWriter}

/**
 * Write to plain text files.
 */
object TextFileWriter {
  
    /**
     * Write text to a new file in the given path.
     */
    def write(text: String, path: String) {
		val writer = new PrintWriter(new File(path))
		writer.write(text)
  		writer.close()
    }
}