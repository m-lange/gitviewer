package de.lange.m.git.io

import scala.xml.XML

import de.lange.m.git.data.BranchOrdering
import de.lange.m.git.settings.{ BranchingConfig, RepoConfig, TreeViewSettings }
import de.lange.m.git.util.ColorUtil
import scalafx.scene.paint.Color

class XmlRepositoryConfigReader {
    
    def read(path: String, treeSettings: TreeViewSettings): RepoConfig = {
		val doc = XML.loadFile(path)
		val settings = doc
		val login = (settings \ "login")
		val user = (login \ "@user").text
		val pw = (login \ "@password").text
		val draftMessage = (settings \ "draftMessage")
		
		val branching = (settings \ "branching")
		
		val branchConf = if(branching == null || branching.isEmpty) treeSettings.defaultBranchingConfig.clone()
		                else {
                    		val order = (for(b <- (branching \ "branchOrder" \ "branch")) yield (b \ "@prefix").text).toArray
                    		val pers  = (for(b <- (branching \ "branchPersistence" \ "branch")) yield (b \ "@prefix").text).toArray
                    		val colors = (for(b <- (branching \ "branchColors" \ "branch")) yield {
                    		        ((b \ "@prefix").text, Color.web((b \ "@color").text))
                    		    }).toMap    
            		        new BranchingConfig (
            		                new BranchOrdering(order), 
            		                new BranchOrdering(pers), 
            		                colors
            		            )                
		                }
		new RepoConfig (
		        user = user,
		        password = pw,
		        draftMessage = draftMessage.text,
		        branchConf
		    )
    }
    
    def write(conf: RepoConfig, path: String) {
        val xml =   <config>
						<login user={conf.user} password={conf.password}/>
						<draftMessage>{ conf.draftMessage }</draftMessage>
						<branching>
							<branchOrder>
								{ for( br <- conf.branching.branchOrder.order ) yield <branch prefix={br} /> }
							</branchOrder>
							<branchPersistence>
								{ for( br <- conf.branching.branchPersistence.order ) yield <branch prefix={br} /> }
							</branchPersistence>
							<branchColors>
								{ for( (br, col) <- conf.branching.branchColors ) yield <branch prefix={br} color={ColorUtil.toWeb(col)} /> }
							</branchColors>
						</branching>
					</config>
	    val str = new BetterPrettyPrinter(200, 4, true).format(xml)
	    TextFileWriter.write(str, path)
    }
    
}