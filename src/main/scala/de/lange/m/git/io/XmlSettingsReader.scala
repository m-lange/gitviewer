package de.lange.m.git.io

import java.text.SimpleDateFormat

import scala.xml.{ Node, XML }

import de.lange.m.git.data.BranchOrdering
import de.lange.m.git.settings.{ AppSettings, DiffSettings, TreeViewSettings, WindowSettings }
import de.lange.m.git.ui.layout.Layout
import scalafx.scene.paint.Color
import de.lange.m.git.settings.UpdateSettings
import de.lange.m.git.settings.BranchingConfig

/**
 * Reads application settings from an XML file.
 */
class XmlSettingsReader {
  
    /**
     * Read setting from the given path.
     */
    def read(path: String): AppSettings = {
		val doc = XML.loadFile(path)
		
		val settings = doc //(doc \ "settings")(0)
		val window = (doc \ "window")(0)
		val tree = (doc \ "tree")(0)
		val diff = (doc \ "diff")(0)
		val update = (doc \ "update")(0)
		
		val winSet = windowSettings(window)
		val treeSet = treeSettings(tree)
		val diffSet = diffSettings(diff)
		val updateSet = updateSettings(update)
		
		new AppSettings(winSet, treeSet, diffSet, updateSet)
    }
    
    private def updateSettings(update: Node): UpdateSettings = {
        val versionUrl = (update \ "versionUrl" \ "@value").text
        val downloadUrl = (update \ "downloadUrl" \ "@value").text
        val homeUrl = (update \ "homeUrl" \ "@value").text
        val gitUrl = (update \ "gitUrl" \ "@value").text
        val issuesUrl = (update \ "issuesUrl" \ "@value").text
        new UpdateSettings( 
                versionUrl,
                downloadUrl,
                homeUrl,
                gitUrl,
                issuesUrl)
    }
    private def windowSettings(window: Node): WindowSettings = {
        val layout = Layout.create( (window \ "_")(0) )
        new WindowSettings( layout )
    }
    private def treeSettings(tree: Node): TreeViewSettings = {
        val maxCommits = (tree \ "maxCommits" \ "@value").text.toInt
        val fontSize = (tree \ "fontSize" \ "@value").text.toFloat
		val fontFamily = (tree \ "fontFamily" \ "@value").text
		val horizontalSpacing = (tree \ "horizontalSpacing" \ "@value").text.toFloat
		val verticalSpacing = (tree \ "verticalSpacing" \ "@value").text.toFloat
		val textSpacing = (tree \ "textSpacing" \ "@value").text.toFloat
		val lineWidth = (tree \ "lineWidth" \ "@value").text.toFloat
		val dotSize = (tree \ "dotSize" \ "@value").text.toFloat
		val graphXOffset = (tree \ "graphXOffset" \ "@value").text.toFloat
		val divergeEarly = (tree \ "divergeEarly" \ "@value").text.toInt > 0
		val interactiveSvg = (tree \ "interactiveSvg" \ "@value").text.toInt > 0
		
		val order = (for(b <- (tree \ "branchOrder" \ "branch")) yield (b \ "@prefix").text).toArray
		val pers  = (for(b <- (tree \ "branchPersistence" \ "branch")) yield (b \ "@prefix").text).toArray
		val colors = (for(b <- (tree \ "branchColors" \ "branch")) yield {
		        ((b \ "@prefix").text, Color.web((b \ "@color").text))
		    }).toMap
		    
		val branching = new BranchingConfig( new BranchOrdering(order), new BranchOrdering(pers), colors )
		    
		val commitColor = Color.web( (tree \ "commitColor" \ "@value").text )
		val mergeColor = Color.web( (tree \ "mergeColor" \ "@value").text )
		
		val dateFormat = (tree \ "dateFormat" \ "@value").text
		
		new TreeViewSettings(
		                        maxCommits,
                    		    fontFamily,
                                fontSize,
                        		textSpacing,
                        		horizontalSpacing,
                        		verticalSpacing,
                        		lineWidth,
                        		dotSize,
                        		graphXOffset,
                        		divergeEarly,
                        		branching,
                        		commitColor,
                        		mergeColor,
                        		interactiveSvg,
                        		new SimpleDateFormat( dateFormat )
		                    )
    }
    private def diffSettings(diff: Node): DiffSettings = {
        val contextLines = (diff \ "contextLines" \ "@value").text.toInt
		val addColor = Color.web( (diff \ "addColor" \ "@value").text )
		val deleteColor = Color.web( (diff \ "deleteColor" \ "@value").text )
		val modifyColor = Color.web( (diff \ "modifyColor" \ "@value").text )
        
        new DiffSettings( 
                contextLines,
                addColor,
                deleteColor,
                modifyColor)
    }
    
	private def propertiesMap(node: scala.xml.Node): Map[String, String] = {
		(node \ "PROPERTY").map { node => {
			val str = node.text.split(" ", 2)
			(str(0), str(1))
		} }.toMap
	}
}