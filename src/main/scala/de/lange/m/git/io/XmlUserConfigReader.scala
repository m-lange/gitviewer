package de.lange.m.git.io

import scala.collection.mutable.ArrayBuffer
import scala.xml.XML

import de.lange.m.git.settings.UserConfig

/**
 * Reads user configurations from an XML file.
 */
class XmlUserConfigReader {
    
    /**
     * Read config from a given path.
     */
    def read(path: String): UserConfig = {
		val doc = XML.loadFile(path)
		
		val settings = doc
		val win = (settings \ "window")
		
		val x = (win \ "@x").text.toInt
		val y = (win \ "@y").text.toInt
		val width = (win \ "@width").text.toInt
		val height = (win \ "@height").text.toInt
		val maximize = (win \ "@maximized").text.toBoolean
		val repositories = new ArrayBuffer[String]()
		repositories ++= (settings \ "repositories" \ "repository").map( (s) => (s \ "@path").text )
		
		new UserConfig(     x,
		                    y,
		                    width,
		                    height,
		                    maximize,
		                    repositories )
    }
    
    /**
     * Write config to a given path.
     */
    def write(conf: UserConfig, path: String) {
        val xml =   <config>
						<window x={conf.windowX.toString()} 
								y={conf.windowY.toString()} 
								width={conf.windowWidth.toString()} 
								height={conf.windowHeight.toString()} 
								maximized={conf.windowMaximized.toString()} />
						<repositories>
							{ for(s <- conf.repositories) yield <repository path={s} /> }
						</repositories>
					</config>
	    val str = new BetterPrettyPrinter(200, 4, true).format(xml)
	    TextFileWriter.write(str, path)
    }
}