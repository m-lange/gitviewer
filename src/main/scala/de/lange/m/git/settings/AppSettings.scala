package de.lange.m.git.settings

/**
 * The application's settings
 */
class AppSettings(
            val window: WindowSettings,
            val tree: TreeViewSettings,
            val diff: DiffSettings,
            val update: UpdateSettings
        ) {
  
}