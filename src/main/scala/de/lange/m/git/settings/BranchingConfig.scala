package de.lange.m.git.settings

import de.lange.m.git.data.BranchOrdering
import scalafx.scene.paint.Color

/**
 * BranchingConfig helpers
 */
object BranchingConfig {
    
    /**
     * Construct a default config.
     */
    /*
    def apply() = {
        new BranchingConfig (
            branchOrder = new BranchOrdering( Array(
                    "master", 
                    "hotfix", 
                    "dev") ),
            branchPersistence = new BranchOrdering( Array(
                    "master", 
                    "dev", 
                    "feature",
                    "release",
                    "hotfix",
                    "bugfix") ),
            branchColors = Map(
                    "master"  -> Color.Blue,
                    "dev"     -> Color.Orange,
                    "feature" -> Color.Purple,
                    "release" -> Color.Green,
                    "bugfix"  -> Color.Red,
                    "hotfix"  -> Color.Red
                )
        )
    }
    * 
    */
    
}
class BranchingConfig (  
    		val branchOrder: BranchOrdering,
    		val branchPersistence: BranchOrdering,
    		val branchColors: Map[String, Color] ) {
    
    override def clone(): BranchingConfig = {
        new BranchingConfig ( 
                branchOrder.clone(),
                branchPersistence.clone(),
                (for( (b,c) <- branchColors ) yield (b, c)).toMap )
    }
    
    override def toString(): String = {
        "Order: "+branchOrder.order.mkString(" ")+"\n"+
        "Persi: "+branchPersistence.order.mkString(" ")
    }
}