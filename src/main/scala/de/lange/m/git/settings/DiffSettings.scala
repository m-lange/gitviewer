package de.lange.m.git.settings

import scalafx.scene.paint.Color

/**
 * Settings for diff view
 * 
 * @constructor Construct new settings.
 * @param contextLines number of lines to show around changes
 * @param addColor color for added lines, as well as added files
 * @param deleteColor color for deleted lines, as well as deleted files
 * @param modifyColor color for modified files
 */
class DiffSettings(
            val contextLines: Int,
            val addColor: Color,
            val deleteColor: Color,
            val modifyColor: Color
        ) {
  
}