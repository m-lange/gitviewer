package de.lange.m.git.settings

/**
 * RepoConfig helpers
 */
object RepoConfig {
    
    /**
     * Construct a default config.
     */
    def apply(settings: TreeViewSettings) = {
        new RepoConfig(
            user = "",
            password = "",
            draftMessage = "",
            settings.defaultBranchingConfig.clone()
        )
    }
    
}

class RepoConfig(
            var user: String,
            var password: String,
            var draftMessage: String,
            var branching: BranchingConfig
        ) {
  
}