package de.lange.m.git.settings

import java.text.DateFormat

import de.lange.m.git.data.BranchOrdering
import scalafx.scene.paint.Color

/**
 * Settings for tree view
 * 
 * @constructor Construct new settings.
 * @param maxCommits maximum number of commits to load
 * @param fontFamily font family for the tree view
 * @param fontSize font size for the tree view
 * @param textSpacing spacing between graphics and text
 * @param horizontalSpacing horizontal spacing between commits/branche traces
 * @param verticalSpacing vertical spacing between commits/rows
 * @param lineWidth line width commit of connections
 * @param dotSize size of commit dots
 * @param graphXOffset offset of graphical tree representation from the left edge
 * @param divergeEarly preferably diverge branch early (not implemented!)
 * @param branchOrder horizontal ordering of branches by prefix
 * @param branchPersistence persistence ranking of branches by prefix
 * @param branchColors colors of branches by prefix
 * @param commitColor text color for normal commits
 * @param mergeColor text color for merge commits
 * @param interactiveSvg if the SVG should be interctive
 * @param dateFormat data format string
 */
class TreeViewSettings(
            val maxCommits: Int,
    		val fontFamily: String,
            val fontSize: Float,
    		val textSpacing: Float,
    		val horizontalSpacing: Float,
    		val verticalSpacing: Float,
    		val lineWidth: Float,
    		val dotSize: Float,
    		val graphXOffset: Float,
    		val divergeEarly: Boolean,
    		val defaultBranchingConfig: BranchingConfig,
    		val commitColor: Color,
    		val mergeColor: Color,
    		val interactiveSvg: Boolean,
    		val dateFormat: DateFormat
        ) {
  
}