package de.lange.m.git.settings

/**
 * Settings for updating/version checks
 * 
 * @constructor Construct new settings.
 * @param versionUrl url of the version page
 * @param homeUrl software home page
 * @param gitUrl software git repository
 */
class UpdateSettings(
            val versionUrl: String,
            val downloadUrl: String,
            val homeUrl: String,
            val gitUrl: String,
            val issuesUrl: String
        ) {
  
}