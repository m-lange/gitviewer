package de.lange.m.git.settings

import scala.collection.mutable.ArrayBuffer

/**
 * UserConfig helpers
 */
object UserConfig {
    
    /**
     * Construct a default config.
     */
    def apply() = {
        new UserConfig(
            windowX = -1,
            windowY = -1,
            windowWidth = 1024,
            windowHeight = 800,
            windowMaximized = false,
            repositories = new ArrayBuffer[String]()
        )
    }
    
}

/**
 * User specific configuration
 * 
 * @constructor Construct new config.
 * @param windowX window x position
 * @param windowY window y position
 * @param windowWidth window width
 * @param windowHeight window height
 * @param windowMaximized window is maximized
 * @param repositories list of open repository paths
 */
class UserConfig(
            var windowX: Int,
            var windowY: Int,
            var windowWidth: Int,
            var windowHeight: Int,
            var windowMaximized: Boolean,
            val repositories: ArrayBuffer[String]
        ) {
  
}