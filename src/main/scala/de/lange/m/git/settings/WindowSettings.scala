package de.lange.m.git.settings

import de.lange.m.git.ui.layout.Layout

/**
 * Settings for application window layout
 * 
 * @constructor Construct new settings.
 * @param layout the layout
 */
class WindowSettings(
            val layout: Layout
        ) {
  
}