package de.lange.m.git.ui

import java.io.File
import java.net.URL

import de.lange.m.git.{ Paths, Version }
import de.lange.m.git.settings.UpdateSettings
import de.lange.m.git.util.{ ExternalProgsUtil, ResourceUtil }
import scalafx.Includes.{ double2DurationHelper, handle, observableList2ObservableBuffer }
import scalafx.animation.FadeTransition
import scalafx.geometry.{ HPos, Insets, Orientation, Pos }
import scalafx.scene.Scene
import scalafx.scene.control.{ Hyperlink, Label, Separator }
import scalafx.scene.effect.DropShadow
import scalafx.scene.image.{ Image, ImageView }
import scalafx.scene.layout.{ GridPane, HBox, Priority, VBox }
import scalafx.stage.{ Screen, Stage, StageStyle }

/**
  */
class AboutFrame(val settings: UpdateSettings) {
    frame =>

    private val image = new Image( ResourceUtil.getStream( Paths.splashPath ) )
    private val icon = new Image( ResourceUtil.getStream( Paths.appIconPath ) )
  
    private val splashWidth = image.width()
    private val splashHeight = image.height()
    
    private val splashStage = new Stage()
  
    private val splash = new ImageView() {
        image = frame.image
        margin = Insets(0,0,5,0)
    }
    private val spacer = new Separator() {
        
    }
    private val leftText = new Label() {
        text = "GitViewer"
        alignment = Pos.Center
    }
    private val versionText = new Label() {
        text = "Version "+Version.version.toString()
        alignment = Pos.Center
    }
    private val authorText = new Label() {
        text =  "Author: M. Lange"
        alignment = Pos.Center
        hgrow = Priority.Always
    }
    private val links = new HBox() {
        val sep = new Separator()
        sep.orientation() = Orientation.Vertical
        children ++= Seq(
                    new Hyperlink(){ 
                            text = "Home page"
                            onAction = evt => {
                                ExternalProgsUtil.openUrl( new URL(settings.homeUrl) )
                                closeFrame()
                            }
                        }.delegate,
                    new Label(" "),
                    new Hyperlink(){ 
                            text = "Help"
                            onAction = evt => {
                                ExternalProgsUtil.openFile( new File(Paths.appPath+"\\"+Paths.helpPath) )
                                closeFrame()
                            }
                        }.delegate,
                    new Label(" "),
                    new Hyperlink(){ 
                            text = "Change log"
                            onAction = evt => {
                                ExternalProgsUtil.openFile( new File(Paths.appPath+"\\"+Paths.changelogPath) )
                                closeFrame()
                            }
                        }.delegate,
                    new Label(" "),
                    new Hyperlink(){ 
                            text = "Git repository"
                            onAction = evt => {
                                ExternalProgsUtil.openUrl( new URL(settings.gitUrl) )
                                closeFrame()
                            }
                        }.delegate,
                    new Label(" "),
                    new Hyperlink(){ 
                            text = "Download"
                            onAction = evt => {
                                ExternalProgsUtil.openUrl( new URL(settings.downloadUrl) )
                                closeFrame()
                            }
                        }.delegate
                )
        hgrow = Priority.Always
    }
    
    
    private val textBox = new GridPane {
        margin = Insets(0,0,5,0)
        add(leftText, 0, 0)
        add(versionText, 1, 0)
        prefWidth() = splashWidth
        hgrow = Priority.Always
        GridPane.setHalignment(versionText, HPos.Right)
        GridPane.setHgrow(leftText, Priority.Always)
    }
    private val splashLayout = new VBox() {
        children ++= Seq(splash, spacer, textBox, authorText, links)
        style =
          "-fx-padding: 5; " +
            "-fx-background-color: white; " + // cornsilk
            "-fx-border-width:5; " +
            "-fx-border-color: " +
            "linear-gradient(" +
            "to bottom, " +
            "#ff9600, " +
            "derive(#ff9600, 50%)" +
            ");"
        effect = new DropShadow()
    }

    def show(): Unit = {
        splashStage.initStyle(StageStyle.Undecorated)
        splashStage.icons.addAll( icon )
        splashStage.scene = new Scene {
            root = splashLayout
            onMouseClicked = evt => {
                closeFrame()
            }
        }
        val bounds = Screen.primary.bounds
        splashStage.x = bounds.minX + bounds.width / 2 - splashWidth / 2
        splashStage.y = bounds.minY + bounds.height / 2 - splashHeight / 2
        
        splashStage.
				focused.onChange( (op, oldVal, newVal) => {
				    if( ! newVal ) {
				        closeFrame()
				    }
				})
        
        splashStage.show()
        splashStage.alwaysOnTop = true
        splashStage.alwaysOnTop = false
    }
    
    def closeFrame() {
        new FadeTransition(0.33.s, splashLayout) {
                    fromValue = 1.0
                    toValue = 0.0
                    onFinished = handle {
                      splashStage.hide()
                      splashStage.close()
                    }
                }.play()
    }
}