package de.lange.m.git.ui

import org.eclipse.jgit.lib.ProgressMonitor

import javafx.{ concurrent => jfxr }
import scalafx.concurrent.{ Task, Worker }


object FXWorker {
    
    def apply[T]( task: ((String, Double, Double) => Unit) => T, onSuccess: (T) => Unit ): FXWorker[T] = {
        val loaderTask = new FXWorker(task)
        loaderTask.state.onChange { (_, _, newState) =>
            newState match {
                case Worker.State.Succeeded.delegate =>
                    onSuccess( loaderTask.getValue )
                case _ =>
                    // TODO: handle other states
            }
        }
        loaderTask
    }
    
    def apply[T]( task: () => T, onSuccess: (T) => Unit ): SimpleFXWorker[T] = {
        val loaderTask = new SimpleFXWorker(task)
        loaderTask.state.onChange { (_, _, newState) =>
            newState match {
                case Worker.State.Succeeded.delegate =>
                    onSuccess( loaderTask.getValue )
                    
                case _ =>
                    // TODO: handle other states
            }
        }
        loaderTask
    }
    def apply[T]( task: (ProgressMonitor) => T, onSuccess: (T) => Unit, monitor: ProgressMonitor ): ProgressMonitorFXWorker[T] = {
        val loaderTask = new ProgressMonitorFXWorker(task, monitor)
        loaderTask.state.onChange { (_, _, newState) =>
            newState match {
                case Worker.State.Succeeded.delegate =>
                    onSuccess( loaderTask.getValue )
                case _ =>
                    // TODO: handle other states
            }
        }
        loaderTask
    }
}

class FXWorker[T] (
            val task: ((String, Double, Double) => Unit) => T
        ) extends Task(new jfxr.Task[T]() {
    
    override protected def call: T = {
        val res: T = task( update _ )
        res
    }
    
    def update(message: String, v: Double, max: Double) {
        if(message != null) {
            updateMessage(message)
        }
        if(max > 0) {
            updateProgress(v, max)
        }
    }
}) {
    def start() {
        new Thread(this).start()
    }
}

class SimpleFXWorker[T] (
            val task: () => T
        ) extends Task(new jfxr.Task[T]() {
    
    override protected def call: T = {
        val res: T = task()
        res
    }
    
    def update(message: String, v: Double, max: Double) {
        if(message != null) {
            updateMessage(message)
        }
        if(max > 0) {
            updateProgress(v, max)
        }
    }
}) {
    def start() {
        new Thread(this).start()
    }
}
abstract class JFXTask[T] extends jfxr.Task[T] {
    def update(message: String, v: Double, max: Double) {
        if(message != null) {
            updateMessage(message)
        }
        if(max > 0) {
            updateProgress(v, max)
        }
    }
}
class ProgressMonitorFXWorker[T] (
            val task: (ProgressMonitor) => T,
            val monitor: ProgressMonitor
        ) extends Task(new JFXTask[T]() {
    
    override protected def call: T = {
        val res: T = task(monitor)
        res
    }
}) {
    def start() {
        new Thread(this).start()
    }
    def update(message: String, v: Double, max: Double) {
        delegate.asInstanceOf[JFXTask[T]].update(message, v, max)
    }
}
