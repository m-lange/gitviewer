package de.lange.m.git.ui

import java.io.File

import scalafx.stage.{ DirectoryChooser, FileChooser }
import scalafx.stage.FileChooser.ExtensionFilter

/**
 * Helper for file and directory selection dialogs
 */
object FileSelect {
  
    /**
     * Select a directory via a dialog.
     */
	def selectDirectory(text: String, path: String) : File = {
		val fileChooser = new DirectoryChooser {
			title = text
		}
		if(path != null) {
		    fileChooser.initialDirectory = new File(path)
		}
		val file = fileChooser.showDialog(null)
		file
	}
    
    /**
     * Select a file via an open dialog.
     */
	def selectFileOpen(text: String, fileType: String, extensions: Array[String]) : File = {
		val fileChooser = new FileChooser() {
			title = text
			extensionFilters.addAll( new ExtensionFilter(fileType, extensions) );
		}
		val file = fileChooser.showOpenDialog(null)
		file
	}
	
    /**
     * Select a file via a save dialog.
     */
	def selectFileSave(text: String, fileType: String, extensions: Array[String]) : File = {
		val fileChooser = new FileChooser() {
			title = text
			extensionFilters.addAll( new ExtensionFilter(fileType, extensions) );
		}
		val file = fileChooser.showSaveDialog(null)
		file
	}
	
}