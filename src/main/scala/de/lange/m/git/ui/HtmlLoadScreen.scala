package de.lange.m.git.ui

object HtmlLoadScreen {
    
    def apply() = html
    
    val html: String = {
        
        val xml = 
        <html>
			<head><style>{"""
                .container {
                    width: 100%;
                    height: 100%;
                }
				.loader {
                    position: absolute;
                    left: 50%;
                    top: 50%;
                    margin-top: -41px;
                    margin-left: -41px;
                    border: 16px solid #f3f3f3; /* Light grey */
                    border-top: 16px solid #3498db; /* Blue */
                    border-radius: 50%;
                    width: 50px;
                    height: 50px;
                    animation: spin 1s linear infinite;
                }

				@keyframes spin {
                    0% { transform: rotate(0deg); }
                    100% { transform: rotate(360deg); }
                }
			"""}</style></head>
			<body>
				 <div class="container"><div class="loader"></div></div>
			</body>
		</html>
        
        xml.toString()
    }
}