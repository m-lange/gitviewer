package de.lange.m.git.ui

import scala.reflect._
import scala.annotation._

import scala.reflect.runtime.{ universe => ru }

/**
 * Frequent helper functions
 */
object Includes {
    
	/**
	 * Convert a type to a class
	 */
	def toClass[T : ClassTag] : Class[T] = {
		classTag[T].runtimeClass.asInstanceOf[Class[T]]
	}
	/**
	 * Convert a class to a reflection type
	 */
	def getType[T](clazz: Class[T]): ru.Type = {
  		val runtimeMirror =  ru.runtimeMirror(clazz.getClassLoader)
  		runtimeMirror.classSymbol(clazz).toType
	}
}