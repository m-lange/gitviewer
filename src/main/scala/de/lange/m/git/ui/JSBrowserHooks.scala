package de.lange.m.git.ui

/**
 * Hooks to expose Scala functionality to JavaScript
 * 
 * @constructor Construct a new hooks instance
 * @param frame the RepositoryPanel for which to create the hooks
 */
class JSBrowserHooks(
            val frame: RepositoryPanel
        ) {
    
    /** Print to Scala output */
    def println(str: String) {
        Predef.println(str)
    }
    /** Show diff for commit hashes and file path */
    def showDiff(commit: String, parent: String, path: String) {
        frame.showDiff(commit, parent, path)
    }
    /** Show diff for uncommitted changes and file path */
    def showDiffStaging(path: String) {
        frame.showDiff(path)
    }
    def clearDiff() {
        frame.clearDiff()
    }
    /** Set selection by commit hashes */
    def setSelection(commitA: String, commitB: String) {
        frame.setSelection(
                if(commitA == null) null else commitA.replace("row-", ""), 
                if(commitB == null) null else commitB.replace("row-", ""))
    }
    /** Update selection by commit hashes */
    def updateSelection(selA: String, selB: String) {
        frame.updateSelection(selA, selB)
    }
    /** Get the current selection */
    def getSelection(): Array[String] = {
        frame.getSelection()
    }
    /** Get the currently selected file. */
    def getSelectedFile(): String = {
        frame.getSelectedFile()
    }
    /** Get the currently selected staging file. */
    def getSelectedStaging(): String = {
        frame.getSelectedStaging()
    }
    
    def contextMenuBranch(name: String, isRemote: Boolean, localOnly: Boolean, screenX: Double, screenY: Double) {
        frame.showContextMenuBranch(name, isRemote, localOnly, screenX, screenY)
    }
    def contextMenuTag(name: String, commit: String, screenX: Double, screenY: Double) {
        frame.showContextMenuTag(name, commit, screenX, screenY)
    }
    def contextMenuCommit(id: String, screenX: Double, screenY: Double) {
        frame.showContextMenuCommit(id, screenX, screenY)
    }
    
    def loadMoreCommits() = frame.loadMoreCommits()
    
    def getLastScrollPosition(): Int = frame.getLastScrollPosition()
    
    def checkoutBranch(name: String, isRemote: Boolean) = frame.checkoutBranch(name, isRemote)
    def checkoutCommit(name: String) = frame.checkoutCommit(name)
    def stageSelected() = frame.stageSelected()
    def stageAll() = frame.stageAll()
    def unstageSelected() = frame.unstageSelected()
    def unstageAll() = frame.unstageAll()
    
    def saveDraftMessage(message: String) = frame.saveDraftMessage(message)
    
    def commit() = frame.commit()
        
}