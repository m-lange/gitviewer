package de.lange.m.git.ui

import java.io.File
import java.net.URL

import scala.collection.JavaConverters.asScalaBufferConverter

import com.sun.javafx.webkit.WebConsoleListener
import scala.collection.JavaConverters._

import de.lange.m.git.{ Paths, Version }
import de.lange.m.git.data.GitTree
import de.lange.m.git.io.XmlUserConfigReader
import de.lange.m.git.settings.{ AppSettings, UserConfig }
import de.lange.m.git.ui.event.StatusListener
import de.lange.m.git.update.UpdateManager
import de.lange.m.git.util.{ ExternalProgsUtil, GitUtil, ResourceUtil }
import javafx.scene.{ control => jfxc }
import javafx.scene.control.Separator
import javafx.scene.image.{ Image => JFXImage }
import scalafx.application.Platform
import scalafx.geometry.Pos
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.control.{ Button, Hyperlink, Label, Menu, MenuBar, MenuItem, SeparatorMenuItem, Tab, TabPane, ToolBar, Tooltip }
import scalafx.scene.layout.{ BorderPane, HBox, Priority, VBox }
import scalafx.scene.paint.Color
import scalafx.stage.Stage
import de.lange.m.git.util.DialogUtil
import de.lange.m.git.data.Uncommitted

/**
 * Application main frame
 * 
 * @constructor Create a new frame.
 * @param settings application settings
 * @param config user settings
 */
class MainFrame(
        val settings: AppSettings,
        val config: UserConfig ) extends UiFrame with StatusListener {
    frame =>
    
    private val cssPath = Paths.cssPath+"/MainFrame.css"
        
    private val iconImage = Paths.appIconPath
    
    /** Last directory opened in repository selection dialog */
    private var lastDirectory: String = null
    
    /** Currently open about frame */
    private var about: AboutFrame = null
    
	override protected def createStage(): Stage = {
		val stage = new Stage {
			if(config.windowX >= 0) x = config.windowX
			if(config.windowY >= 0) y = config.windowY
			width = config.windowWidth
			height = config.windowHeight
			maximized = config.windowMaximized
			title.value = "GitViewer "+Version.version
			icons.addAll( new JFXImage( ResourceUtil.getStream(iconImage) ) )
			scene = new Scene {
				fill = Color.White
				root = new BorderPane {
				    center = mainPanel 
				    top = topPanel
				    bottom = statusBar
				}
				stylesheets.add(getClass().getResource("/"+cssPath).toExternalForm())
			}
			position.set(x(), y())
			
			WebConsoleListener.setDefaultListener(new WebConsoleListener(){
                override def messageAdded(webView: javafx.scene.web.WebView, message: String, lineNumber: Int, sourceId: String) {
                    println("JS: [" + sourceId + ":" + lineNumber + "] " + message)
                }
            })
			
            onCloseRequest = { evt => {
                onApplicationQuit()
            } }
            
			show()
		}
		showRepositories( config.repositories )
		checkForUpdates()
		stage
	}
    
    /**
     * Show repositories.
     * 
     * @param repos list of repository paths (.git folders) to show in tabs
     */
    def showRepositories(repos: Seq[String]) {
        for( path <- repos ) {
            getTab(path) match {
                case Some(tab) => mainPanel.selectionModel().select(tab)
                case None => showRepository(path, false)
            }
        }
    }
	
    /**
     * Check for updates in a background thread.
     */
	private def checkForUpdates() {
	    val backgroundThread = new Thread {
            setDaemon(true)
            override def run = {
    	        val (ver, err) = new UpdateManager(settings.update).updateAvailable
    	        Platform.runLater {
        	        ver match { 
        	            case Some( v ) =>
        	                updateLabel.text = "Update available: Version "+v.toString()
        	                updateLabel.style = "-fx-padding: 0 10; -fx-background-color: #d0ffd0;"
        	            case None =>
        	                err match {
        	                    case Some( msg ) =>
        	                    updateLabel.text = "Update site not available!"
        	                    updateLabel.style = "-fx-padding: 0 10; -fx-background-color: #ffd0ff;"
        	                    case None => 
        	                        updateLabel.style = "-fx-padding: 0;"
        	                }
        	        }
    	        }
            }
	    }
        backgroundThread.start()
	}
	
	/**
	 * Show the given tree's repository in a new tab
	 * 
	 * @param tree GitTree to show
	 * @param select Select the new tab
	 */
	def showRepository(tree: GitTree, select: Boolean) {
	    val content = new RepositoryPanel( tree, settings, config, this )
	    showRepository(tree.name, content, select)
	}
	
	/**
	 * Show the repository at the given path in a new tab
	 * 
	 * @param tree GitTree to show
	 * @param select Select the new tab
	 */
	def showRepository(path: String, select: Boolean) {
	    val file = new File(path)
        if( GitUtil.isRepository( file ) ) {
    	    val name = file.getAbsoluteFile.getParentFile.getName
    	    val content = new RepositoryPanel( path, settings, config, this )
    	    showRepository(name, content, select)
        } else {
            println("Not a repository: "+file.getAbsolutePath)
        }
	}
	
	private def showRepository(name: String, content: RepositoryPanel, select: Boolean) {
	    val tab = new Tab()
	    tab.setText(name)
	    tab.setContent( content )
	    
	    tab.onClosed = ( (evt) => {
	        content.onClosed()
	    })
	    tab.onSelectionChanged = ( (evt) => {
	        content.onSelectionChanged(evt, tab.isSelected())
	    })
	    
	    mainPanel.tabs.addAll( tab )
	    if(select) {
	        mainPanel.getSelectionModel().select(tab)
	    }
	}
	
	/**
	 * Called on stage close.
	 * 
	 * Save UserConfig and cancel timers for repository change detection.
	 */
	protected def onApplicationQuit() {
	    val max = stage.maximized()
	    stage.maximized = false
        updateUserConfig(max)
        new XmlUserConfigReader().write(config, Paths.userConfigPath)
        
        if(about != null) about.closeFrame()
        
        for(tab <- mainPanel.tabs.asScala) {
            tab.getContent match {
                case rp: RepositoryPanel => rp.onClosed()
                case _ => 
            }
        }
	    Platform.runLater {
	        Platform.exit()
	        System.exit(0)
	    }
	}
	
	/**
	 * Find the tab for a given repository path if it is already opened.
	 */
	private def getTab(path: String): Option[jfxc.Tab] = {
	    for(tab <- mainPanel.tabs.asScala) {
            tab.getContent match {
                case rp: RepositoryPanel => if(rp.path == path) return Some(tab)
                case _ => 
            }
        }
	    return None
	}
	
	/**
	 * Update the UserConfig
	 */
	protected def updateUserConfig(maximized: Boolean) {
	    config.windowX = stage.x().toInt
	    config.windowY = stage.y().toInt
	    config.windowWidth = stage.width().toInt
	    config.windowHeight = stage.height().toInt
	    config.windowMaximized = maximized
	    
	    config.repositories.clear()
	    config.repositories ++= (for( tab <- mainPanel.tabs.asScala if tab.getContent.isInstanceOf[RepositoryPanel] ) 
	                                yield tab.getContent.asInstanceOf[RepositoryPanel].path)
	}
	
	/**
	 * Open a repository via file selection dialog.
	 * 
	 * Switches to the respective tab if the repository is already opened.
	 */
	private def openRepository() {
	    val file = FileSelect.selectDirectory("Select a Git repository", lastDirectory)
	    if(file != null) {
            val basePath = file.getAbsolutePath()
		    val dir = Paths.gitDataDir
		    lastDirectory = file.getParentFile.getAbsolutePath
		    val path = if(basePath.endsWith(".git")) basePath else (basePath+"\\"+dir)
            if( GitUtil.isRepository( new File(path) ) ) {
                getTab(path) match {
                    case Some(tab) => mainPanel.selectionModel().select(tab)
                    case None => openRepository(path)
                }
            } else {
                DialogUtil.error("Unable to open", "Unable to open repository.", "Not a repository: "+file.getAbsolutePath)
            }
	    }
	}
	
	/**
	 * Clone a repository.
	 * 
	 * Switches to the respective tab if the repository is already opened.
	 */
	private def cloneRepository() {
	    cloneRepositoryAsync()
	}
	private def cloneRepositoryAsync() {
	    val opt = DialogUtil.clone("Clone repository", "Clone a remote repository.")
	    opt match {
	        case None =>
	            
	        case Some( result ) =>
	            val target = new File(result.path)
	            if( ! target.exists() ) {
	                DialogUtil.error("Clone failed", "Clone failed: Target directory does not exist.", "Directory does not exist: "+result.path)
	            } else if( ! target.isDirectory() ) {
	                DialogUtil.error("Clone failed", "Clone failed: Target is not a directory.", "Target is not a directory: "+result.path)
	            } else {
	                val isOk = if( target.list().length > 0 ) {
            	                    DialogUtil.confirm("Directory not empty", 
        	                                "The target directory is not empty!", 
        	                                "Directory "+result.path+" is not empty! Clone anyway?")
            	                } else { true }
	                if( isOk ) {
	                    DialogUtil.runWithProgressResult {
	                        GitUtil.clone(result.url, result.path, this)
	                    }{ (gitOpt) =>
    	                    gitOpt match {
    	                        case None =>
    	                        case Some(git) =>
    	                            frame.openRepository( result.path )
    	                    }
	                    }
	                }
	            }
	    }
	}
	/*
	private def cloneRepositoryAsync()( progress: (String, Double, Double) => Unit ) {
	    val opt = DialogUtil.async( () => DialogUtil.clone("Clone repository", "Clone a remote repository.") )
	    opt match {
	        case None =>
	            
	        case Some( result ) =>
	            val target = new File(result.path)
	            if( ! target.exists() ) {
	                DialogUtil.async( () => DialogUtil.error("Clone failed", "Clone failed: Target directory does not exist.", "Directory does not exist: "+result.path) )
	            } else if( ! target.isDirectory() ) {
	                DialogUtil.async( () => DialogUtil.error("Clone failed", "Clone failed: Target is not a directory.", "Target is not a directory: "+result.path) )
	            } else {
	                val isOk = if( target.list().length > 0 ) {
            	                    DialogUtil.async( () => DialogUtil.confirm("Directory not empty", 
        	                                "The target directory is not empty!", 
        	                                "Directory "+result.path+" is not empty! Clone anyway?") )
            	                } else { true }
	                if( isOk ) {
	                    val gitOpt = DialogUtil.async( () => GitUtil.clone(result.url, result.path, this)( progress ) )
	                    gitOpt match {
	                        case None =>
	                        case Some(git) =>
	                            frame.openRepository( result.path )
	                    }
	                }
	            }
	    }
	}*/
	
	/**
	 * Open the repository at the given path (.git folder).
	 * 
	 * Switches to the respective tab if the repository is already opened.
	 */
	def openRepository(path: String) {
		val dir = Paths.gitDataDir
	    val fullPath = if(path.endsWith(".git")) path else (path+"\\"+dir)
	    val file = new File(fullPath)
        if( GitUtil.isRepository(file) ) {
            getTab(file.getAbsolutePath) match {
                    case Some(tab) => mainPanel.selectionModel().select(tab)
                    case None => 
                        try {
                            val loadTask = new TreeWorker(fullPath, settings.tree, settings.tree.maxCommits)
                            Platform.runLater {
                                new Splash().show( loadTask, () => showRepository(loadTask.value.value, true) )
                                new Thread(loadTask).start()    
                            }
                        } catch {
                            case e: Exception =>
                                DialogUtil.async( () => DialogUtil.error("Error reading repository", "Error reading repository: "+e.getClass.getSimpleName, 
                                                    e.getMessage ) )
                        }
                }
        } else {
            DialogUtil.async( () => DialogUtil.error("Unable to open", "Unable to open repository.", "Not a repository: "+file.getAbsolutePath) )
        }
	}
	
	def currentRepository(): GitTree = {
	    val tabs = mainPanel.tabs.asScala
	    if(tabs.isEmpty) {
	        null
	    } else {
	        mainPanel.selectionModel().getSelectedItem.getContent.asInstanceOf[RepositoryPanel].tree
	    }
	}
	private def currentRepositoryPanel(): RepositoryPanel = {
	    val tabs = mainPanel.tabs.asScala
	    if(tabs.isEmpty) {
	        null
	    } else {
	        mainPanel.selectionModel().getSelectedItem.getContent.asInstanceOf[RepositoryPanel]
	    }
	}
	
	def toFront() {
	    Platform.runLater { 
	        if(stage != null) {
	            stage.setAlwaysOnTop(true)
                stage.setAlwaysOnTop(false)
                stage.toFront()
	        }
	    }
	}
	
	/**
	 * Show help files in external browser.
	 */
	private def showHelp() {
	    val helpFile = new File(Paths.appPath+"\\"+Paths.helpPath)
	    ExternalProgsUtil.openFile(helpFile)
	}
	
	/**
	 * Show changelog in external browser.
	 */
	private def showChanges() {
	    val changelogFile = new File(Paths.appPath+"\\"+Paths.changelogPath)
	    ExternalProgsUtil.openFile(changelogFile)
	}
	
	/**
	 * Show home page in external browser.
	 */
	private def showHomePage() {
	    ExternalProgsUtil.openUrl( new URL( settings.update.homeUrl ) )
	}
	
	/**
	 * Show git repository in external browser.
	 */
	private def showGitPage() {
	    ExternalProgsUtil.openUrl( new URL( settings.update.gitUrl ) )
	}
	
	/**
	 * Show issue tracker.
	 */
	private def showIssuesPage() {
	    ExternalProgsUtil.openUrl( new URL( settings.update.issuesUrl ) )
	}
	
	/**
	 * Show about frame.
	 */
	private def showAbout() {
	    if( about != null ) about.closeFrame()
	    about = new AboutFrame(settings.update)
	    about.show()
	}
	
	override def updateStatus(status: String) {
	    Platform.runLater {
	        statusBar.text() = status
	    }
	}
	
	private lazy val mainPanel = new TabPane {
	                
	            }
	private lazy val topPanel = new VBox {
	                    children.addAll(menuBarPane, toolBars)
	            }
	private lazy val menuBarPane = new HBox {
	                    children.addAll(
	                            menuBar, updateLabel
	                        )
	            }
	
	private lazy val menuBar = new MenuBar {
	                    menus.addAll( 
	                            fileMenu, 
	                            repositoryMenu,
	                            helpMenu )
	                    this.hgrow = Priority.Always
	            }
	private lazy val fileMenu = new Menu {
	                    text = "File"
	                    items.addAll( 
	                            cloneMenuItem,
	                            openMenuItem )
	            }
	private lazy val cloneMenuItem = new MenuItem {
	                    text = "Clone..."
	                    graphic = ResourceUtil.getIconImage("clone.png")
	                    onAction = (evt) => {
	                        cloneRepository()
	                    }
	            }
	private lazy val openMenuItem = new MenuItem {
	                    text = "Open..."
	                    graphic = ResourceUtil.getIconImage("opentype.png")
	                    onAction = (evt) => {
	                        openRepository()
	                    }
	            }
	
	private lazy val repositoryMenu = new Menu {
	                    text = "Repository"
	                    items.addAll( 
	                            reloadRepositoryItem,
	                            new SeparatorMenuItem(),
	                            clearLoginItem,
	                    )
	            }
	private lazy val reloadRepositoryItem = new MenuItem {
	                    text = "Reload..."
	                    graphic = ResourceUtil.getIconImage("refresh.png")
	                    onAction = (evt) => {
	                        val repo = currentRepositoryPanel()
	                        if(repo != null) {
	                            repo.reloadRepository()
	                        }
	                    }
	            }
	private lazy val clearLoginItem = new MenuItem {
	                    text = "Clear login information"
	                    graphic = ResourceUtil.getIconImage("clear.png")
	                    onAction = (evt) => {
	                        val repo = currentRepository()
	                        if(repo != null) {
	                            GitUtil.clearLogin(repo, frame)
	                        }
	                    }
	            }
	
	
	private lazy val helpMenu = new Menu {
	                    text = "Help"
	                    items.addAll( 
	                            helpMenuItem,
	                            changelogMenuItem,
	                            new SeparatorMenuItem(),
	                            homePageMenuItem,
	                            gitMenuItem,
	                            reportBugMenuItem,
	                            new SeparatorMenuItem(),
	                            aboutMenuItem )
	            }
	private lazy val helpMenuItem = new MenuItem {
	                    text = "Help..."
	                    graphic = ResourceUtil.getIconImage("help.png")
	                    onAction = (evt) => {
	                        showHelp()
	                    }
	            }
	private lazy val changelogMenuItem = new MenuItem {
	                    text = "Changes..."
	                    graphic = ResourceUtil.getIconImage("changelog_obj.png")
	                    onAction = (evt) => {
	                        showChanges()
	                    }
	            }
	private lazy val homePageMenuItem = new MenuItem {
	                    text = "Home page..."
	                    graphic = ResourceUtil.getIconImage("homepage.png")
	                    onAction = (evt) => {
	                        showHomePage()
	                    }
	            }
	private lazy val gitMenuItem = new MenuItem {
	                    text = "Git repository..."
	                    graphic = ResourceUtil.getIconImage("gitrepository.png")
	                    onAction = (evt) => {
	                        showGitPage()
	                    }
	            }
	private lazy val reportBugMenuItem = new MenuItem {
	                    text = "Report a bug..."
	                    graphic = ResourceUtil.getIconImage("bug.png")
	                    onAction = (evt) => {
	                        showIssuesPage()
	                    }
	            }
	private lazy val aboutMenuItem = new MenuItem {
	                    text = "About GitViewer"
	                    graphic = ResourceUtil.getIconImage("about.png")
	                    onAction = (evt) => {
	                        showAbout()
	                    }
	            }
	
	private lazy val updateLabel = new Hyperlink {
	                    text = ""
	                    tooltip = new Tooltip("Open download page...")
	                    alignment = Pos.Center
	                    vgrow = Priority.Always
	                    textFill = Color.Black
	                    onAction = evt => {
                            ExternalProgsUtil.openUrl( new URL(settings.update.downloadUrl) )
                        }
	            }
	
	private lazy val toolBars = new HBox {
	                    children.addAll( toolBar )
	            }
	private lazy val toolBar = new ToolBar {
	                    items.addAll ( 
	                            new Button() {
	                                graphic = ResourceUtil.getIconImage("clone.png")
	                                tooltip = new Tooltip("Clone a remote repository")
	                                onAction = (evt) => { cloneRepository() }
	                            }.delegate,
	                            new Button() {
	                                graphic = ResourceUtil.getIconImage("opentype.png")
	                                tooltip = new Tooltip("Open a local repository")
	                                onAction = (evt) => { openRepository() }
	                            }.delegate,
	                            new Separator(),
	                            new Button() {
	                                graphic = ResourceUtil.getIconImage("fetch.png")
	                                tooltip = new Tooltip("Fetch remotes")
	                                onAction = (evt) => { DialogUtil.runWithProgress( GitUtil.fetchRemotes( currentRepository(), frame ) ) }
	                            }.delegate,
	                            new Button() {
	                                graphic = ResourceUtil.getIconImage("pull.png")
	                                tooltip = new Tooltip("Pull current branch")
	                                onAction = (evt) => { DialogUtil.runWithProgress( GitUtil.pullBranch( currentRepository(), frame ) ) }
	                            }.delegate,
	                            new Separator(),
	                            new Button() {
	                                graphic = ResourceUtil.getIconImage("push.png")
	                                tooltip = new Tooltip("Push current branch")
	                                onAction = (evt) => { pushCurrentBranch() }
	                            }.delegate,
	                            new Separator(),
	                            new Button() {
	                                graphic = ResourceUtil.getIconImage("new_branch_obj.png")
	                                tooltip = new Tooltip("Create branch from selection...")
	                                onAction = (evt) => { createBranch() }
	                            }.delegate,
	                            new Separator(),
	                            new Button() {
	                                graphic = ResourceUtil.getIconImage("new_tag_obj.png")
	                                tooltip = new Tooltip("Tag the selected commit...")
	                                onAction = (evt) => { createTag() }
	                            }.delegate
	                            
	                    )
	            }
	private lazy val statusBar = new Label() {
	    styleClass += "status-bar"
	}
	
	def pushCurrentBranch() {
	    DialogUtil.runWithProgress( GitUtil.pushBranch( currentRepository(), null, false, frame ) )
	}
	def createBranch() {
	    val repo = currentRepositoryPanel()
	    if(repo != null) repo.createBranchFromSelection()
	}
	def createTag() {
	    val repo = currentRepositoryPanel()
	    if(repo != null) repo.createTagFromSelection()
	}
	
	override protected def updateInternal(canvas: Canvas) {}
	
	
    
}