package de.lange.m.git.ui

import scala.collection.mutable.ArrayBuffer
import scala.language.implicitConversions
import scala.reflect.ClassTag

import de.lange.m.git.ui.Includes.toClass

object Property {
	/**
	 * Implicit conversion of a [[Property]] to its content.
	 */
	implicit def propertyToValue[T](prop: Property[T]) : T = prop.value
}
/**
 * Float implementation of [[Property]]
 */
class FloatProp(owner: AnyRef, name: String, value: Float, range: (Float, Float) = (0,1)) extends
		Property[Float](owner, name, value, range) {
}

/**
 * Float implementation of [[Property]], bound to a callback function.
 */
class BoundFloatProp(owner: AnyRef, name: String, private[this] var _function: () => Float, range: (Float, Float) = (0,1)) extends
		FloatProp(owner, name, 0, range) {
	override def update(v: Float) = {
		throw new IllegalStateException("The value of a BoundFloatProp cannot be set.")
	}
	override def apply() = _function()
}


/**
 * Integer implementation of [[Property]]
 */
class IntProp(owner: AnyRef, name: String, value: Integer, range: (Integer, Integer) = (0,1)) extends
		Property[Integer](owner, name, value, range) {
}

/**
 * Integer implementation of [[Property]], bound to a callback function.
 */
class BoundIntProp(owner: AnyRef, name: String, private[this] var _function: () => Integer, range: (Integer, Integer) = (0,1)) extends
		IntProp(owner, name, 0, range) {
	override def update(v: Integer) = {
		throw new IllegalStateException("The value of a BoundFloatProp cannot be set.")
	}
	override def apply() = _function()
}

/**
 * String implementation of [[Property]]
 */
class StringProp(owner: AnyRef, name: String, value: String) extends
		Property[String](owner, name, value, ("", "")) {}

/**
 * String implementation of [[Property]], bound to a callback function.
 */
class BoundStringProp(owner: AnyRef, name: String, private[this] var _function: () => String) extends
		StringProp(owner, name, "") {
	override def update(v: String) = {
		throw new IllegalStateException("The value of a BoundFloatProp cannot be set.")
	}
	override def apply() = _function()
}

/**
 * Boolean implementation of [[Property]]
 */
class BoolProp(owner: AnyRef, name: String, value: Boolean) extends
		Property[Boolean](owner, name, value, (false, true)) {}

/**
 * Boolean implementation of [[Property]], bound to a callback function.
 */
class BoundBoolProp(owner: AnyRef, name: String, private[this] var _function: () => Boolean) extends
		BoolProp(owner, name, false) {
	override def update(v: Boolean) = {
		throw new IllegalStateException("The value of a BoundBoolProp cannot be set.")
	}
	override def apply() = _function()
}

class ObjectProp[T : ClassTag](owner: AnyRef, name: String, value: T) extends
		Property[T](owner, name, value, (null.asInstanceOf[T], null.asInstanceOf[T])) {}

/*
class EnumProp[T <: Enumeration#Value : ClassTag](
			owner: AnyRef,
			name: String,
			val enum: Enumeration,
			value: T) extends
		Property[T](owner, name, value, (null.asInstanceOf[T], null.asInstanceOf[T])) {

	def values = enum.values
}
*/
/**
 * [[Property]], bound to a callback function.
 */
class BoundProperty[T : ClassTag](owner: AnyRef, name: String, private[this] var _function: () => T) extends
		Property[T](owner, name, null.asInstanceOf[T], (null.asInstanceOf[T], null.asInstanceOf[T])) {
	override def update(v: T) = {
		throw new IllegalStateException("The value of a BoundProperty cannot be set.")
	}
	override def apply() = _function()
}


/**
 *Properties are values that can be observed (i.e. listeners can be added)
 * @param owner The Property's owner
 * @param name The Property's name
 * @param _value The Property's initial value
 * @param range The range of values (no restrictions to actual values, just for display/UI)
 */
class Property[T : ClassTag](val owner: AnyRef, val name: String, private[this] var _value: T, val range: (T, T)) {
	/** The type of the value */
	type ValueType = T

	/** The class of the value */
	val valueClass = toClass[ValueType]

	/**
	 * Change listeners.
	 *
	 * Function parameters are:<br>
	 * - the changed Property<br>
	 * - the old value<br>
	 * - the new value<br>
	 *
	 * Example:{{{
	 * prop.onChange += { (prop, oldValue, newValue) => ... }
	 * }}}
	 */
	val onChange = new ArrayBuffer[ (Property[T], T, T) => Unit ]()

	/**
	 * Get the Property's value
	 */
	def value = apply()
	/**
	 * Set the Property's value
	 */
	def value_=(v: T) = update(v)

	/**
	 * Get the Property's value
	 */
	def apply() = _value
	/**
	 * Get the Property's value
	 */
	def get() = apply()

	/**
	 * Set the Property's value
	 */
	def update(v: T) = {
		if(v != _value) {
			val old = v
			_value = v
			valueChanged(old, _value)
		}
	}
	/**
	 * Set the Property's value
	 */
	def set(v: T) = update(v)

	private[this] def valueChanged(oldValue: T, newValue: T) {
		for(listener <- onChange) {
			listener(this, oldValue, newValue)
		}
	}
}