package de.lange.m.git.ui

import java.nio.file.WatchService

import scala.collection.JavaConverters.{ asScalaSetConverter, bufferAsJavaListConverter }
import scala.collection.mutable
import scala.xml.Elem

import org.eclipse.jgit.api.ResetCommand.ResetType
import org.eclipse.jgit.events.{ ListenerHandle, RepositoryEvent }
import org.eclipse.jgit.lib.{ BranchConfig, ProgressMonitor }

import de.lange.m.git.Paths
import de.lange.m.git.data.{ Commit, DefaultCommit, GitTree, Uncommitted }
import de.lange.m.git.io.{ GitTreeReader, SvgExporter, XmlRepositoryConfigReader }
import de.lange.m.git.settings.{ AppSettings, UserConfig }
import de.lange.m.git.ui.event.StatusListener
import de.lange.m.git.ui.formatter.{ DefaultCommitFormatter, DefaultFileDiffFormatter, DefaultRefsFormatter, DefaultStagingFormatter, FileDiffFormatter }
import de.lange.m.git.ui.layout.{ Layout, LayoutElement, LayoutLeaf, SplitContainer, TabsContainer }
import de.lange.m.git.util.{ DialogUtil, GitUtil, ResourceUtil, ThreadUtil }
import javafx.concurrent.Worker
import javafx.event.Event
import javafx.scene.{ control => jfxc }
import javafx.scene.control.SeparatorMenuItem
import javafx.scene.{ layout => jfxl }
import netscape.javascript.JSObject
import scalafx.application.Platform
import scalafx.scene.Node
import scalafx.scene.control.{ CheckMenuItem, ContextMenu, Menu, MenuButton, MenuItem, RadioMenuItem, SplitPane, Tab, TabPane, ToggleGroup, ToolBar, Tooltip }
import scalafx.scene.layout.{ BorderPane, Priority }
import scalafx.scene.web.WebView


/**
 * UI element for the entire representation of a repository. I.e. a tab in the main frame.
 * 
 * @constructor Create a repo panel
 * @param _tree the GitTree
 * @param path the repository's path (.git directory)
 * @param settings the AppSettings to use
 * @param config the UserConfig to use
 */
class RepositoryPanel protected (
        private var _tree: GitTree,
        val path: String,
        val settings: AppSettings,
        val config: UserConfig,
        val statusListener: StatusListener ) extends jfxl.BorderPane {
    panel =>
    
    /** First selection */
    private var selectedA: String = null
    /** Second selection */
    private var selectedB: String = null
    /** Selected file path */
    private var selectedFile: String = null
    
    private var selectedStaging: String = null
    
    private var diffViewMode = FileDiffFormatter.DiffViewMode.Normal
    private var lastScrollPosition = 0
    
    /** Indicates changes to the underlying repository */
    private var hasChanged = false
    /** Indicates if the underlying repository had uncommitted changes (for reload/update) */
    private var hasUncommittedChanges = false
    private var hasUncommittedFilesChanged = false
    
    private var stagedFiles: mutable.Set[String] = new mutable.HashSet[String]
    private var unstagedFiles: mutable.Set[String] = new mutable.HashSet[String]
    
    private var maxCommits = settings.tree.maxCommits
    
    /** Timer to check for changes in regular intervals */
    private var changeCheckTimer: java.util.Timer = null
    
    /** Formatter for the commit view */
    private var commitFormatter = new DefaultCommitFormatter()
    /** Formatter for the diff view */
    private var fileDiffFormatter = new DefaultFileDiffFormatter()
    /** Formatter for the refs view */
    private var refsFormatter = new DefaultRefsFormatter()
    /** Formatter for the staging view */
    private var stagingFormatter = new DefaultStagingFormatter()
        
    /**
     * Create a repo panel from a repository path (.git directory)
     */
    def this( _path: String, settings: AppSettings, config: UserConfig, listener: StatusListener ) = 
            this( null, _path, settings, config, listener )
            
    /**
     * Create a repo panel from an already loaded GitTree
     */
    def this( _tree: GitTree, settings: AppSettings, config: UserConfig, listener: StatusListener ) = 
            this( _tree, _tree.repository.getDirectory.getAbsoluteFile.getPath, settings, config, listener )
    
    /** Indicates if the panelis initialized */
    private var isInitialized = false
    
    /** The underlying GitTree */
    def tree = _tree
    
    /** Initializes the panel */
    private def initialize() {
        if(!isInitialized) {
            setCenter( createLayout(settings.window.layout) )
            startChangeCheckTimer()
            isInitialized = true
        }
    }
    
    /** Load HTML or SVG String into the main/graph view */
    protected def loadContentMain(content: String) {
		Platform.runLater {
	        mainBrowser.engine.loadContent(content)
		}
    }
    /** Load HTML or SVG String into the commit view */
    protected def loadContentCommit(content: String) {
		Platform.runLater {
	        commitBrowser.engine.loadContent(content)
		}
    }
    /** Load HTML or SVG String into the diff view */
    protected def loadContentDiff(content: String) {
		Platform.runLater {
	        diffBrowser.engine.loadContent(content)
		}
    }
    /** Load HTML or SVG String into the refs view */
    protected def loadContentRefs(content: String) {
		Platform.runLater {
	        refsBrowser.engine.loadContent(content)
		}
    }
    /** Load HTML or SVG String into the staging view */
    protected def loadContentStaging(content: String) {
		Platform.runLater {
	        stagingBrowser.engine.loadContent(content)
		}
    }
    
    private var indexListenerHandle: ListenerHandle = null
    private var configListenerHandle: ListenerHandle = null
    private var refsListenerHandle: ListenerHandle = null
    private var fileWatcher: WatchService = null
    
    /** Show a given tree. */
    protected def showTree(tree: GitTree) {
        if(tree != null) {
            this._tree = tree
            initialize()
            val repo = tree.repository
            hasUncommittedChanges = tree.hasUncommittedChanges
            
            if(indexListenerHandle != null) indexListenerHandle.remove()
            if(refsListenerHandle != null) refsListenerHandle.remove()
            if(configListenerHandle != null) configListenerHandle.remove()
            indexListenerHandle = repo.getListenerList.addIndexChangedListener( evt => { reloadStaging(evt) } )
            refsListenerHandle = repo.getListenerList.addRefsChangedListener( evt => { reloadRepository(evt) } )
            configListenerHandle = repo.getListenerList.addConfigChangedListener( evt => { reloadRepository(evt) } )
            
            ThreadUtil.parallel[(String, Elem)] { () => 
                val str = new SvgExporter(tree, true).getSvg()
                val refs = refsFormatter.formatRefs(tree)
                (str, refs)
            }{ (strRefs) => 
                loadContentMain(strRefs._1)
                loadContentRefs(strRefs._2.toString())
                showStaging()
            }
        }
    }
    
    private def showStaging() {
        val xml = stagingFormatter.formatStaging(tree, settings.diff)
        loadContentStaging(xml.toString())
    }
    
    /** 
     * Show a commit, or the difference between two commits.
     * 
     * @param id the hash of the commit
     * @param oldId optional hash of commit to compare to. Use null for parent
     */
    def showCommit(id: String, oldId: String) {
        if(id == null) {
            loadContentDiff("")
            loadContentCommit("")
            return
        }
        
        loadContentCommit( HtmlLoadScreen() )
        
        if( tree.lookup.contains(id) ) {
            ThreadUtil.parallel[String] { () => {
                    val commit = tree.lookup(id)
                    
            		val parent = if(oldId != null) tree.lookup(oldId) else null
            		
            		val xml = commitFormatter.formatCommits(tree, parent, commit, settings.diff)
                    val str = xml.toString()
                    str
                }
            } { (str) =>
                loadContentCommit(str)
            }
        }
    }
    
    /** 
     * Show a file's diff for a commit, or the difference between two commits.
     * 
     * @param commitId the hash of the commit
     * @param oldId optional hash of commit to compare to. Use null for parent
     * @param path path of the file
     */
    def showDiff(commitId: String, oldId: String, path: String) {
        selectedFile = path
        val commit = tree.lookup(commitId)
        val oldCommit = if(oldId != null && tree.lookup.contains(oldId)) tree.lookup(oldId) else null
        
        val xml = fileDiffFormatter.formatFileDiff(tree, settings.diff, oldCommit, commit, path, diffViewMode)
        val str = xml.toString()
        loadContentDiff(str)
    }
    /** 
     * Show a file's diff for unstaged changes.
     * 
     * @param path path of the file
     */
    def showDiff(path: String) {
        val comm = tree.uncommitted
        if(  null != comm ) {
            selectedStaging = path
            val xml = fileDiffFormatter.formatFileDiff(tree, settings.diff, null, comm, path, diffViewMode)
            val str = xml.toString()
            loadContentDiff(str)
        } else {
            clearDiff()
        }
    }
    def clearDiff() {
        loadContentDiff("")
    }
        
    /** Get the current selection. */
    def getSelection(): Array[String] = {
        Array[String]( selectedA, selectedB )
    }  
    /** Get the currently selected file. */
    def getSelectedFile(): String = {
        selectedFile
    }
    /** Get the currently selected file of the staging view. */
    def getSelectedStaging(): String = {
        selectedStaging
    }
    /** Set the current selection. Updates the commit view. */
    def setSelection(selA: String, selB: String) {
        selectedA = selA
        selectedB = selB
        showCommit(selectedA, selectedB)
    }
    /** Update the current selection. Notifies JS. Does not updates the commit view. */
    def updateSelection(selA: String, selB: String) {
        selectedA = selA
        selectedB = selB
        mainBrowser.engine.executeScript("updateSelection(true)");
    }
    def getLastScrollPosition(): Int = {
        lastScrollPosition
    }
    
    /** Stop timer for change checks. */
	def onClosed() {
        if(changeCheckTimer != null) {
            changeCheckTimer.cancel()
        }
        saveRepoConfig()
	}
	private def saveRepoConfig() {
	    GitUtil.saveRepoConfig(tree)
	}
	
    /** When the tab first becomes visible, load the repository and show the GitTree. */
	def onSelectionChanged(evt: Event, selected: Boolean) {
	    if( selected && (!isInitialized || _tree == null) ) {
            val loadTask = new TreeWorker(path, settings.tree, settings.tree.maxCommits)
    		Platform.runLater {
                new Splash().show(loadTask, () => {
                    showTree(loadTask.value.value)
                })
                new Thread(loadTask).start()    
            }
	    }
	}
	
    /** Start timer for change checks. */
	private def startChangeCheckTimer() {
	    changeCheckTimer = new java.util.Timer()
        val t = new java.util.TimerTask {
            def run() {
                hasChanged = true
                hasUncommittedFilesChanged = true
                if( tree.hasUncommittedChanges != hasUncommittedChanges ) {
                    hasUncommittedChanges = true
                    reloadRepository(null)
                } else {
                    reloadStaging(null)
                }
                tree.repository.scanForRepoChanges()
            }
        }
        changeCheckTimer.schedule(t, 500L, 2500L)
	}
	
	def reloadRepository() { reloadRepository(null) }
	
	/** Reload the repository if necessary due to changes. */
	private def reloadRepository(evt: RepositoryEvent[_]) {
        Platform.runLater {
            lastScrollPosition = mainBrowser.engine.executeScript("getScrollPosition()").asInstanceOf[Int]
            //loadContentMain( HtmlLoadScreen() )
            
            ThreadUtil.parallel[GitTree] { () => {
            	    hasChanged = false
            	    println("Reloading "+path)
            	    val tree = GitTreeReader.loadTree(_tree.git, settings.tree, maxCommits, null)
            	    if( selectedA != null && !tree.lookup.contains(selectedA) ) selectedA = null
            	    if( selectedB != null && !tree.lookup.contains(selectedB) ) selectedB = null
            	    tree
                }
            } { (tree) =>
                    showTree(tree)
            }
        }
	}
	private def reloadStaging(evt: RepositoryEvent[_]) {
	    if( hasUncommittedChanged() ) {
            showStaging()
	    }
	}
	private def hasUncommittedChanged(): Boolean = {
	    GitUtil.getUncommittedChanges(tree) match {
	        case Some((staged, unstaged)) => 
	            if( staged == stagedFiles && unstaged == unstagedFiles ) {
	                false
	            } else {
	                stagedFiles = staged
	                unstagedFiles = unstaged
	                true
	            }
	        case None => 
	            false
	    }
	}
	
	def loadMoreCommits() {
	    maxCommits += settings.tree.maxCommits
	    reloadRepository(null)
	}
	
	/** Create the actual layot from a Layout instance. */
    private def createLayout(layout: Layout): Node = {
        createNode( layout.root )
    }
    
	/** Create a layout node. */
    private def createNode(node: LayoutElement): Node = {
        node match {
            case c: SplitContainer =>
                new SplitPane {
                        hgrow = Priority.Always
                        vgrow = Priority.Always
                        orientation = c.orientation
                        val ch = for( child <- c.children ) yield Node.sfxNode2jfx( createNode( child ) )
                        items.addAll( ch.asJava )
                        c.fixed match {
                            case Layout.Side.Left => 
                                if(ch.length > 0) javafx.scene.control.SplitPane.setResizableWithParent(ch(0), java.lang.Boolean.FALSE)
                            case Layout.Side.Right => 
                                if(ch.length > 1) javafx.scene.control.SplitPane.setResizableWithParent(ch(1), java.lang.Boolean.FALSE)
                            case Layout.Side.None => 
                        }
                        dividerPositions = c.position
                    }
            case c: TabsContainer =>
                new TabPane {
                        hgrow = Priority.Always
                        vgrow = Priority.Always
                        side() = c.side
                        tabs.addAll( (for( child <- c.children ) yield {
                                val tab = new Tab()
                        	    tab.setText(child.title)
                        	    tab.setContent( Node.sfxNode2jfx( createNode( child ) ) )
                        	    tab.closable() = false
                        	    tab.delegate
                            }): _* )
                    }
            case LayoutLeaf(n, t) => 
                n match {
                    case "main" => mainBrowser
                    case "commit" => commitBrowser
                    case "diff" => new BorderPane() { 
                                        top = diffToolbar
                                        center = diffBrowser 
                                   }
                    case "refs" => refsBrowser
                    case "staging" => stagingBrowser
                }
        }
    }
    
	/** The JS hooks. */
	private lazy val browserHooks = new JSBrowserHooks(panel)
	/** The main/graph view. */
	private lazy val mainBrowser = createBrowser()
	/** The commit view. */
	private lazy val commitBrowser = createBrowser()
	/** The diff view. */
	private lazy val diffBrowser = createBrowser()
	/** The refs view. */
	private lazy val refsBrowser = createBrowser()
	/** The staging view. */
	private lazy val stagingBrowser = createBrowser()
	
	private lazy val diffToolbar = new ToolBar() {
	                    items.addAll ( 
	                            diffViewButton
	                    )
	}
	private lazy val diffViewButton = new MenuButton() {
	                                tooltip = new Tooltip("Diff view options")
	                                val group = new ToggleGroup()
	                                items.addAll(
	                                        (for(i <- 0 until FileDiffFormatter.DiffViewMode.maxId) 
	                                            yield new RadioMenuItem( FileDiffFormatter.DiffViewMode(i).toString() ){ 
	                                                                toggleGroup = group 
	                                                                onAction = ( evt => setDiffViewMode( FileDiffFormatter.DiffViewMode.withName( text() ) ) )
	                                                            }.delegate 
	                                        ):_*
	                                    )
	                                group.selectToggle( items.get(0).asInstanceOf[jfxc.Toggle] )
	                            }
	
	def setDiffViewMode( mode: FileDiffFormatter.DiffViewMode.Value ) {
	    if( mode != diffViewMode ) {
	        diffViewMode = mode
	        showDiff(selectedA, selectedB, selectedFile)
	    }
	}
	
	/**
	 * Create a WebView.
	 */
	private def createBrowser(): WebView = {
	    new WebView {
    	    browser =>
            hgrow = Priority.Always
            vgrow = Priority.Always
            contextMenuEnabled = false
            
            onScroll = ( evt => {
                if(evt.isControlDown()) {
                    val delta = evt.getDeltaY()
                    if(delta < 0) {
                        zoom() = zoom() + 0.1
                    } else if(delta > 0) {
                        if(zoom() >= 0.2) {
                            zoom() = zoom() - 0.1
                        }
                    }
                    evt.consume()
                }
            })
            onMousePressed = (evt => {
                hideContextMenus()
            })
            engine.getLoadWorker.stateProperty().addListener( { (obs, os, ns) =>
                if (ns == Worker.State.SUCCEEDED) {
                    val windowObject = engine.executeScript("window").asInstanceOf[JSObject]
                    windowObject.setMember("app", browserHooks)
                    windowObject.call("ready")
                }
            })
        }
	}
	
	def hideContextMenus() {
	    if( contextMenuBranch.isShowing() ) contextMenuBranch.hide()
	    if( contextMenuTag.isShowing() ) contextMenuTag.hide()
	    if( contextMenuCommit.isShowing() ) contextMenuCommit.hide()
	}
    
	def showContextMenuBranch(name: String, isRemote: Boolean, localOnly: Boolean, screenX: Double, screenY: Double) {
	    contextMenuBranch.show(refsBrowser, screenX, screenY, name, isRemote, localOnly)
	}
	def showContextMenuTag(name: String, commit: String, screenX: Double, screenY: Double) {
	    contextMenuTag.show(refsBrowser, screenX, screenY, name, commit)
	}
	def showContextMenuCommit(id: String, screenX: Double, screenY: Double) {
	    contextMenuCommit.show(refsBrowser, screenX, screenY, id)
	}
	
	private lazy val contextMenuBranch = new ContextMenu() {
	    
	    private var branch: String = null
	    private var isRemote: Boolean = false
	    private var localOnly: Boolean = false
	    	    
	    val checkout = new MenuItem("Check out") {
	                        graphic = ResourceUtil.getIconImage("checkout.png")
	                        onAction = evt => {
	                            checkoutBranch(branch, isRemote)
	                        }
	                    }
	    val sep1 = new SeparatorMenuItem()
	    
	    val createBranch = new MenuItem("Create branch...") {
	                        graphic = ResourceUtil.getIconImage("new_branch_obj.png")
	                        onAction = evt => {
	                            createBranchFromBranch(branch)
	                        }
	                    }
	    val merge = new MenuItem("Merge into current") {
	                        graphic = ResourceUtil.getIconImage("merge.png")
	                        onAction = evt => {
	                            mergeBranch(branch)
	                        }
	                    }
	    val sep2 = new SeparatorMenuItem()
	    
	    val trackingBranch = new Menu("Tracking branch...") {
	                        graphic = ResourceUtil.getIconImage("remote.png")
	                    }
	    val pull = new MenuItem("Pull") {
	                        graphic = ResourceUtil.getIconImage("pull.png")
	                        onAction = evt => pullCurrentBranch()
	                    }
	    val push = new Menu("Push") {
	                        graphic = ResourceUtil.getIconImage("push.png")
	                    }
	    val pushForced = new Menu("Push forced") {
	                        graphic = ResourceUtil.getIconImage("push.png")
	                    }
	    val sep3 = new SeparatorMenuItem()
	    
	    val renameBranch = new MenuItem("Rename branch") {
	                        graphic = ResourceUtil.getIconImage("editconfig.png")
	                        onAction = evt => {
	                            panel.renameBranch(branch, isRemote)
	                        }
	                    }
	    val deleteBranch = new MenuItem("Delete branch") {
	                        graphic = ResourceUtil.getIconImage("delete.png")
	                        onAction = evt => {
	                            panel.deleteBranch(branch, isRemote)
	                        }
	                    }
	    
	    def addItems() {
	        items.addAll(
	                    checkout,
	                        sep1,
	                    createBranch,
	                    merge,
	                        sep2,
	                    trackingBranch,
	                    pull,
	                    push,
	                    pushForced,
	                        sep3,
	                    renameBranch,
	                    deleteBranch
	                )
	    }
	                
	    def update() {
	        items.clear()
	        checkout.text() = if(isRemote) "Check out '"+branch+"' as local"
	                            else "Check out '"+branch+"'"
	                            
            createBranch.disable = isRemote
            merge.disable = isRemote
            push.disable = isRemote
            
		    val remotes = tree.remoteRefs.toArray.sortBy( tup => tup._1 )
		    val brConf = new BranchConfig(tree.repository.getConfig, branch)
	        trackingBranch.disable = isRemote
	        trackingBranch.items.clear()
	        trackingBranch.items.addAll( (for((remote, commit) <- remotes) yield { new CheckMenuItem(remote) {
                                                	            if( brConf.getRemoteTrackingBranch == GitTree.remotePrefix+remote ) selected = true
                                                	            onAction = evt => setTrackingBranch(branch, remote)
                                                	        }.delegate } ):_* )
            trackingBranch.items.addAll( new CheckMenuItem("(None)") {
                                            if( brConf.getRemoteTrackingBranch == "" || brConf.getRemoteTrackingBranch == null ) selected = true
                            	            onAction = evt => setTrackingBranch(branch, null)
                            	        }.delegate )
                                                	        
	        pull.text() = "Pull '"+branch+"'"
	        pull.disable = (branch != tree.currentBranch) || isRemote || localOnly
	                          
	        push.text() = "Push '"+branch+"'"
	        push.disable = isRemote
	        push.items.clear()
	        push.items.addAll( (for(remote <- tree.remotes.asScala.toSeq) yield { new MenuItem(remote) {
                                                	            onAction = evt => pushBranch(remote, branch, false)
                                                	        }.delegate } ):_* )
                                                	        
	        pushForced.text() = "Force-Push '"+branch+"'"
	        pushForced.disable = isRemote
	        pushForced.items.clear()
	        pushForced.items.addAll( (for(remote <- tree.remotes.asScala.toSeq) yield { new MenuItem(remote) {
                                                	            onAction = evt => pushBranch(remote, branch, true)
                                                	        }.delegate } ):_* )
                                                	        
            renameBranch.text() = "Rename '"+branch+"'"
            // TODO enable renaming remote branches
            renameBranch.disable = isRemote
            
            deleteBranch.text() = "Delete '"+branch+"'"
            
            addItems()
	    }
	    
	    def show(owner: Node, screenX: Double, screenY: Double, branch: String, isRemote: Boolean, localOnly: Boolean) {
	        this.branch = branch
	        this.isRemote = isRemote
	        this.localOnly = localOnly
	        update()
	        show(owner, screenX, screenY)
	    }
	}
	
	private lazy val contextMenuTag = new ContextMenu() {
	    
	    private var tag: String = null
	    private var commit: String = null
	    	  
	    val checkout = new MenuItem("Check out") {
	                        graphic = ResourceUtil.getIconImage("checkout.png")
	                        onAction = evt => {
	                            checkoutCommit(commit)
	                        }
	                    }  
	    val sep1 = new SeparatorMenuItem()
	    val deleteTag = new MenuItem("Delete tag") {
	                        graphic = ResourceUtil.getIconImage("delete.png")
	                        onAction = evt => {
	                            panel.deleteTag(tag)
	                        }
	                    }
	    def addItems() {
    	    items.addAll(
    	                    checkout,
    	                        sep1,
    	                    deleteTag
    	                )
	    }
	                
	    def update() {
	        items.clear()
	        checkout.text() = "Check out '"+tag+"'"
            deleteTag.text() = "Delete tag '"+tag+"'"
            addItems()
	    }
	    
	    def show(owner: Node, screenX: Double, screenY: Double, tag: String, commit: String) {
	        this.tag = tag
	        this.commit = commit
	        update()
	        show(owner, screenX, screenY)
	    }
	}
	
	private lazy val contextMenuCommit = new ContextMenu() {
	    private var commit: String = null
	    
	    val checkout = new MenuItem("Check out") {
	                        graphic = ResourceUtil.getIconImage("checkout.png")
	                        onAction = evt => {
	                            checkoutCommit( commit )
	                        }
	                    }
	    val sep1 = new SeparatorMenuItem()
	    val createBranch = new MenuItem("Create branch...") {
	                        graphic = ResourceUtil.getIconImage("new_branch_obj.png")
	                        onAction = evt => {
	                            createBranchFromCommit(commit)
	                        }
	                    }
	    val merge = new MenuItem("Merge into current") {
	                        graphic = ResourceUtil.getIconImage("merge.png")
	                        onAction = evt => {
	                            mergeCommit(commit)
	                        }
	                    }
	    val tag = new MenuItem("Tag commit") {
	                        graphic = ResourceUtil.getIconImage("new_tag_obj.png")
	                        onAction = evt => {
	                            createTagFromCommit(commit)
	                        }
	                    }
	    val sep2 = new SeparatorMenuItem()
	    val resetMenu = new Menu("Reset current branch to this commit") {
	                        graphic = ResourceUtil.getIconImage("reset.png")
	                        items.addAll(
	                                    new MenuItem("Soft (HEAD only)") {
	                                        onAction = evt => { resetCurrentBranch(commit, ResetType.SOFT) }
	                                    }.delegate,
	                                    new MenuItem("Mixed (HEAD and Index)") {
	                                        onAction = evt => { resetCurrentBranch(commit, ResetType.MIXED) }
	                                    }.delegate,
	                                    new MenuItem("Hard (HEAD, Index and Working Tree)") {
	                                        onAction = evt => { resetCurrentBranch(commit, ResetType.HARD) }
	                                    }.delegate
	                                )
	                    }
	    def addItems() {
	        items.addAll(
	                    checkout,
	                        sep1,
	                    createBranch,
	                    merge,
	                    tag,
	                        sep2,
	                    resetMenu
	                )
	    }
	    
	    def update() {
	        items.clear()
	        val commit = tree.lookup(this.commit)
	        checkout.text() = "Check out "+commit.shortName
	        addItems()
	    }
	    
	    def show(owner: Node, screenX: Double, screenY: Double, commit: String) {
	        this.commit = commit
	        update()
	        show(owner, screenX, screenY)
	    }
	}
	
	def setTrackingBranch(branch: String, remoteBranch: String) {
	    DialogUtil.runWithProgress( GitUtil.setTrackingBranch(tree, branch, remoteBranch, statusListener) )
	}
	
	def checkoutBranch(branch: String, isRemote: Boolean) {
	    if( isRemote ) {
            DialogUtil.runWithProgress( GitUtil.checkoutRemoteBranch(tree, branch, statusListener) )
        } else {
            DialogUtil.runWithProgress( GitUtil.checkoutLocalBranch(tree, branch, statusListener) )
        }
	}
	def checkoutCommit(id: String) {
	    if( tree.lookup.contains(id) ) {
	        DialogUtil.runWithProgress( GitUtil.checkoutCommit(tree, tree.lookup(id), statusListener) )
	    } else {
	        DialogUtil.error("Commit not found", "Commit not found", "Unable to find commit "+id)
	    }
	}
	private def pullCurrentBranch() {
	    DialogUtil.runWithProgress( GitUtil.pullBranch(tree, statusListener) )
	}
	private def pushBranch(remote: String, branch: String, forced: Boolean) {
	    DialogUtil.runWithProgress( GitUtil.pushBranch(tree, remote, branch, forced, statusListener) )
	}
	
	def createBranchFromSelection() {
	    createBranchFromCommit(selectedA)
	}
	
	private def createBranchFromBranch(branch: String) {
	    val commit = tree.branchRefs(branch)
	    if( commit == null ) {
	        DialogUtil.error("Error creating branch", "Error creating branch: target commit not found", "No commit found for branch "+branch)
	    } else {
	        createBranchFromCommit(commit)
	    }
	}
	private def createBranchFromCommit(commitId: String) {
	    if( commitId == null ) {
	        DialogUtil.error("Error creating branch", "Error creating branch: nothing selected", "Select a commit to create a branch!")
	    } else if( commitId == Uncommitted.ID ) {
	        createBranchFromCommit(null.asInstanceOf[Commit])
	    } else if( tree.lookup.contains(commitId) ) {
	        createBranchFromCommit( tree.lookup(commitId) )
	    } else {
	        DialogUtil.error("Error creating branch", "Error creating branch: target commit not found", "Commit not found "+commitId)
	    }
	}
	private def deleteTag(tag: String) {
	    DialogUtil.runWithProgress( GitUtil.deleteTag(tree, tag, statusListener) )
	}
	private def createBranchFromCommit(commit: Commit) {
	    DialogUtil.runWithProgress( GitUtil.createBranch(tree, commit, statusListener) )
	}
	private def renameBranch(branch: String, isRemote: Boolean) {
	    DialogUtil.runWithProgress( GitUtil.renameBranch(tree, branch, isRemote, statusListener) )
	}
	private def deleteBranch(branch: String, isRemote: Boolean) {
	    DialogUtil.runWithProgress( GitUtil.deleteBranch(tree, branch, isRemote, statusListener) )
	}
	private def resetCurrentBranch(commitId: String, mode: ResetType) {
	    if( tree.lookup.contains(commitId) ) {
    	    val commit = tree.lookup(commitId)
    	    commit match {
    	        case uc: Uncommitted => DialogUtil.error("Error resetting", "Error resetting: can't reset to working tree", "Select a commit to reset to!")
    	        case dc: DefaultCommit => DialogUtil.runWithProgress( GitUtil.resetBranch(tree, dc, mode, statusListener) )
    	        case _ =>
    	    }
	    }
	}
	private def mergeBranch(branch: String) {
	    val br = tree.branches(branch)
	    DialogUtil.runWithProgress( GitUtil.mergeBranch(tree, br, statusListener) )
	}
	private def mergeCommit(commitId: String) {
	    if( commitId == null ) {
	        DialogUtil.error("Error merging", "Error merging: nothing selected", "Select a commit to merge!")
	    } else if( commitId == Uncommitted.ID ) {
	        DialogUtil.error("Error merging", "Error merging: uncommitted changes", "Commit or stash your changes before merging!")
	    } else if( tree.lookup.contains(commitId) ) {
	        mergeCommitOrBranch( tree.lookup(commitId) )
	    } else {
	        DialogUtil.error("Error merging", "Error merging: target commit not found", "Commit not found "+commitId)
	    }
	}
	private def mergeCommit(commit: Commit)( progress: ProgressMonitor ) {
        val choices = new mutable.ArrayBuffer[String]()
        for( (br, comm) <- tree.branchRefs ) {
            if( comm == commit ) {
                choices += br
            }
        }
        choices += "Commit "+commit.shortName
        val sel = DialogUtil.async( () => DialogUtil.choice("Merge what", "Merge what into "+tree.currentBranch, "", choices(0), choices) )
        sel match {
            case Some(choice) =>
                if( choice == choices.last ) {
                    GitUtil.mergeCommit(tree, commit, statusListener)( progress )
                } else {
                    GitUtil.mergeBranch(tree, tree.branches(choice), statusListener)( progress )
                }
            case None =>
        }
	}
	private def mergeCommitOrBranch(commit: Commit) {
	    DialogUtil.runWithProgress( mergeCommit(commit) )
	}
	
	
	
	def createTagFromSelection() {
	    createTagFromCommit(selectedA)
	}
	private def createTagFromCommit(commitId: String) {
	    if( commitId == null ) {
	        DialogUtil.error("Error creating tag", "Error creating tag: nothing selected", "Select a commit to create a tag!")
	    } else if( commitId == Uncommitted.ID ) {
	        DialogUtil.error("Error creating tag", "Error creating tag: can't tag uncommitted changed", "Select a commit to create a tag!")
	    } else if( tree.lookup.contains(commitId) ) {
	        DialogUtil.runWithProgress{ GitUtil.createTag( tree, tree.lookup(commitId), statusListener ) }
	    } else {
	        DialogUtil.error("Error creating tag", "Error creating tag: target commit not found", "Commit not found "+commitId)
	    }
	}
	
	
    def stageSelected() {
        if(selectedStaging != null) {
            GitUtil.stageFiles( tree, Seq( selectedStaging ) )
        }
    }
    def stageAll() {
        GitUtil.stageAll( tree )
    }
    def unstageSelected() {
        if(selectedStaging != null) {
            GitUtil.unstageFiles( tree, Seq( selectedStaging ) )
        }
    }
    def unstageAll() {
        GitUtil.unstageAll( tree )
    }
    def saveDraftMessage(message: String) {
        tree.repoConfig.draftMessage = message
    }
    def commit() {
        val message = tree.repoConfig.draftMessage.trim()
        val staged = GitUtil.getStaged(tree)
        if(staged.isEmpty) {
            DialogUtil.error("Nothing to commit", "Noting to commit", "No staged files to commit!")
            return
        }
        if(message.isEmpty()) {
            DialogUtil.error("Missing message", "No commit message", "Please enter a commit message!")
            return
        }
        tree.repoConfig.draftMessage = ""
        saveRepoConfig()
        GitUtil.commit( tree, message, statusListener )
    }
}