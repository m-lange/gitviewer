package de.lange.m.git.ui

import de.lange.m.git.{ Paths, Version }
import de.lange.m.git.util.ResourceUtil
import scalafx.Includes.{ double2DurationHelper, handle, observableList2ObservableBuffer }
import scalafx.animation.FadeTransition
import scalafx.concurrent.{ Task, Worker }
import scalafx.geometry.{ HPos, Pos }
import scalafx.scene.Scene
import scalafx.scene.control.{ Label, ProgressBar }
import scalafx.scene.effect.DropShadow
import scalafx.scene.image.{ Image, ImageView }
import scalafx.scene.layout.{ GridPane, Priority, VBox }
import scalafx.stage.{ Screen, Stage, StageStyle }

/**
  */
class Splash() {

  private val image = new Image( ResourceUtil.getStream( Paths.splashPath ) )
  private val icon = new Image( ResourceUtil.getStream( Paths.appIconPath ) )
  private val splash = new ImageView( image )
  
  private val splashWidth = image.width()
  private val splashHeight = image.height()
  
  private val splashStage = new Stage()
  
  private val loadProgress = new ProgressBar {
    prefWidth = splashWidth
  }
  private val progressText = new Label(". . .") {
    alignment = Pos.Center
  }
  private val versionText = new Label("Version "+Version.version.toString()) {
    alignment = Pos.Center
  }
  private val textBox = new GridPane {
      add(progressText, 0, 0)
      add(versionText, 1, 0)
      prefWidth() = splashWidth
      hgrow = Priority.Always
      GridPane.setHalignment(versionText, HPos.Right)
      GridPane.setHgrow(progressText, Priority.Always)
  }
  private val splashLayout = new VBox() {
    children ++= Seq(splash, loadProgress, textBox)
    style =
      "-fx-padding: 5; " +
        "-fx-background-color: white; " + // cornsilk
        "-fx-border-width:5; " +
        "-fx-border-color: " +
        "linear-gradient(" +
        "to bottom, " +
        "#ff9600, " +
        "derive(#ff9600, 50%)" +
        ");"
    effect = new DropShadow()
  }

  def show(loaderTask: Task[_], onSuccess: () => Unit): Unit = {
    progressText.text <== loaderTask.message
    loadProgress.progress <== loaderTask.progress
    loaderTask.state.onChange { (_, _, newState) =>
      newState match {
        case Worker.State.Succeeded.delegate =>
          loadProgress.progress.unbind()
          loadProgress.progress = 1
          new FadeTransition(0.5.s, splashLayout) {
            fromValue = 1.0
            toValue = 0.0
            onFinished = handle {
              splashStage.hide()
              splashStage.close()
            }
          }.play()

          onSuccess()

        case _ =>
        // TODO: handle other states
      }
    }


    splashStage.initStyle(StageStyle.Undecorated)
    splashStage.icons.addAll( icon )
    splashStage.scene = new Scene(splashLayout)
    val bounds = Screen.primary.bounds
    splashStage.x = bounds.minX + bounds.width / 2 - splashWidth / 2
    splashStage.y = bounds.minY + bounds.height / 2 - splashHeight / 2
    splashStage.show()
    splashStage.alwaysOnTop = true
    splashStage.alwaysOnTop = false
  }


}