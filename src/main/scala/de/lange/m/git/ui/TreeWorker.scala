package de.lange.m.git.ui

import de.lange.m.git.data.GitTree
import de.lange.m.git.io.GitTreeReader
import de.lange.m.git.settings.TreeViewSettings
import javafx.{ concurrent => jfxr }
import scalafx.concurrent.Task

/**
 * Internal worker to load a Git repository into a GitTree.
 * 
 * @constructor Construct a new TreeWorker.
 * @param path repository path to load (.git directory)
 * @param settings the TreeViewSettings to use
 */
class TreeWorker(
        val path: String,
        val settings: TreeViewSettings,
        val maxCommits: Int) extends Task(new jfxr.Task[GitTree] {
    
    override protected def call: GitTree = {
        update("Loading repository...", 3, 100)
        val tree = GitTreeReader.loadTree(path, settings, maxCommits, update _)
        tree
    }
    
    def update(message: String, v: Double, max: Double) {
        if(message != null) {
            updateMessage(message)
        }
        if(max > 0) {
            updateProgress(v, max)
        }
    }
})