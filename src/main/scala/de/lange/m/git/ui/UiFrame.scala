package de.lange.m.git.ui

import java.util.concurrent.{ Callable, FutureTask }

import scala.collection.mutable.ArrayBuffer

import javafx.event.EventHandler
import javafx.stage.WindowEvent
import scalafx.application.Platform
import scalafx.embed.swing.SFXPanel
import scalafx.scene.canvas.Canvas
import scalafx.stage.Stage

/**
 * Base class for UI frames with potentially limited update frequency
 */
trait UiFrame {

	private[this] var lastUpdateNanos = System.nanoTime()
	private[this] var updating = false


	protected class Position(var x: Double, var y: Double) {
		def set(x: Double, y: Double) { this.x = x; this.y = y }
	}
	protected val position = new Position(0,0)
	protected val size = new Position(0,0)

	val onHidden = new ArrayBuffer[(WindowEvent) => Unit]

	var stage : Stage = null

	// dummy to start FX framework
	new SFXPanel()
	Platform.runLater {
		stage = createStage()
		stage.onHidden = new EventHandler[WindowEvent] {
					override def handle(evt: WindowEvent) {
						for( func <- onHidden ) {
							func(evt)
						}
					}
				}
	}


	protected def createStage(): Stage



	def dispose() {
		if(stage != null) {
			setVisible(false)
		}
	}
	def show() {
		if(stage != null) {
			setVisible(true)
		}
	}
	def hide() {
		if(stage != null) {
			setVisible(false)
		}
	}
	def setVisible(visible: Boolean) {
		Platform.runLater{
			if( stage != null && stage.isShowing() != visible ) {
				if(visible) {
					stage.show()
					if( ! (position.x.isNaN() || position.y.isNaN()) ) {
						stage.x = position.x
						stage.y = position.y
					}
					stage.width = size.x
					stage.height = size.y
				} else {
					position.set(stage.x(), stage.y())
					size.set(stage.width(), stage.height())
					stage.hide()
				}
			}
		}
	}


	/**
	 * Calls updateInternal() if the time since the last update exceeds the time governed by parameter targetFps.<br>
	 * Note: In most cases, you want to use updateAndWait(...),
	 * since this method does not wait for the update call to finish.
	 * @param targetFps the maximum frame rate of this frame
	 * @return If updateInternal() was actually called
	 */
	def update(targetFps: Float) : Boolean = {
		if(canUpdate(targetFps)) {
			Platform.runLater{
				updating = true
				updateInternal(null)
				updating = false
			}
			true
		} else {
			false
		}
	}
	/**
	 * Calls updateInternal() if the time since the last update exceeds the time governed by parameter targetFps.<br>
	 * Does not return until the update call has finished.
	 * @param targetFps the maximum frame rate of this frame
	 * @return If updateInternal() was actually called
	 */
	def updateAndWait(targetFps: Float) : Boolean = {
		if(canUpdate(targetFps)) {
			val task = new FutureTask( new Callable[Boolean]() {
				override def call() = {
					updating = true
					updateInternal(null)
					updating = false
					true
				}
			})
			Platform.runLater(task)
			task.get()
		} else {
			false
		}
	}
	protected def canUpdate(targetFps: Float) : Boolean = {
		if( updating ) {
			return false
		}
		val nanos = System.nanoTime()
		if( targetFps <= 0 ) {
			lastUpdateNanos = nanos
			return true
		}
		val time = (System.nanoTime() - lastUpdateNanos) / 1e6f
		if(time > 1000f/targetFps) {
			lastUpdateNanos = nanos
			true
		} else {
			false
		}
	}

	/**
	 * Performs the actual update.<br>
	 * This is where the main update code for an implementation goes.
	 */
	protected def updateInternal(canvas: Canvas)

}