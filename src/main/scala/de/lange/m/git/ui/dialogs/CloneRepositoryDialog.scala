package de.lange.m.git.ui.dialogs

import javafx.scene.{ control => jfxc }
import scalafx.application.Platform
import scalafx.geometry.Insets
import scalafx.scene.control.{ CheckBox, Dialog, Label, PasswordField, TextField }
import scalafx.scene.control.ButtonBar.ButtonData
import scalafx.scene.control.ButtonType
import scalafx.scene.control.ButtonType.sfxButtonType2jfx
import scalafx.scene.layout.GridPane
import scalafx.scene.layout.GridPane.sfxGridPane2jfx
import scalafx.stage.Window
import scalafx.scene.control.Button
import de.lange.m.git.ui.FileSelect

object CloneRepositoryDialog {
    case class Result(url: String, path: String)
}

class CloneRepositoryDialog(
            val window: Window,
            _title: String,
            _header: String
        ) extends Dialog[CloneRepositoryDialog.Result] {
      

    initOwner(window)
    title = _title
    headerText = _header
    dialogPane().contentProperty.setValue( grid )
    dialogPane().getButtonTypes().addAll( cloneButtonType, ButtonType.Cancel )
    
    private val cloneButton = dialogPane().lookupButton(cloneButtonType).asInstanceOf[jfxc.Button]
                cloneButton.setDisable(true)
                            
    url.text.onChange { (_, _, newValue) => cloneButton.setDisable(newValue.trim().isEmpty) }
    path.text.onChange { (_, _, newValue) => cloneButton.setDisable(newValue.trim().isEmpty) }
    
    Platform.runLater(url.requestFocus())
    resultConverter = dialogButton =>
              if (dialogButton == cloneButtonType) CloneRepositoryDialog.Result(url.text(), path.text())
              else null
    
    private lazy val grid = new GridPane() {
        hgap = 10
        vgap = 10
        padding = Insets(20, 10, 10, 10)

        add(new Label("URL:"), 0, 0)
        add(url, 1, 0)
        add(new Label("Path:"), 0, 1)
        add(path, 1, 1)
        add(browse, 2, 1)
    }
    private lazy val url = new TextField() {
        promptText = "Remote URL"
        minWidth = 350
    }
    private lazy val path = new TextField() {
        promptText = "Local path"
        minWidth = 350
    }
    private lazy val browse = new Button {
        text = "..."
        onAction = evt => {
            val file = FileSelect.selectDirectory("Select a path to clone to", null)
            if( file != null ) {
                path.text() = file.getAbsolutePath
            }
        }
    }
    private lazy val cloneButtonType = new ButtonType("Clone", ButtonData.OKDone)
}

