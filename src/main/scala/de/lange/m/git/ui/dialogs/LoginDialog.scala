package de.lange.m.git.ui.dialogs

import javafx.scene.{ control => jfxc }
import scalafx.application.Platform
import scalafx.geometry.Insets
import scalafx.scene.control.{ CheckBox, Dialog, Label, PasswordField, TextField }
import scalafx.scene.control.ButtonBar.ButtonData
import scalafx.scene.control.ButtonType
import scalafx.scene.control.ButtonType.sfxButtonType2jfx
import scalafx.scene.layout.GridPane
import scalafx.scene.layout.GridPane.sfxGridPane2jfx
import scalafx.stage.Window

object LoginDialog {
    case class Result(username: String, password: String, store: Boolean)
}

class LoginDialog(
            val window: Window,
            _title: String,
            _header: String
        ) extends Dialog[LoginDialog.Result] {
      

    initOwner(window)
    title = _title
    headerText = _header
    dialogPane().contentProperty.setValue( grid )
    dialogPane().getButtonTypes().addAll( loginButtonType, ButtonType.Cancel )
    
    private val loginButton = dialogPane().lookupButton(loginButtonType).asInstanceOf[jfxc.Button]
                            loginButton.setDisable(true)
                            
    username.text.onChange { (_, _, newValue) => loginButton.setDisable(newValue.trim().isEmpty) }
    
    Platform.runLater(username.requestFocus())
    resultConverter = dialogButton =>
              if (dialogButton == loginButtonType) LoginDialog.Result(username.text(), password.text(), store.selected())
              else null
    
    private lazy val grid = new GridPane() {
        hgap = 10
        vgap = 10
        padding = Insets(20, 10, 10, 10)

        add(new Label("Username:"), 0, 0)
        add(username, 1, 0)
        add(new Label("Password:"), 0, 1)
        add(password, 1, 1)
        add(store, 1, 2)
    }
    private lazy val username = new TextField() {
        promptText = "Username"
        minWidth = 200
    }
    private lazy val password = new PasswordField() {
        promptText = "Password"
        minWidth = 200
    }
    private lazy val store = new CheckBox {
        text = "Store in INSECURE place?"
    }
    private lazy val loginButtonType = new ButtonType("Login", ButtonData.OKDone)
}

