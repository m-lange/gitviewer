package de.lange.m.git.ui.dialogs

import org.eclipse.jgit.lib.ProgressMonitor

import de.lange.m.git.Paths
import de.lange.m.git.ui.ProgressMonitorFXWorker
import de.lange.m.git.util.ResourceUtil
import scalafx.concurrent.Worker
import scalafx.geometry.Pos
import scalafx.scene.Scene
import scalafx.scene.control.{ Label, ProgressBar }
import scalafx.scene.image.Image
import scalafx.scene.layout.{ Priority, VBox }
import scalafx.stage.{ Modality, Stage, Window }

class ProgressDialog (
            owner: Window,
            _title: String
        ) extends ProgressMonitor {
  
    private val icon = new Image( ResourceUtil.getStream( Paths.appIconPath ) )
    
    private val stage = new Stage()
    private var _canceled = false
    private var loaderTask: ProgressMonitorFXWorker[_] = null
    
    def show(loaderTask: ProgressMonitorFXWorker[_]) {
        this.loaderTask = loaderTask
        stage.initOwner(owner)
        stage.initModality(Modality.ApplicationModal)
        stage.icons.addAll( icon )
        stage.title = _title
        stage.scene = new Scene {
            root = grid
        }
        stage.onHidden = evt => {
            _canceled = true
            stage.close()
        }
        progressText.text <== loaderTask.message
        
        loaderTask.message.onChange { (_, _, newState) =>
            if( ! stage.isShowing() ) {
                stage.show()
            }
        }
        loaderTask.state.onChange { (_, _, newState) =>
            newState match {
                case Worker.State.Succeeded.delegate | 
                     Worker.State.Cancelled.delegate | 
                     Worker.State.Failed.delegate =>
                    progressText.text.unbind()
                    close()
                case _ =>
                    // TODO: handle other states
            }
        }
    }
    
    override def beginTask(name: String, tasks: Int) { 
        loaderTask.update(name, 0, 1)
    }
    override def endTask() {}
    override def isCancelled() = _canceled
    override def start(tasks: Int) {}
    override def update(tasks: Int) {}
    
    def close() {
        stage.close()
    }
              
    private lazy val grid = new VBox {
        children.addAll(  
                    progressBar,
                    progressText
                )
        style =
                  "-fx-padding: 5; " +
                    "-fx-background-color: white; " + // cornsilk
                    "-fx-border-width:5; " +
                    "-fx-border-color: " +
                    "linear-gradient(" +
                    "to bottom, " +
                    "#ff9600, " +
                    "derive(#ff9600, 50%)" +
                    ");"
        vgrow = Priority.Always
    }
    private lazy val progressBar = new ProgressBar {
                        prefWidth = 250
                    }
    private lazy val progressText = new Label {
                        text = "Please wait..."
                        alignment = Pos.Center
                    }
}