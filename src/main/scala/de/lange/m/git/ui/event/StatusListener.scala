package de.lange.m.git.ui.event

trait StatusListener {
    
    def updateStatus(status: String)
}