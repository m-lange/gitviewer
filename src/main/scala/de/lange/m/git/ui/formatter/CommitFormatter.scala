package de.lange.m.git.ui.formatter

import scala.xml.Elem

import de.lange.m.git.data.{ Commit, GitTree }
import de.lange.m.git.settings.DiffSettings

/**
 * Formatter for commit view
 */
trait CommitFormatter {
    
    /**
     * Format a commit or the difference between two commits.
     * 
     * @param tree the repository tree
     * @param oldCommit optional first commit (null to use parent)
     * @param newCommit commit to show
     * @param settings the settings for diff view
     */
    def formatCommits(tree: GitTree, oldCommit: Commit, newCommit: Commit, settings: DiffSettings): Elem
}