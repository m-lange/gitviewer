package de.lange.m.git.ui.formatter

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.xml.{ Elem, NodeBuffer, Text }

import org.eclipse.jgit.diff.{ DiffFormatter, RawTextComparator }
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.diff.DiffEntry.ChangeType
import org.eclipse.jgit.util.io.DisabledOutputStream

import de.lange.m.git.Paths
import de.lange.m.git.data.{ Commit, GitTree }
import de.lange.m.git.settings.{ DiffSettings, TreeViewSettings }
import de.lange.m.git.util.{ ColorUtil, GitUtil, ResourceUtil, XmlUtil }


class DefaultCommitFormatter extends CommitFormatter {
  
    val cssFile = Paths.cssPath+"/commit.css"
    val jsFile = Paths.jsPath+"/commit.js"
    
    override def formatCommits(tree: GitTree, oldCommit: Commit, newCommit: Commit, settings: DiffSettings): Elem = {
        
		val css = createCss( tree.settings, settings, ResourceUtil.getResource( cssFile ) )
		val parentXml: Option[NodeBuffer] = if(oldCommit != null) {
		                    val parKey = oldCommit.name
    				        val buff = new NodeBuffer()
    				        buff += <div>
                                		Id: {oldCommit.shortName}<br/>
    				            		Date: { tree.settings.dateFormat.format( oldCommit.time * 1000L ) }<br/>
    				            		Author: { oldCommit.author }<br/>
									</div>
    				        buff += <div class="message">{ oldCommit.fullMessage }</div>
    				        
    				        val branchesP = if( tree.branchCommits.contains(oldCommit) ) tree.branchCommits(oldCommit) else null
    				        val tagsP = if( tree.tagCommits.contains(oldCommit) ) tree.tagCommits(oldCommit) else null
    				        if( branchesP != null || tagsP != null || ! oldCommit.parents.isEmpty ) {
    				            buff += <br />
    				        }
                    		if( branchesP != null ) {
                    		    buff += <div class="branches">Branches: { for( (br) <- branchesP ) yield {
                    		                    val style = "color:"+{ColorUtil.toWeb(tree.branchColor(br))}
                    		                    val b = new NodeBuffer()
                    		                    b += <span class="branch"><span style={style}>&#9609;</span>&nbsp;{br} </span>
                    		                    b += new Text(" ")
                    		                    b
                    		                }}</div>
                    		}
                    		if( tagsP != null ) {
                    		    buff += <div class="tags">Tags: { for( (br) <- tagsP ) yield {
                    		                    val style = "color:"+{ColorUtil.toWeb(tree.branchColor(oldCommit.branches.head))}
                    		                    val b = new NodeBuffer()
                    		                    b += <span class="tag"><span style={style}>&#9609;</span>&nbsp;{br} </span>
                    		                    b += new Text(" ")
                    		                    b
                    		                }}</div>
                    		}
                    		if( ! oldCommit.parents.isEmpty ) {
                    		    buff += <div class="parents">Parents: { for( (p) <- oldCommit.parents ) yield {
                    		                    <span class="parent" data-commit={p.name}>{p.shortName} </span>
                    		                }}</div>
                    		}
    				        buff += <hr />
    				        Some(buff)
		                } else { None }
		val childXml = new NodeBuffer()
		childXml += <div>
						Id: {newCommit.shortName}<br/>
						Date: { tree.settings.dateFormat.format( newCommit.time * 1000L ) }<br/>
						Author: { newCommit.author }
					</div>
		childXml += <div class="message">{ newCommit.fullMessage }</div>
		
        val branches = if( tree.branchCommits.contains(newCommit) ) tree.branchCommits(newCommit) else null
        val tags = if( tree.tagCommits.contains(newCommit) ) tree.tagCommits(newCommit) else null
        if( branches != null || tags != null || ! newCommit.parents.isEmpty ) {
            childXml += <br />
        }
		if( branches != null ) {
		    childXml += <div class="branches">Branches: { for( (br) <- branches ) yield {
		                    val style = "color:"+{ColorUtil.toWeb(tree.branchColor(br))}
		                    val b = new NodeBuffer()
		                    b += <span class="branch"><span style={style}>&#9609;</span>&nbsp;{br}</span>
		                    b += new Text(" ")
		                    b
		                }}</div>
		}
		if( tags != null ) {
		    childXml += <div class="tags">Tags: { for( (br) <- tags ) yield {
		                    val style = "color:"+{ColorUtil.toWeb(tree.branchColor(newCommit.branches.head))}
		                    val b = new NodeBuffer()
		                    b += <span class="branch"><span style={style}>&#9609;</span>&nbsp;{br} </span>
		                    b += new Text(" ")
		                    b
		                }}</div>
		}
		if( ! newCommit.parents.isEmpty ) {
		    childXml += <div class="parents">Parents: { for( (p) <- newCommit.parents ) yield {
		                    <span class="parent" data-commit={p.name}>{p.shortName} </span>
		                }}</div>
		}
		childXml += <hr />
		
        val xml = <html>
			<head><style>{new Text(css)}</style></head>
			<body>
				{ childXml }
				{ parentXml.orNull }
				<table class="diff-table">{ getCommitDiffs(tree, newCommit, oldCommit, settings) }</table>
				{ XmlUtil.scriptTag( ResourceUtil.getResource( Paths.jQueryFilePath ) ) }
				{ XmlUtil.scriptTag( ResourceUtil.getResource( jsFile ) ) }
			</body>
		</html>
		xml
    }
    
    private def createCss(settings: TreeViewSettings, diffSet: DiffSettings, css: String): String = {
        val res = css.
            replaceAll("\\[fontSize\\]", settings.fontSize.toString()).
            replaceAll("\\[fontFamily\\]", settings.fontFamily.toString()).
            replaceAll("\\[addColor\\]", ColorUtil.toWeb( diffSet.addColor.deriveColor(0, 0.5, 1.5, 1) ) ).
            replaceAll("\\[deleteColor\\]", ColorUtil.toWeb( diffSet.deleteColor.deriveColor(0, 0.5, 1.5, 1) ) )
        res
    }
    
    private def getTypeSymbol(t: ChangeType, settings: DiffSettings): Elem = {
        val (style, sym) = t match {
	        case ChangeType.ADD => ("background-color:"+ColorUtil.toWeb(settings.addColor)+";", "[+]")
	        case ChangeType.DELETE => ("background-color:"+ColorUtil.toWeb(settings.deleteColor)+";", "[-]")
	        case ChangeType.COPY => ("background-color:"+ColorUtil.toWeb(settings.modifyColor)+";", "[c]")
	        case ChangeType.RENAME => ("background-color:"+ColorUtil.toWeb(settings.modifyColor)+";", "[r]")
	        case ChangeType.MODIFY => ("background-color:"+ColorUtil.toWeb(settings.modifyColor)+";", "[m]")
	        case _ => ("color:"+ColorUtil.toWeb(settings.modifyColor)+";", "(?)")
	    }
        <td style={style}>{sym}</td>
    }
    
    private def getCommitDiffs(tree: GitTree, commit: Commit, oldCommit: Commit, settings: DiffSettings): NodeBuffer = {
        val nb = new NodeBuffer()
        val parent = if(oldCommit != null) oldCommit
                     else if(commit.parents.length > 0) commit.parents(0) else null
        if( parent != null ) {
            val df = new DiffFormatter(DisabledOutputStream.INSTANCE)
            df.setRepository(tree.repository)
            df.setDiffComparator(RawTextComparator.DEFAULT)
            df.setDetectRenames(true)
            val diffs = df.scan(GitUtil.prepareTreeParser(tree.repository, parent), GitUtil.prepareTreeParser(tree.repository, commit)).asScala
            
            val elems = new NodeBuffer()
            for( diff <- diffs ) {
                val path = if(diff.getChangeType == ChangeType.DELETE) diff.getOldPath else diff.getNewPath
                val slashPos = path.lastIndexOf("/")
                val (pathStr, fileStr) = if( slashPos < 0 ) ("", "/"+path) else path.splitAt(slashPos)
                val (added, removed) = getChangedLines(diff, df)
                elems += <tr class="commit-file" data-file-path={ path } data-commit={ commit.name } data-parent={ parent.name }>
							{ getTypeSymbol(diff.getChangeType, settings) }
							<td class="lines-removed">{ "-"+removed }</td>
							<td class="lines-added">{ "+"+added }</td>
							<td>{ fileStr.drop(1) }</td>
							<td class="path">{ pathStr }</td>
						</tr>
            }
            nb += <ul>
            		{ elems }
            	</ul>
            
        } else {
            nb += new Text("No parent commit.")
        }
        nb
    }
    
    def getChangedLines(diff: DiffEntry, df: DiffFormatter): (Int, Int) = {   
        var linesDeleted = 0
        var linesAdded = 0
        for (edit <- df.toFileHeader(diff).toEditList().asScala) {
            linesDeleted += edit.getEndA() - edit.getBeginA();
            linesAdded += edit.getEndB() - edit.getBeginB();
        }
        (linesAdded, linesDeleted)
    }
    
}