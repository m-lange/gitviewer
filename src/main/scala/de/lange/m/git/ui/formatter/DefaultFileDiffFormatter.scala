package de.lange.m.git.ui.formatter

import java.io.{ ByteArrayOutputStream, File }
import java.nio.file.Files

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.mutable.ArrayBuffer
import scala.xml.{ Elem, NodeBuffer, Text }

import org.eclipse.jgit.diff.{ DiffFormatter, RawText, RawTextComparator }
import org.eclipse.jgit.diff.DiffEntry.ChangeType
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.treewalk.TreeWalk
import org.eclipse.jgit.treewalk.filter.PathFilter

import de.lange.m.git.Paths
import de.lange.m.git.data.{ Commit, DefaultCommit, GitTree, Uncommitted }
import de.lange.m.git.settings.{ DiffSettings, TreeViewSettings }
import de.lange.m.git.util.{ ColorUtil, GitUtil, ResourceUtil }


class DefaultFileDiffFormatter extends FileDiffFormatter {
    
    val lineNumberPadding = 5
    val cssFile = Paths.cssPath+"/diff.css"
  
    def formatFileDiff(tree: GitTree, settings: DiffSettings, oldCommit: Commit, newCommit: Commit, path: String, mode: FileDiffFormatter.DiffViewMode.Value): Elem = {
        
		val css = createCss( tree.settings, settings, ResourceUtil.getResource( cssFile ) )
		
		val slashPos = path.lastIndexOf("/")
        val (pathStr, fileStr) = if( slashPos < 0 ) ("", "/"+path) else path.splitAt(slashPos)
        val xml = <html>
			<head><style>{new Text(css)}</style></head>
			<body>
				<div id="file-path">{ fileStr.drop(1)+(if(pathStr.length() > 0) " ("+pathStr+")" else "") }</div>
				{ getDiff(tree, settings, newCommit, oldCommit, path, mode) }
			</body>
		</html>
	    xml
    }
    
    private def createCss(treeSet: TreeViewSettings, diffSet: DiffSettings, css: String): String = {
        val res = css.
            replaceAll("\\[fontSize\\]", treeSet.fontSize.toString()).
            replaceAll("\\[fontFamily\\]", treeSet.fontFamily.toString()).
            replaceAll("\\[addColor\\]", ColorUtil.toWeb( diffSet.addColor ) ).
            replaceAll("\\[deleteColor\\]", ColorUtil.toWeb( diffSet.deleteColor ) )
        res
    }
    
    
    def getDiff(tree: GitTree, settings: DiffSettings, commit: Commit, oldCommit: Commit, path: String, mode: FileDiffFormatter.DiffViewMode.Value): NodeBuffer = {   
        val buff = new NodeBuffer()
        val parent = if(oldCommit != null) oldCommit
                     else if(commit.parents.length > 0) commit.parents(0) else null
        if( parent != null ) {
            mode match {
                case FileDiffFormatter.DiffViewMode.Normal | FileDiffFormatter.DiffViewMode.Full =>
                    val out = new ByteArrayOutputStream()
                    val df = new DiffFormatter(out)
                    df.setRepository(tree.repository)
                    if( mode == FileDiffFormatter.DiffViewMode.Normal ) df.setContext( settings.contextLines )
                    else df.setContext( 9999 )
                    df.setDiffComparator(RawTextComparator.DEFAULT)
                    df.setDetectRenames(true)
                    val diffs = df.scan(GitUtil.prepareTreeParser(tree.repository, parent), GitUtil.prepareTreeParser(tree.repository, commit)).asScala
                    
                    for( diff <- diffs ) {
                        val p = if(diff.getChangeType == ChangeType.DELETE) diff.getOldPath else diff.getNewPath
                        if( p == path ) {
                            df.format(diff)
                            val diffText = out.toString("UTF-8")
                            out.reset()
                            buff ++= formatDiff(diffText)
                            return buff
                        }
                    }
                    buff += new Text("File not found: "+path)
                case FileDiffFormatter.DiffViewMode.New =>
                    val cont = getContent( tree, commit, path )
                    if( cont.isDefined ) buff ++= formatFile(cont.get, false)
                case FileDiffFormatter.DiffViewMode.Old =>
                    val cont = getContent( tree, parent, path )
                    if( cont.isDefined ) buff ++= formatFile(cont.get, true)
            }
            buff
        } else {
            buff += new Text("Initial commit, no diff to show.")
            buff
        }
    }
    private def getContent(tree: GitTree, commit: Commit, path: String): Option[Array[String]] = {
        commit match {
            case dc: DefaultCommit =>
                val rw = new RevWalk(tree.repository)
                val tw = new TreeWalk(tree.repository)
                tw.addTree( GitUtil.prepareTreeParser(tree.repository, commit) )
                tw.setFilter( PathFilter.create(path) )
                tw.setRecursive(true)
                val res = if (tw.next()) {
                                val loader = tree.repository.open(tw.getObjectId(0))
                                val bytes = loader.getBytes
                                if( ! RawText.isBinary( bytes ) ) {
                                    Some( new String( bytes ).split("\\r?\\n") )
                                } else {
                                    Some( Array("Binary file") )
                                }
                            } else {
                                None
                            }
                tw.close()
                rw.close()
                res
            case uc: Uncommitted =>
                val file = new File(path)
                if( file.exists() ) {
                    val bytes = Files.readAllBytes( java.nio.file.Paths.get(path) )
                    if( ! RawText.isBinary( bytes ) ) {
                        Some( new String( bytes ).split("\\r?\\n") )
                    } else {
                        Some( Array("Binary file") )
                    }
                } else {
                    None
                }
        }
    }
    
    private def formatDiff(text: String): NodeBuffer = {
        val lines = text.split("\\r?\\n")
        val buff = new NodeBuffer()
        var currLines: ArrayBuffer[String] = null
        for(i <- 0 until lines.length) {
            val line = lines(i)
            if(line.startsWith("@@")) {
                if(null != currLines) {
                    val hunk = formatHunk(currLines)
                    buff ++= hunk
                }
                currLines = new ArrayBuffer[String]()
                currLines += line
            } else {
                if(null != currLines) {
                    currLines += line
                }
            }
        }
        if( null != currLines && currLines.length > 0 ) {
            val hunk = formatHunk(currLines)
            buff ++= hunk
        } else {
            buff += <div class="preformat">{new Text(lines.drop(1).mkString("\n"))}</div>
        }
        
        buff
    }
    
    private def formatHunk(lines: Seq[String]): NodeBuffer = {
        val buff = new NodeBuffer()
        val header = lines(0)
        val content = lines.drop(1)
        
        val linesHeader = header.split("@@")(1).trim().split(' ')
        val lineNumbersOld = linesHeader(0).split(',').map( _.toInt )
        lineNumbersOld(0) = -lineNumbersOld(0)
        val endLineOld = if(lineNumbersOld.length > 1) lineNumbersOld(0)+lineNumbersOld(1)-1 else lineNumbersOld(0)
        
        val lineNumbersNew = linesHeader(1).split(',').map( _.toInt )
        val endLineNew = if(lineNumbersNew.length > 1) lineNumbersNew(0)+lineNumbersNew(1)-1 else lineNumbersNew(0)
        
        val items = new NodeBuffer()
        val numbersOld = new NodeBuffer()
        val numbersNew = new NodeBuffer()
        var cntOld = lineNumbersOld(0)
        var cntNew = lineNumbersNew(0)
        for(line <- content) {
            if( line.startsWith("+") ) {
                items += <span class="added">{ line.substring(1)+" " }</span>
                items += new Text("\n")
                numbersNew += new Text( padNumber(cntNew)+"\n" )
                numbersOld += new Text( "\n" )
                cntNew += 1
            } else if( line.startsWith("-") ) {
                items += <span class="removed">{ line.substring(1)+" " }</span>
                items += new Text("\n")
                numbersNew += new Text( "\n" )
                numbersOld += new Text( padNumber(cntOld)+"\n" )
                cntOld += 1
            } else {
                items += new Text(line.substring(1)+"\n")
                numbersNew += new Text( padNumber(cntNew)+"\n" )
                numbersOld += new Text( padNumber(cntOld)+"\n" )
                cntNew += 1
                cntOld += 1
            }
        }
        buff += <table class="hunk-diff">
					<tr>
						<td  class="hunk-header" colspan="3">Lines {lineNumbersNew(0)}-{endLineNew} ({lineNumbersOld(0)}-{endLineOld})</td>
					</tr>
					<tr>
						<td class="hunk-line-numbers">{ numbersOld }</td>
						<td class="hunk-line-numbers">{ numbersNew }</td>
						<td class="hunk-content">{ items }</td>
					</tr>
				</table>
              
        buff
    }
    
    private def formatFile(lines: Seq[String], isOldVersion: Boolean): NodeBuffer = {
        val buff = new NodeBuffer()
        val header = lines(0)
        val content = lines
        
        val lineNumbersOld = Array(1, lines.length-1)
        val endLineOld = if(lineNumbersOld.length > 1) lineNumbersOld(0)+lineNumbersOld(1)-1 else lineNumbersOld(0)
        
        val items = new NodeBuffer()
        val numbersOld = new NodeBuffer()
        var cntOld = lineNumbersOld(0)
        for(line <- content) {
            items += new Text(line+"\n")
            numbersOld += new Text( padNumber(cntOld)+"\n" )
            cntOld += 1
        }
        buff += <table class="hunk-diff">
					<tr>
						<td  class="hunk-header" colspan="2">Lines {lineNumbersOld(0)}-{endLineOld} ({ if(isOldVersion) "old version" else "new version" })</td>
					</tr>
					<tr>
						<td class="hunk-line-numbers">{ numbersOld }</td>
						<td class="hunk-content">{ items }</td>
					</tr>
				</table>
              
        buff
    }
    
    private def padNumber(n: Int): String = {
        val str = n.toString()
        val len = str.length()
        if( len < lineNumberPadding ) {
            (" " * (lineNumberPadding - len)) + str
        } else {
            str
        }
    }
    
}