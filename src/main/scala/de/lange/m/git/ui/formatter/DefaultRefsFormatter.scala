package de.lange.m.git.ui.formatter

import scala.util.control.Breaks.{ break, breakable }
import scala.xml.{ Elem, Text, NodeBuffer }

import org.eclipse.jgit.lib.{ BranchConfig, BranchTrackingStatus }

import de.lange.m.git.Paths
import de.lange.m.git.data.GitTree
import de.lange.m.git.settings.TreeViewSettings
import de.lange.m.git.util.{ ColorUtil, GitUtil, ResourceUtil, XmlUtil }

class DefaultRefsFormatter extends RefsFormatter {
  
    val cssFile = Paths.cssPath+"/refs.css"
    val jsFile = Paths.jsPath+"/refs.js"
    
    override def formatRefs(tree: GitTree): Elem = {
		val css = createCss( tree.settings, ResourceUtil.getResource( cssFile ) )
		
		val tags = tree.tagRefs.toArray.sortBy( tup => -tup._2.time )
		val refs = tree.branchRefs.toArray.sortBy( tup => tup._1 )
		val remotes = tree.remoteRefs.toArray.sortBy( tup => tup._1 )
		
        val xml = <html>
			<head><style>{new Text(css)}</style></head>
			<body>
				<div>
					<big>Branches</big>
					<hr />
					<ul class="no-list branches">
						{ for((n, c) <- refs if c != null) yield {
						    val style = "color:" + ColorUtil.toWeb(tree.branchColor(n)) 
						    var clazz = "branch"
						    if(tree.currentBranch == n) clazz += " current"
						    if(BranchTrackingStatus.of(tree.repository, n) == null) clazz += " local"
						    
						    val (ahead, behind) = GitUtil.calcTrackingDivergence(tree, n)
						    val track = new NodeBuffer()
						    if( ahead != 0 ) track += <div class="divergence">{ahead}&#8593;</div>
						    if( behind != 0 ) track += <div class="divergence">{behind}&#8595;</div>
						    if( track.length > 0 ) track += new Text(" ")
						    <li class={ clazz } data-commit-id={c.name} data-name={n}><span style={style}>&#9609;</span> {track}{n}</li> 
						}}
					</ul>
				</div>
				<div>
					<big>Remotes</big>
					<hr />
					<ul class="no-list remotes">
						{ for((n, c) <- remotes) yield {
						    val baseName = n.split("/", 2)(1)
						    val style = "color:" + ColorUtil.toWeb( tree.branchColor(baseName) )
						    var clazz = "remote"
						    var isTracked = false
						    breakable(for( (br, comm) <- tree.branchRefs ) {
						        if( new BranchConfig(tree.repository.getConfig(), br).getTrackingBranch == GitTree.remotePrefix+n ) {
						            isTracked = true
						            break
						        }
						    })
						    if( ! isTracked ) clazz += " remote-only"
						    if( c != null ) {
						        <li class={clazz} data-commit-id={c.name} data-name={n}><span style={style}>&#9609;</span> {n}</li>
						    } else {
						        <li class={clazz} data-commit-id="none" data-name={n}><span style={style}>&#9609;</span> {n}</li>  
						    }
						}}
					</ul>
				</div>
				<div>
					<big>Tags</big>
					<hr />
					<ul class="no-list tags">
						{ for((n, c) <- tags if c != null) yield {
						    val style = "color:"+{ColorUtil.toWeb(tree.branchColor(c.branches.head))}
						    <li class={ if(c.isCheckedOut) "tag current" else "tag"} data-commit-id={c.name} data-name={n}><span style={style}>&#9609;</span> {n}</li>
						}}
					</ul>
				</div>
				{ XmlUtil.scriptTag( ResourceUtil.getResource( Paths.jQueryFilePath ) ) }
				{ XmlUtil.scriptTag( ResourceUtil.getResource( jsFile ) ) }
			</body>
		</html>
		xml
    }
    
    private def createCss(settings: TreeViewSettings, css: String): String = {
        val res = css.
            replaceAll("\\[fontSize\\]", settings.fontSize.toString()).
            replaceAll("\\[fontFamily\\]", settings.fontFamily.toString())
        res
    }
}