package de.lange.m.git.ui.formatter

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.xml.{ Elem, NodeBuffer, Text }

import org.eclipse.jgit.diff.{ DiffFormatter, RawTextComparator }
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.diff.DiffEntry.ChangeType
import org.eclipse.jgit.util.io.DisabledOutputStream

import de.lange.m.git.Paths
import de.lange.m.git.data.{ Commit, GitTree }
import de.lange.m.git.settings.{ DiffSettings, TreeViewSettings }
import de.lange.m.git.util.{ ColorUtil, GitUtil, ResourceUtil, XmlUtil }
import de.lange.m.git.util.UIFile


class DefaultStagingFormatter extends StagingFormatter {
  
    val cssFile = Paths.cssPath+"/staging.css"
    val jsFile = Paths.jsPath+"/staging.js"
    
    override def formatStaging(tree: GitTree, settings: DiffSettings): Elem = {
        
		val css = createCss( tree.settings, settings, ResourceUtil.getResource( cssFile ) )
		
        val xml = <html>
			<head><style>{new Text(css)}</style></head>
			<body>
			<div class="flex-container">
				<div>Staged</div>
				<div class="staged scroll">
					<table class="diff-table">{getStaged(tree, settings)}</table>
				</div>
				<div>
					Unstaged
					<div class="staging-buttons">
						<div class="staging-button" id="stage-selected" title="stage selected">&#8593;</div>
						<div class="staging-button" id="stage-all" title="stage all">&#8648;</div>
						<div class="staging-button" id="unstage-selected" title="unstage selected">&#8595;</div>
						<div class="staging-button" id="unstage-all" title="unstage all">&#8650;</div>
					</div>
				</div>
				<div class="unstaged scroll">
					<table class="diff-table">{getUnstaged(tree, settings)}</table>
				</div>
				<div>Message</div>
				<textarea id="message" class="message">{ tree.repoConfig.draftMessage }</textarea>
				<div class="commit-buttons">
					<div id="commit-button">Commit</div>
				</div>
			</div>
				{ XmlUtil.scriptTag( ResourceUtil.getResource( Paths.jQueryFilePath ) ) }
				{ XmlUtil.scriptTag( ResourceUtil.getResource( jsFile ) ) }
			</body>
		</html>
		xml
    }
    
    private def getStaged(tree: GitTree, settings: DiffSettings): NodeBuffer = {
        getFileList(GitUtil.getStaged(tree), settings)
    }
    private def getUnstaged(tree: GitTree, settings: DiffSettings): NodeBuffer = {
        getFileList(GitUtil.getUnstaged(tree), settings)
    }
    
    private def getFileList(files: Seq[UIFile], settings: DiffSettings): NodeBuffer = {
        val buffer = new NodeBuffer()
        
        for( file <- files.sortBy( _.path )) {
            val slashPos = file.path.lastIndexOf("/")
            val (pathStr, fileStr) = if( slashPos < 0 ) ("", "/"+file.path) else file.path.splitAt(slashPos)
            buffer += <tr class="commit-file" data-file-path={ file.path } >
							{ getTypeSymbol(file.change, settings) }
							<td>{ fileStr.drop(1) }</td>
							<td class="path">{ pathStr }</td>
						</tr>
        }
        
        buffer
    }
    
    private def createCss(settings: TreeViewSettings, diffSet: DiffSettings, css: String): String = {
        val res = css.
            replaceAll("\\[fontSize\\]", settings.fontSize.toString()).
            replaceAll("\\[fontFamily\\]", settings.fontFamily.toString()).
            replaceAll("\\[addColor\\]", ColorUtil.toWeb( diffSet.addColor.deriveColor(0, 0.5, 1.5, 1) ) ).
            replaceAll("\\[deleteColor\\]", ColorUtil.toWeb( diffSet.deleteColor.deriveColor(0, 0.5, 1.5, 1) ) ).
            replaceAll("\\[modifyColor\\]", ColorUtil.toWeb( diffSet.modifyColor.deriveColor(0, 0.5, 1.5, 1) ) )
        res
    }
    
    private def getTypeSymbol(t: ChangeType, settings: DiffSettings): Elem = {
        val (style, sym) = t match {
	        case ChangeType.ADD => ("background-color:"+ColorUtil.toWeb(settings.addColor)+";", "[+]")
	        case ChangeType.DELETE => ("background-color:"+ColorUtil.toWeb(settings.deleteColor)+";", "[-]")
	        case ChangeType.COPY => ("background-color:"+ColorUtil.toWeb(settings.modifyColor)+";", "[c]")
	        case ChangeType.RENAME => ("background-color:"+ColorUtil.toWeb(settings.modifyColor)+";", "[r]")
	        case ChangeType.MODIFY => ("background-color:"+ColorUtil.toWeb(settings.modifyColor)+";", "[m]")
	        case _ => ("background-color:"+ColorUtil.toWeb(settings.modifyColor)+";", "(?)")
	    }
        <td style={style}>{sym}</td>
    }
}