package de.lange.m.git.ui.formatter

import scala.xml.Elem

import de.lange.m.git.data.{ Commit, GitTree }
import de.lange.m.git.settings.DiffSettings

object FileDiffFormatter {
    object DiffViewMode extends Enumeration {
        val Normal, Full, Old, New = Value
    }
}

/**
 * Formatter for diff view
 */
trait FileDiffFormatter {
    
    /**
     * Format diff for a file, for one commit of between two commits.
     * 
     * @param tree the repository tree
     * @param settings the settings for diff view
     * @param oldCommit optional first commit (null to use parent)
     * @param newCommit commit to show
     * @param path path of the file to show
     * @param mode diff display mode
     */
    def formatFileDiff(tree: GitTree, settings: DiffSettings, oldCommit: Commit, newCommit: Commit, path: String, mode: FileDiffFormatter.DiffViewMode.Value): Elem
}