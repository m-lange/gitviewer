package de.lange.m.git.ui.formatter

import scala.xml.Elem

import de.lange.m.git.data.GitTree

/**
 * Formatter for refs view
 */
trait RefsFormatter {
    
    /**
     * Format list of references (branches, tags, remotes).
     * 
     * @param tree the repository tree
     */
    def formatRefs(tree: GitTree): Elem
}