package de.lange.m.git.ui.formatter

import scala.xml.Elem

import de.lange.m.git.data.GitTree
import de.lange.m.git.settings.DiffSettings

trait StagingFormatter {
  
    def formatStaging(tree: GitTree, settings: DiffSettings): Elem
    
}