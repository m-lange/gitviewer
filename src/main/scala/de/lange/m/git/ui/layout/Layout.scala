package de.lange.m.git.ui.layout

import scala.xml.{XML, Node}
import scalafx.geometry.Orientation

/**
 * Layout helper values and functions
 */
object Layout {
    
    object Side extends Enumeration {
        val Left, Right, None = Value
    }
    
    val Split = "split"
    val Tabs = "tabs"
    val View = "view"
    
    def create( node: Node ): Layout = {
        new Layout( createNode(node) )
    }
    
    private def createNode( node: Node ): LayoutElement = {
        node.label.toLowerCase() match {
            case Split => 
                val title = (node \ "@title").text
                val oriStr = (node \ "@orientation").text
                val ori = if(oriStr.toLowerCase().startsWith("v")) Orientation.Vertical
                    else Orientation.Horizontal
                val pos = (node \ "@position").text.toDouble
                val fixed = Side.withName( (node \ "@fixed").text.capitalize )
                val split = new SplitContainer(Split, title, ori, pos, fixed)
                for( child <- (node \ "_") ) {
                    split.children += createNode(child)
                }
                split
                
            case Tabs => 
                val title = (node \ "@title").text
                val side = scalafx.geometry.Side( (node \ "@side").text.toUpperCase() )
                val split = new TabsContainer(Tabs, title, side)
                for( child <- (node \ "_") ) {
                    split.children += createNode(child)
                }
                split
                
            case View => 
                val title = (node \ "@title").text
                LayoutLeaf( (node \ "@name").text, title )
        }
    }
}

/**
 * Representation of application layout
 * 
 * @constructor Create a new Layout
 * @param root the Layout's root element
 */
class Layout(
            val root: LayoutElement
        ) {
  
}