package de.lange.m.git.ui.layout

import scala.collection.mutable.ArrayBuffer
import scalafx.geometry.Orientation
import scalafx.geometry.Side

/**
 * Base class for layout containers
 * 
 * @constructor Create a new layout container
 * @param name the name of the element
 */
abstract class LayoutContainer(myName: String, myTitle: String) extends LayoutElement(myName, myTitle) {
      
    /** The container's children */
    val children = new ArrayBuffer[LayoutElement]()
    
}

/**
 * SplitPane container
 * 
 * @constructor Create a SplitPane
 * @param myName the name of the container
 * @param orientation split orientation
 * @param position relative position of the separator
 * @param fixed side that does not change size on parent resize
 */
case class SplitContainer(
            myName: String, 
            myTitle: String,
            val orientation: Orientation, 
            val position: Double, 
            val fixed: Layout.Side.Value ) extends LayoutContainer(myName, myTitle) {
    
}

/**
 * TabPane container
 * 
 * @constructor Create a SplitPane
 * @param myName the name of the container
 * @param title text to display on tab
 */
case class TabsContainer(
            myName: String,
            myTitle: String,
            side: Side ) extends LayoutContainer(myName, myTitle) {
    
}
