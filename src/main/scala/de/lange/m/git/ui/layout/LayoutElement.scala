package de.lange.m.git.ui.layout

/**
 * Base class for layout elements
 * 
 * @constructor Create a new layout element
 * @param name the name of the element
 */
abstract class LayoutElement (
            val name: String,
            val title: String
        ) {
  
}