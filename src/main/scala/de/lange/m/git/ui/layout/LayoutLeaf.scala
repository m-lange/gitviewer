package de.lange.m.git.ui.layout

/**
 * Layout leaf node
 * 
 * @constructor Create a new leaf node
 * @param myName the name of the element
 */
case class LayoutLeaf(myName: String, myTitle: String) extends LayoutElement(myName, myTitle) {
  
}