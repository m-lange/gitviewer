package de.lange.m.git.update

import de.lange.m.git.settings.UpdateSettings
import de.lange.m.git.Version
import scala.io.Source

class UpdateManager(
            val settings: UpdateSettings
        ) {
    
    def updateAvailable: (Option[Version], Option[String]) = {
        try {
            val html = Source.fromURL(settings.versionUrl)
            val s = html.mkString
            val version = new Version(s)
            if( version > Version.version ) {
                (Some(version), None)
            } else {
                (None, None)
            }
        } catch {
            case e: Exception => 
                ( None, Some(e.getMessage) )
        }
    }
    
}