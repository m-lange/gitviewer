package de.lange.m.git.util

import scalafx.scene.paint.Color

object ColorUtil {
  
    def toWeb( color: Color ): String = {
        return "#%02X%02X%02X".format(
            ( color.getRed() * 255 ).toInt,
            ( color.getGreen() * 255 ).toInt,
            ( color.getBlue() * 255 ).toInt );
    }
}