package de.lange.m.git.util

import java.util.concurrent.{ Callable, FutureTask }

import org.eclipse.jgit.lib.ProgressMonitor

import de.lange.m.git.ui.FXWorker
import de.lange.m.git.ui.dialogs.{ CloneRepositoryDialog, LoginDialog, ProgressDialog }
import scalafx.application.Platform
import scalafx.embed.swing.SFXPanel
import scalafx.scene.control.{ ButtonType, ChoiceDialog, TextInputDialog }
import scalafx.scene.control.Alert
import scalafx.scene.control.Alert.AlertType

object DialogUtil {
  
    new SFXPanel()
    
    def warning(title: String, header: String, content: String) {
        simpleDialog(AlertType.Warning, title, header, content)
    }
    def error(title: String, header: String, content: String) {
        simpleDialog(AlertType.Error, title, header, content)
    }
    def login(title: String, header: String): Option[LoginDialog.Result] = {
        val dialog = new LoginDialog(null, title, header)
        val result = dialog.showAndWait()
        result match {
            case Some(r: LoginDialog.Result) => Some(r)
            case _ => None
        }
    }
    def clone(title: String, header: String): Option[CloneRepositoryDialog.Result] = {
        val dialog = new CloneRepositoryDialog(null, title, header)
        val result = dialog.showAndWait()
        result match {
            case Some(r: CloneRepositoryDialog.Result) => Some(r)
            case _ => None
        }
    }
    def confirm(title: String, header: String, content: String): Boolean = {
		val dialog = new Alert(AlertType.Confirmation)
        dialog.setTitle(title)
        dialog.setHeaderText(header)
        dialog.setContentText(content)
        
        dialog.showAndWait() match {
		    case Some(bt) => bt == ButtonType.OK
		    case None => false
		}
    }
    def choice[T](title: String, header: String, content: String, selected: T, choices: Seq[T]): Option[T] = {
		val dialog = new ChoiceDialog[T](selected, choices)
        dialog.setTitle(title)
        dialog.setHeaderText(header)
        dialog.setContentText(content)
        dialog.showAndWait()
    }
    def textInput(title: String, header: String, content: String, text: String): Option[String] = {
		val dialog = new TextInputDialog(text)
        dialog.setTitle(title)
        dialog.setHeaderText(header)
        dialog.setContentText(content)
        dialog.showAndWait()
    }
    def runWithProgress[T](task: (ProgressMonitor) => T) {
        var prog = new ProgressDialog(null, "Progress")
        val worker = FXWorker( task, (t: T) => {}, prog )
        prog.show(worker)
        worker.start()
    }
    def runWithProgressResult[T](task: (ProgressMonitor) => T)(onSuccess: (T) => Unit) {
        var prog = new ProgressDialog(null, "Progress")
        val worker = FXWorker( task, onSuccess, prog )
        prog.show(worker)
        worker.start()
    }
    
    def async[T]( fun: () => T ): T = {
        val task = new FutureTask( new Callable[T]() {
			override def call() = {
			    fun()
			}
	    })
		Platform.runLater(task)
		task.get()
    }
    
    def simpleDialog(alertType: AlertType, title: String, header: String, content: String) {
        val alert = new Alert(alertType)
        alert.title = title
        alert.headerText = header
        alert.contentText = content
        alert.showAndWait()
    }
}