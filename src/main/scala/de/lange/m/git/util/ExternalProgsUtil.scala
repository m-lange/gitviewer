package de.lange.m.git.util

import java.io.File
import java.awt.Desktop
import java.net.URL

object ExternalProgsUtil {
    
    def openFile(file: File) {
	    if (Desktop.isDesktopSupported()) { 
            Desktop.getDesktop().open(file) 
        } else {
            System.out.println("Unable to open file externally: Desktop not supported.")
        }
    }
    
    def openUrl(url: URL) {
	    if (Desktop.isDesktopSupported()) { 
            Desktop.getDesktop().browse( url.toURI() ) 
        } else {
            System.out.println("Unable to open URL externalls: Desktop not supported.")
        }
    }
}