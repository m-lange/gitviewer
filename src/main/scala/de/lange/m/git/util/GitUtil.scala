package de.lange.m.git.util

import java.io.{ File, IOException }

import scala.collection.JavaConverters.asScalaSetConverter
import scala.collection.mutable

import org.eclipse.jgit.api.{ CreateBranchCommand, Git }
import org.eclipse.jgit.api.MergeCommand.FastForwardMode
import org.eclipse.jgit.api.ResetCommand.ResetType
import org.eclipse.jgit.api.errors.TransportException
import org.eclipse.jgit.diff.DiffEntry.ChangeType
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.eclipse.jgit.lib.{ BranchConfig, BranchTrackingStatus, ConfigConstants, Constants, ProgressMonitor, Repository }
import org.eclipse.jgit.merge.MergeStrategy
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.transport.{ CredentialsProvider, RefSpec, UsernamePasswordCredentialsProvider }
import org.eclipse.jgit.treewalk.{ AbstractTreeIterator, CanonicalTreeParser, FileTreeIterator }

import de.lange.m.git.Paths
import de.lange.m.git.data.{ Branch, Commit, DefaultCommit, GitTree, Uncommitted }
import de.lange.m.git.io.XmlRepositoryConfigReader
import de.lange.m.git.ui.dialogs.LoginDialog
import de.lange.m.git.ui.event.StatusListener

object GitUtil {
  
	def isRepository(file: File) : Boolean = {
	    if( file.exists() && file.isDirectory() ) {
	        try {
                new FileRepository(file.getAbsolutePath).getObjectDatabase().exists();
            } catch {
                case e: IOException => false
            }
	    } else {
	        false
	    }
	}
	def getRemoteUrl(repository: Repository, origin: String): String = {
	    repository.getConfig().getString( "remote", origin, "url" )
	}
	def getCredentials(tree: GitTree): Option[CredentialsProvider] = {
	    getLogin(tree) match {
	        case Some( (n, p) ) => Some( new UsernamePasswordCredentialsProvider( n, p ) )
	        case None => None
	    }
	}
	def getLogin(tree: GitTree): Option[(String, String)] = {
	    if(tree == null || tree.repoConfig.user.length() == 0) {
	        val result = DialogUtil.async( () => DialogUtil.login("Login", "Login for "+(if(tree==null) "clone" else tree.name)) )
	        result match {
	            case Some( LoginDialog.Result(n,p,store) ) => 
	                if(store && tree != null) {
	                    tree.repoConfig.user = n
	                    tree.repoConfig.password = p
	                    saveRepoConfig(tree)
	                }
	                Some(n,p)
	            case _ => None
	        }
	    } else {
	        Some( tree.repoConfig.user, tree.repoConfig.password )
	    }
	}
	def clearLogin(tree: GitTree, listener: StatusListener) {
	    tree.repoConfig.user = ""
	    tree.repoConfig.password = ""
	    listener.updateStatus("Login information cleared for "+tree.name)
	}
	def saveRepoConfig(tree: GitTree) {
	    if(tree != null) {
            new XmlRepositoryConfigReader().write(tree.repoConfig, tree.repository.getDirectory+"\\"+Paths.repoConfigPath)
        }
	}
	
	def getUnstaged(tree: GitTree): Seq[UIFile] = {
	    val files = new mutable.ArrayBuffer[UIFile]()
	    val status = try {
            tree.git.status().call()
        } catch {
            case e: Exception =>
                e.printStackTrace();
                return files;
        }
        files ++= status.getUntracked.asScala.map( new UIFile(_, ChangeType.ADD, false ) )
        files ++= status.getModified.asScala.map( new UIFile(_, ChangeType.MODIFY, false ) )
        files ++= status.getConflicting.asScala.map( new UIFile(_, ChangeType.MODIFY, false ) )
        files ++= status.getMissing.asScala.map( new UIFile(_, ChangeType.DELETE, false ) )
        files
	}
	def getStaged(tree: GitTree): Seq[UIFile] = {
	    val files = new mutable.ArrayBuffer[UIFile]()
	    val status = try {
            tree.git.status().call()
        } catch {
            case e: Exception =>
                e.printStackTrace();
                return files;
        }
        files ++= status.getAdded.asScala.map( new UIFile(_, ChangeType.ADD, true ) )
        files ++= status.getChanged.asScala.map( new UIFile(_, ChangeType.MODIFY, true ) )
        files ++= status.getRemoved.asScala.map( new UIFile(_, ChangeType.DELETE, true ) )
        files
	}
	def getUncommittedChanges(tree: GitTree): Option[(mutable.Set[String], mutable.Set[String])] = {
	    val staged = new mutable.HashSet[String]()
	    val unstaged = new mutable.HashSet[String]()
	    val status = try {
            tree.git.status().call()
        } catch {
            case e: Exception =>
                e.printStackTrace()
                return None
        }
        staged ++= status.getAdded.asScala
        staged ++= status.getChanged.asScala
        staged ++= status.getRemoved.asScala
        unstaged ++= status.getUntracked.asScala
        unstaged ++= status.getModified.asScala
        unstaged ++= status.getConflicting.asScala
        unstaged ++= status.getMissing.asScala
        Some( staged, unstaged )
	}
	def hasUncommittedChanges(tree: GitTree): Boolean = {
	    val status = try {
            tree.git.status().call()
        } catch {
            case e: Exception =>
                e.printStackTrace()
                return false
        }
        ! (status.getAdded.isEmpty() && 
           status.getChanged.isEmpty() &&
           status.getRemoved.isEmpty() &&
           status.getUntracked.isEmpty() &&
           status.getModified.isEmpty() &&
           status.getConflicting.isEmpty() &&
           status.getMissing.isEmpty())
	}
	
	def clone(url: String, path: String, listener: StatusListener)( progress: ProgressMonitor ): Option[Git] = {
	    try {
	        progress.beginTask( "Clone...", 1 )
            val git = Git.cloneRepository().
                          setURI( url ).
                          setDirectory( new File(path) ).
                          setProgressMonitor(progress).
                          call()
	        if( listener != null ) listener.updateStatus("Clone done")
	        Some(git)
        } catch {
            case te: TransportException =>
                try {
                    GitUtil.getCredentials(null) match {
                        case Some(cred) =>
                            val git = Git.cloneRepository().
                                      setURI( url ).
                                      setCredentialsProvider( cred ).
                                      setDirectory( new File(path) ).
                                      setProgressMonitor(progress).
                                      call()
                	        if( listener != null ) listener.updateStatus("Clone done")
                	        Some(git)
                        case None =>
                            if( listener != null ) listener.updateStatus("Clone canceled")
                            None
                    }       
                } catch {
                    case e: Exception => 
                        e.printStackTrace()
                        DialogUtil.async( () => DialogUtil.error( "Clone failed", "Clone failed: "+e.getClass.getSimpleName, e.getMessage ) )
                        if( listener != null ) listener.updateStatus("Clone failed")
                        None
            }
            case e: Exception => 
                e.printStackTrace()
                DialogUtil.async( () => DialogUtil.error( "Clone failed", "Clone failed: "+e.getClass.getSimpleName, e.getMessage ) )
                if( listener != null ) listener.updateStatus("Clone failed")
                None
        }
	}
	def checkoutCommit(tree: GitTree, commit: Commit, listener: StatusListener)( progress: ProgressMonitor ) {
	    if( tree.hasUncommittedChanges ) {
	        DialogUtil.async( () => DialogUtil.warning("Checkout failed",
	                            "Checkout failed: uncommitted changes.",
	                            "Commit or stash your changes before checking out another branch.") )
	        return
	    }
        val confirm = DialogUtil.async( () => DialogUtil.confirm("Checkout commit", 
                                "Really check out commit "+commit.shortName+"?", 
                                "Checking out this commit will create a detached HEAD, "+
                                "and you will no longer be on any branch. Really check out?") )
        if(! tryWithDialog("Checkout failed") { () =>
            if(confirm) {
                tree.git.checkout().setName(commit.name).call()
    	        if( listener != null ) listener.updateStatus("Checkout done: "+commit.shortName)
            }
        }) if( listener != null ) listener.updateStatus("Checkout failed: "+commit.shortName)
	}
	def checkoutLocalBranch(tree: GitTree, branch: String, listener: StatusListener)( progress: ProgressMonitor ) {
	    if( tree.hasUncommittedChanges ) {
	        var canCheckout = false
	        val current = tree.currentBranch
	        if( current != tree.current ) {
	            val currentCommit = tree.branchRefs(current)
	            val newCommit = tree.branchRefs(branch)
	            if( currentCommit == newCommit ) {
	                canCheckout = DialogUtil.async( () => DialogUtil.confirm("Checkout with working copy?", "Change working copy?", "Move your working copy to branch '"+branch+"'?") )
	            }
	        }
	        if(canCheckout) {
	            
	        } else {
	            DialogUtil.async( () => DialogUtil.warning("Checkout failed",
	                            "Checkout failed: uncommitted changes.",
	                            "Commit or stash your changes before checking out another branch.") )
	            return
	        }
	    }
        if(! tryWithDialog("Checkout failed") { () =>
            tree.git.checkout().setName(branch).call()
	        if( listener != null ) listener.updateStatus("Checkout done: "+branch)
        }) if( listener != null ) listener.updateStatus("Checkout failed: "+branch)
	}
	def checkoutRemoteBranch(tree: GitTree, branch: String, listener: StatusListener)( progress: ProgressMonitor ) {
	    if( tree.hasUncommittedChanges ) {
	        DialogUtil.async( () => DialogUtil.warning("Checkout failed",
	                            "Checkout failed: uncommitted changes.",
	                            "Commit or stash your changes before checking out another branch.") )
	        if( listener != null ) listener.updateStatus("Canceled checkout remote: "+branch)
	        return
	    }
        val baseName = branch.split("/", 2)(1)
        val result = DialogUtil.async( () => DialogUtil.textInput("Check out remote", 
                                "Check out remote branch '"+branch+"'", 
                                "Branch name:", baseName) )
        result match {
            case Some(newName) =>
                if( tree.branchRefs.contains(newName) ) {
                    DialogUtil.async( () => DialogUtil.warning("Checkout failed",
                                    "Checkout failed: local branch already exisis.",
                                    "A local branch '"+newName+"' already exists.") )
                    if( listener != null ) listener.updateStatus("Canceled checkout remote: "+branch)
                } else {
	                progress.beginTask( "Check out '"+branch+"' ...", 1 )
                    if(! tryWithDialog("Checkout failed") { () =>
	                    val ref = tree.git.checkout().
                                    setCreateBranch(true).
                                    setName(newName).
                                    setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK).
                                    setStartPoint(branch).
                                    setProgressMonitor(progress).
                                    call();
	                   if( listener != null ) listener.updateStatus("Checkout remote done: "+newName)
                    }) if( listener != null ) listener.updateStatus("Checkout remote failed: "+branch)
                }
            case None =>
                if( listener != null ) listener.updateStatus("Canceled checkout remote: "+branch)
        }
	    
	}
	
	def createTag(tree: GitTree, commit: Commit, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(commit == null) { // branch from working copy
	        if( listener != null ) listener.updateStatus("Create tag failed")
	    } else {
	        commit match {
	            case dc: DefaultCommit =>
	                val name = DialogUtil.async( () => DialogUtil.textInput("Create branch", 
                	                                        "Create tag for commit "+dc.shortName, 
                	                                        "Tag name:", "") )
        	        name match {
	                    case Some(n) =>
	                        if(! tryWithDialog("Create tag failed") { () =>
            	                val result = tree.git.tag().
            	                                setObjectId( dc.commit ).
                                                setName(n).
                                                call()
                                if( listener != null ) listener.updateStatus("Created tag "+n)
	                        }) if( listener != null ) listener.updateStatus("Created tag failed: "+n)
	                    case None =>
	                        if( listener != null ) listener.updateStatus("Create tag canceled")
	                }
	            case _ =>
	                if( listener != null ) listener.updateStatus("Create tag failed")
	        }
	    }
	}
	
	def deleteTag(tree: GitTree, tag: String, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(tree != null) {
	        val confirm = DialogUtil.async( () => DialogUtil.confirm("Delete tag", 
            	                                "Delete tag '"+tag+"'?", 
            	                                "Do you really want to delete tag '"+tag+"'?") )
	        if(confirm) {
	            if(! tryWithDialog("Delete failed") { () =>
                    val result = tree.git.tagDelete().
                                    setTags(tag).
                                    call()
                    if(result.isEmpty()) {
                        DialogUtil.async( () => DialogUtil.error("Delete failed",
    	                            "Delete failed: can't delete tag.",
    	                            "Possibly, tab '"+tag+"' is not fully merged. To delete anyway, reset it or delete forced.") )
                        if( listener != null ) listener.updateStatus("Delete failed: "+tag)
                    } else {
                        if( listener != null ) listener.updateStatus("Delete done: "+tag)
                    }
	            }) if( listener != null ) listener.updateStatus("Delete failed: "+tag)
	        } else {
                if( listener != null ) listener.updateStatus("Delete canceled")
	        }
	    }
	}
	
	def createBranch(tree: GitTree, commit: Commit, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(commit == null) { // branch from working copy
	        val name = DialogUtil.async( () => DialogUtil.textInput("Create branch", 
	                                "Create branch from working copy", 
	                                "Branch name:", "") )
	        name match {
                case Some(n) =>
	                val result = tree.git.branchCreate()
                                .setName(n)
                                .call()
                    if( listener != null ) listener.updateStatus("Create remote done: "+n)
                case None =>
                    if( listener != null ) listener.updateStatus("Create branch canceled")
	        }
	    } else {
	        commit match {
	            case dc: DefaultCommit =>
	                val name = DialogUtil.async( () => DialogUtil.textInput("Create branch", 
	                                        "Create branch from commit "+dc.shortName, 
	                                        "Branch name:", "") )
        	        name match {
	                    case Some(n) =>
	                        if(! tryWithDialog("Create branch failed") { () =>
            	                val result = tree.git.branchCreate()
                    	                    .setStartPoint(dc.commit)
                                            .setName(n)
                                            .call()
                                if( listener != null ) listener.updateStatus("Create remote done: "+n)
	                        }) if( listener != null ) listener.updateStatus("Create branch failed: "+n)
	                    case None =>
	                        if( listener != null ) listener.updateStatus("Create branch canceled")
	                }
	            case _ =>
	                if( listener != null ) listener.updateStatus("Create branch failed")
	        }
	    }
	}
	def resetBranch(tree: GitTree, commit: DefaultCommit, mode: ResetType, listener: StatusListener) ( progress: ProgressMonitor ) {
	    if(tree != null) {
	        if( tree.current.name == tree.currentBranch ) {
	            DialogUtil.async( () => DialogUtil.warning("Reset failed",
	                            "Reset failed: not on a branch.",
	                            "Check out a branch before resetting!") )
	            return
	        }
	        val confirm = DialogUtil.async( () => DialogUtil.confirm("Reset branch", 
	                                "Really reset '"+tree.currentBranch+"' to commit "+commit.shortName+"?", 
	                                mode match {
	                                    case ResetType.HARD => "Reset mode HARD will discard all your local changes in index and working copy!"
	                                    case ResetType.MIXED => "Reset mode MIXED will make all changes uncommitted!"
	                                    case ResetType.SOFT => "Reset mode SOFT will reset HEAD!"
	                                    case _ => "???"
	                                }
	                        ) )
	        if(confirm) {
	            if(! tryWithDialog("Reset branch failed") { () =>
                    val result = tree.git.reset().
                                    setMode(mode).
                                    setRef(commit.name).
                                    setProgressMonitor(progress).
                                    call()
                    if( listener != null ) listener.updateStatus("Reset "+mode.name()+" done")
	            }) if( listener != null ) listener.updateStatus("Reset "+mode.name()+" failed")
	        } else {
                if( listener != null ) listener.updateStatus("Reset canceled")
	        }
	    }
	}
	def mergeBranch(tree: GitTree, branch: Branch, listener: StatusListener) ( progress: ProgressMonitor ) {
	    if(tree != null) {
	        if( tree.current.name == tree.currentBranch ) {
	            DialogUtil.async( () => DialogUtil.warning("Merge failed",
	                            "Merge failed: not on a branch.",
	                            "Check out a branch before merging!") )
	            return
	        }
	        if( tree.currentBranch == branch.name ) {
	            DialogUtil.async( () => DialogUtil.warning("Merge failed",
	                            "Merge failed: can't merge branch into itself.",
	                            "Check out another branch before merging!") )
	            return
	        }
	        val br = tree.repository.findRef(branch.name)
	        val confirm = DialogUtil.async( () => DialogUtil.confirm("Merge branch", 
	                                "Merge '"+branch.name+"' into '"+tree.currentBranch+"'?", 
	                                "Do you really want to merge branch '"+branch.name+"' into '"+tree.currentBranch+"'?") )
	        if(confirm) {
	            if(! tryWithDialog("Merge failed") { () =>
                    val result = tree.git.merge().
                                    include(br).
                                    setFastForward(FastForwardMode.NO_FF).
                                    setCommit(true).
                                    setStrategy(MergeStrategy.RECURSIVE).
                                    setProgressMonitor(progress).
                                    call()
                    if( result.getMergeStatus.isSuccessful() ) {
                        if( listener != null ) listener.updateStatus("Merge done")
                    } else {
                        DialogUtil.async( () => DialogUtil.warning("Merge conflics",
    	                            "Conflict merging "+branch.name+" into '"+tree.currentBranch+"'.",
    	                            result.getMergeStatus.toString()) )
                        if( listener != null ) listener.updateStatus("Merge conflict!")
                    }
	            }) if( listener != null ) listener.updateStatus("Merge failed")
	        } else {
                if( listener != null ) listener.updateStatus("Merge canceled")
	        }
	    }
	}
	def mergeCommit(tree: GitTree, commit: Commit, listener: StatusListener) ( progress: ProgressMonitor ) {
	    if(tree != null) {
	        if( tree.current.name == tree.currentBranch ) {
	            DialogUtil.async( () => DialogUtil.warning("Merge failed",
	                            "Merge failed: not on a branch.",
	                            "Check out a branch before merging!") )
	            return
	        }
	        if( tree.current == commit ) {
	            DialogUtil.async( () => DialogUtil.warning("Merge failed",
	                            "Merge failed: can't merge commit into itself.",
	                            "Check out another branch before merging!") )
	            return
	        }
	        val confirm = DialogUtil.async( () => DialogUtil.confirm("Merge branch", 
	                                "Merge "+commit.shortName+" into '"+tree.currentBranch+"'?", 
	                                "Do you really want to delete merge commit "+commit.shortName+" into '"+tree.currentBranch+"'?") )
	        if(confirm) {
	            if(! tryWithDialog("Merge failed") { () =>
                    val result = tree.git.merge().
                                    include(commit.id).
                                    setFastForward(FastForwardMode.NO_FF).
                                    setCommit(true).
                                    setStrategy(MergeStrategy.RECURSIVE).
                                    setProgressMonitor(progress).
                                    call()
                    if( result.getMergeStatus.isSuccessful() ) {
                        if( listener != null ) listener.updateStatus("Merge done")
                    } else {
                        DialogUtil.async( () => DialogUtil.warning("Merge conflics",
    	                            "Conflict merging "+commit.shortName+" into '"+tree.currentBranch+"'.",
    	                            result.getMergeStatus.toString()) )
                        if( listener != null ) listener.updateStatus("Merge conflict!")
                    }
	            }) if( listener != null ) listener.updateStatus("Merge failed")
	        } else {
                if( listener != null ) listener.updateStatus("Merge canceled")
	        }
	    }
	}
	def renameBranch(tree: GitTree, branch: String, isRemote: Boolean, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(isRemote) {
	        renameRemoteBranch(tree, branch, listener)(progress)
	    } else {
	        renameLocalBranch(tree, branch, listener)(progress)
	    }
	}
	def renameLocalBranch(tree: GitTree, branch: String, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(tree != null) {
	        val name = DialogUtil.async( () => DialogUtil.textInput("Rename branch", 
	                                "Rename branch '"+branch+"'", 
	                                "New name:", branch) )
	        name match {
                case Some(n) =>
                    if(! tryWithDialog("Rename failed") { () =>
    	                val result = tree.git.branchRename().
    	                                setOldName(branch).
    	                                setNewName(n).
    	                                call()
                        if( listener != null ) listener.updateStatus("Rename done: "+n)
                    }) if( listener != null ) listener.updateStatus("Rename failed: "+n)
                case None =>
                    if( listener != null ) listener.updateStatus("Rename canceled")
	        }
	    }
	}
	def renameRemoteBranch(tree: GitTree, branch: String, listener: StatusListener)( progress: ProgressMonitor ) {
	    // TODO
	}
	def deleteBranch(tree: GitTree, branch: String, isRemote: Boolean, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(isRemote) {
	        deleteRemoteBranch(tree, branch, listener)(progress)
	    } else {
	        deleteLocalBranch(tree, branch, listener)(progress)
	    }
	}
	def deleteLocalBranch(tree: GitTree, branch: String, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(tree != null) {
	        if( tree.currentBranch == branch ) {
	            DialogUtil.async( () => DialogUtil.warning("Delete failed",
	                            "Delete failed: can't delete checked out branch.",
	                            "Check out another branch before deleting '"+branch+"'!") )
	            return
	        }
	        val confirm = DialogUtil.async( () => DialogUtil.confirm("Delete branch", 
	                                "Delete '"+branch+"'?", 
	                                "Do you really want to delete branch '"+branch+"'?") )
	        if(confirm) {
	            if(! tryWithDialog("Delete failed") { () =>
                    val result = tree.git.branchDelete().
                                    setBranchNames(branch).
                                    call()
                    if(result.isEmpty()) {
                        DialogUtil.async( () => DialogUtil.error("Delete failed",
    	                            "Delete failed: can't delete branch.",
    	                            "Possibly, branch '"+branch+"' is not fully merged. To delete anyway, reset it or delete forced.") )
                        if( listener != null ) listener.updateStatus("Delete failed: "+branch)
                    } else {
                        if( listener != null ) listener.updateStatus("Delete done: "+branch)
                    }
	            }) if( listener != null ) listener.updateStatus("Delete failed: "+branch)
	        } else {
                if( listener != null ) listener.updateStatus("Delete canceled")
	        }
	    }
	}
	def deleteRemoteBranch(tree: GitTree, branch: String, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(tree != null) {
	        if( tree.currentBranch == branch ) {
	            DialogUtil.async( () => DialogUtil.warning("Delete failed",
	                            "Delete failed: can't delete checked out branch.",
	                            "Check out another branch before deleting '"+branch+"'!") )
	            return
	        }
	        val confirm = DialogUtil.async( () => DialogUtil.confirm("Delete branch",
	                                "Delete '"+branch+"'?", 
	                                "Do you really want to delete branch '"+branch+"'?") )
	        if(confirm) {
	            GitUtil.getCredentials(tree) match {
                    case Some(cred) =>
                        if(! tryWithDialog("Delete failed") { () =>
            	            progress.beginTask( "Deleting '"+branch+"' ...", 1 )
            	            val nameParts = branch.split("/", 2)
                            val refSpec = new RefSpec().
                                                setSource(null).
                                                setDestination("refs/heads/"+nameParts(1))
                            val result = tree.git.push().
                                            setRefSpecs(refSpec).
                                            setRemote(nameParts(0)).
                                            setCredentialsProvider(cred).
                                            setProgressMonitor(progress).
                                            call()
                            if( listener != null ) listener.updateStatus("Delete done: "+branch)
                        }) if( listener != null ) listener.updateStatus("Delete failed: "+branch)
                    case None =>
                        if( listener != null ) listener.updateStatus("Delete canceled")
	            }
	        } else {
                if( listener != null ) listener.updateStatus("Delete canceled")
	        }
	    }
	}
	def pullBranch(tree: GitTree, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(tree != null) {
	        /*if( tree.hasUncommittedChanges ) {
	            DialogUtil.warning("Pull failed",
	                            "Pull failed: uncommitted changes.",
	                            "Commit or stash your changes before pulling this branch.")
	            return;
	        }*/
	        val branch = tree.currentBranch
	        val commit = tree.current
	        if( branch != commit.name ) {
	            val conf = new BranchConfig(tree.repository.getConfig, branch)
	            val remoteFull = conf.getTrackingBranch
	            if( remoteFull != null ) {
	                GitUtil.getCredentials(tree) match {
	                    case Some(cred) =>
    	                    val remote = tree.repository.getRemoteName(remoteFull)
    	                    if(! tryWithDialog("Pull failed") { () =>
    	                        progress.beginTask( "Pulling '"+branch+"' ...", 1 )
        	                    val result = tree.git.pull().
        	                                    setRemote(remote).
        	                                    setCredentialsProvider( cred ).
        	                                    setTimeout(10000).
                                                setProgressMonitor(progress).
        	                                    call()
        	                    if( result.isSuccessful() ) {
        	                        println("Pull done: "+branch)
        	                        if( listener != null ) listener.updateStatus("Pull done: "+branch)
        	                    } else {
        	                        DialogUtil.async( () => DialogUtil.error("Pull failed", "Pull failed", "Reason unknown.") )
        	                        if( listener != null ) listener.updateStatus("Pull failed: "+branch)
        	                    }
    	                    }) if( listener != null ) listener.updateStatus("Pull failed: "+branch)
	                    case None =>
                            if( listener != null ) listener.updateStatus("Pull canceled")
	                }
	            } else {
	                DialogUtil.async( () => DialogUtil.warning("Pull failed", "Pull failed: no remote", "The current branch "+branch+" does not track a remote branch!") )
        	        if( listener != null ) listener.updateStatus("Pull failed: "+branch)
	            }
    	    } else {
    	        DialogUtil.async( () => DialogUtil.warning("Pull failed", "Pull failed: headless", "No branch checked out!") )
        	    if( listener != null ) listener.updateStatus("Pull failed: "+branch)
    	    }
	    }
	}
	
	def fetchRemotes(tree: GitTree, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(tree != null) {
	        GitUtil.getCredentials(tree) match {
                case Some(cred) =>
                    for( remote <- tree.repository.getRemoteNames().asScala ) {
    	                progress.beginTask( "Fetching from '"+remote+"' ...", 1 )
                        if(! tryWithDialog("Fetch failed") { () =>
                            val spec = tree.repository.getConfig.getRefSpecs("remote", remote, "fetch")
                            val fetch = tree.git.fetch().
                                            setRefSpecs( spec ).
                                            setRemoveDeletedRefs(true).
    	                                    setCredentialsProvider( cred ).
    	                                    setTimeout(10000).
                                            setProgressMonitor(progress)
    	                    val result = fetch.call()
    	                    println("Fetch "+remote+" done.")
    	                    if( listener != null ) listener.updateStatus("Fetch "+remote+" done.")
                        }) if( listener != null ) listener.updateStatus("Fetch failed: "+remote)
                    }
                case None =>
            }
	    }
	}
	
	def pushBranch(tree: GitTree, remote: String, forced: Boolean, listener: StatusListener)( progress: ProgressMonitor ) {
	    pushBranch(tree, remote, null, forced, listener)(progress)
	}
	
	def pushBranch(tree: GitTree, remoteTarget: String, pushBranch: String, forced: Boolean, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(tree != null) {
	        val remote = if(remoteTarget == null) {
                        	    val choices = tree.remotes.asScala.toSeq
                        	    val remoteOpt = DialogUtil.async( () => DialogUtil.choice("Select remote", "Select a remote to push to", "Remote:", choices.head, choices) )
                        	    remoteOpt match {
                        	        case Some(remote) =>
                        	            remote
                        	        case None =>
                        	            if( listener != null ) listener.updateStatus("Push canceled")
                        	            return
                        	    }
                	        } else {
                	            remoteTarget
                	        }
	        val branch = if(pushBranch != null) pushBranch else tree.currentBranch
	        
	        if(forced) {
	            val confirm = DialogUtil.async( () => DialogUtil.confirm("Force-push branch",
	                                "Really force-push '"+branch+"'?", 
	                                "Do you really want push '"+branch+"' forced? This can be potentially harmful to your remote repository!") )
	            if( ! confirm ) {
	                return
	            }
	        }
	        
	        val commit = tree.current
	        if( remote != null ) {
    	        if( branch != commit.name ) {
    	            val remoteBranchOpt = DialogUtil.async( () => DialogUtil.textInput("Push branch", 
	                                                    "Push branch '"+branch+"' to remote", 
	                                                    "Remote branch:", branch) )
	                remoteBranchOpt match {
    	                case Some(remoteBranch) =>
            	            val conf = new BranchConfig(tree.repository.getConfig, branch)
                            GitUtil.getCredentials(tree) match {
                                case Some(cred) =>
            	                    if(! tryWithDialog("Push failed") { () =>
            	                        progress.beginTask( "Pushing '"+branch+"' ...", 1 )
                	                    val result = tree.git.push().
                	                                    setRefSpecs( new RefSpec( branch+":"+remoteBranch ) ).
                	                                    setRemote(remote).
                	                                    setPushTags().
                	                                    setCredentialsProvider( cred ).
                	                                    setTimeout(10000).
                	                                    setForce(forced).
                	                                    setPushTags().
                                                        setProgressMonitor(progress).
                	                                    call()
            	                        val config = tree.repository.getConfig()
            	                        config.setString(ConfigConstants.CONFIG_BRANCH_SECTION, branch,  ConfigConstants.CONFIG_KEY_REMOTE, remote)
                                        config.setString(ConfigConstants.CONFIG_BRANCH_SECTION, branch, ConfigConstants.CONFIG_KEY_MERGE, Constants.R_HEADS + remoteBranch)
                                        config.save()
                	                    println("Push done: "+branch)
                	                    if( listener != null ) listener.updateStatus("Push done: "+branch)
            	                    }) if( listener != null ) listener.updateStatus("Push failed: "+branch)
                                case None =>
                            }
    	                case None =>
            	            if( listener != null ) listener.updateStatus("Push canceled: "+branch)
    	            }
        	    } else {
        	        DialogUtil.async( () => DialogUtil.warning("Push failed", "Push failed: headless", "No branch checked out!") )
            	    if( listener != null ) listener.updateStatus("Push failed: "+branch)
        	    }
	        } else {
    	        DialogUtil.async( () => DialogUtil.warning("Push failed", "Push failed: no remote", "No remote repository specified!") )
        	    if( listener != null ) listener.updateStatus("Push failed: "+branch)
    	    }
	    }
	}
	
	def setTrackingBranch(tree: GitTree, branch: String, remoteBranch: String, listener: StatusListener)( progress: ProgressMonitor ) {
	    if(tree != null) {
	        val (remote, targetRef) = if( remoteBranch == null ) {
	                                        // TODO get default remote
	                                        ( "origin", "" )
	                                    } else {
	                                        val nameParts = remoteBranch.split("/", 2)
	                                        (nameParts(0), nameParts(1))
	                                    }
	        if( remote != null ) { 
	            if( ! tryWithDialog("Change tracking branch") { () =>
    	            val conf = new BranchConfig(tree.repository.getConfig, branch)
                    val config = tree.repository.getConfig()
                    config.setString(ConfigConstants.CONFIG_BRANCH_SECTION, branch,  ConfigConstants.CONFIG_KEY_REMOTE, remote)
                    if( remoteBranch == null ) config.setString(ConfigConstants.CONFIG_BRANCH_SECTION, branch, ConfigConstants.CONFIG_KEY_MERGE, null)
                    else config.setString(ConfigConstants.CONFIG_BRANCH_SECTION, branch, ConfigConstants.CONFIG_KEY_MERGE, Constants.R_HEADS + targetRef)
                            
                    config.save()
                    if( listener != null ) listener.updateStatus("Tracking branch set to "+remoteBranch)
	            }) if( listener != null ) listener.updateStatus("Set tracking branch failed")
	        } else {
    	        DialogUtil.async( () => DialogUtil.warning("Push failed", "Push failed: no remote", "No remote repository specified!") )
        	    if( listener != null ) listener.updateStatus("Push failed: "+branch)
    	    }
	    }
	}
	
	
    def stageFiles(tree: GitTree, files: Seq[String]) {
        if( files.isEmpty ) return
        try {
            val cmd = tree.git.add()
            val cmd2 = tree.git.add().setUpdate(true)
            for(f <- files) {
                cmd.addFilepattern(f)
                cmd2.addFilepattern(f)
            }
            cmd.call()
            cmd2.call()
        } catch {
            case e: Exception => e.printStackTrace()
        }
    }
    def stageAll(tree: GitTree) {
        try {
            tree.git.add().addFilepattern(".").call()
            tree.git.add().setUpdate(true).addFilepattern(".").call()
        } catch {
            case e: Exception => e.printStackTrace()
        }
    }
    def unstageFiles(tree: GitTree, files: Seq[String]) {
        if( files.isEmpty ) return
        try {
            val cmd = tree.git.reset()
            for(f <- files) {
                cmd.addPath(f)
            }
            cmd.call()
        } catch {
            case e: Exception => e.printStackTrace()
        }
    }
    def unstageAll(tree: GitTree) {
        try {
            tree.git.reset().
                setMode(ResetType.MIXED).
                call()
        } catch {
            case e: Exception => e.printStackTrace()
        }
    }
    def commit(tree: GitTree, message: String, listener: StatusListener): Boolean = {
        try {
            val result = tree.git.commit().
                            setMessage(message).
                            call()
            if( listener != null ) listener.updateStatus("Commit done: "+result.name().substring(0, 7))
            true
        } catch {
            case e: Exception => 
                e.printStackTrace()
                DialogUtil.error(
                    "Commit failed", 
                    "Commit failed: "+e.getClass.getSimpleName, 
                    e.getMessage )
                if( listener != null ) listener.updateStatus("Commit failed")
                false
        }
    }
	
	def calcTrackingDivergence(tree: GitTree, branch: String): (Int, Int) = {
        val trackingStatus = BranchTrackingStatus.of(tree.repository, branch)
        if (trackingStatus != null) {
            (trackingStatus.getAheadCount(), trackingStatus.getBehindCount())
        } else {
            (0, 0)
        }
    }
	
	def prepareTreeParser(repository: Repository, commit: Commit): AbstractTreeIterator = {
        // from the commit we can build the tree which allows us to construct the TreeParser
        //noinspection Duplicates
	    commit match {
	        case dc: DefaultCommit =>
                val walk = new RevWalk(repository)
                val commit = walk.parseCommit( dc.id )
                val tree = walk.parseTree(commit.getTree().getId())
        
                val treeParser = new CanonicalTreeParser()
                val reader = repository.newObjectReader()
                treeParser.reset(reader, tree.getId())
        
                walk.dispose()
        
                treeParser
	        case uc: Uncommitted =>
	            new FileTreeIterator( repository )
	    }
    }
	
	private def tryWithDialog(title: String)( fun: () => Unit ): Boolean = {
	    try {
            fun()
            true
        } catch {
            case e: Exception =>
                e.printStackTrace()
                DialogUtil.async( () => DialogUtil.error(
                        title, 
                        title+": "+e.getClass.getSimpleName, 
                        e.getMessage ) )
                false
        }
	}
	
}