package de.lange.m.git.util

import java.io.{ BufferedReader, InputStream, InputStreamReader }

import de.lange.m.git.Paths
import scalafx.scene.Node
import scalafx.scene.image.{ Image, ImageView }

object ResourceUtil {
  
    private val newline = sys.props("line.separator")
    
    def getResource(path: String): String = {
        val stream = { classOf[ResourceUtil].getClassLoader().getResourceAsStream( path ) }
        val stringBuilder = new StringBuilder()
        val bufferedReader = new BufferedReader(new InputStreamReader(stream)) 	
        var line: String = bufferedReader.readLine()
		while( line != null) {
			stringBuilder.append(line).append(newline)
			line = bufferedReader.readLine()
		}
        stringBuilder.toString()
    }
    def getStream(path: String): InputStream = {
        classOf[ResourceUtil].getClassLoader().getResourceAsStream( path )
    }
    def getIconImage(fileName: String): Node = {
	    new ImageView(  new Image( ResourceUtil.getStream(Paths.iconsPath+"/"+fileName)) )
    }
}
class ResourceUtil {}