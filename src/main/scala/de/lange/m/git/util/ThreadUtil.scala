package de.lange.m.git.util

import de.lange.m.git.ui.FXWorker

object ThreadUtil {
  
    def parallel[T](task: () => T)(onSuccess: (T) => Unit) {
        val worker = FXWorker( task, onSuccess )
        worker.start()
    }
}