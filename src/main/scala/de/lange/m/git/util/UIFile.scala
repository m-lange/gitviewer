package de.lange.m.git.util

import org.eclipse.jgit.diff.DiffEntry.ChangeType

class UIFile(
            val path: String,
            val change: ChangeType,
            val isStaged: Boolean
        ) {
  
}