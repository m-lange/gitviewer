package de.lange.m.git.util

import scala.xml.Elem

object XmlUtil {
  
    def scriptTag(script: String): Elem = {
        <script type="text/javascript">
        { scala.xml.Unparsed("//<![CDATA[\n%s\n// ]]>".format(script)) }
  		</script>
    }
}